#pragma once
#include <assimp/scene.h>
#include <vector>
#include "Shader.h"
#include "Mesh.h"

class Model
{
public:
	Model(const std::string &path)
	{
		loadModel(path);
	}

	void Draw(Shader& shader);
	const std::vector<Mesh>& GetMeshes() const { return m_meshes; }
	const std::vector<Texture>& GetTextures() const { return textures_loaded; }

private:
	// model data
	std::vector<Mesh> m_meshes;
	std::string m_directory;

	std::vector<Texture> textures_loaded; // optimization, do not load same texture multiple times

	void loadModel(const std::string& path);
	void processNode(aiNode* node, const aiScene* scene);
	Mesh processMesh(aiMesh* mesh, const aiScene* scene);
	std::vector<Texture> loadMaterialTextures(aiMaterial* mat, aiTextureType type, Texture::Type textureTpye);
};

