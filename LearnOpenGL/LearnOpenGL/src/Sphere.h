#pragma once

#include "vendor/glm/vec3.hpp"
#include "vendor/glm/vec2.hpp"
#include "Shader.h"

#include <vector>

class Sphere
{
	struct Vertex
	{
		glm::vec3 position;
		glm::vec3 normal;
		glm::vec2 texCoord;
	};

private:
	float radius{};
	int sectorCount{};
	int stackCount{};

	// TODO: stattdessen ein Mesh objekt nutzen?
	std::vector<Vertex> vertices;
	std::vector<unsigned int> indices;

	GLuint vbo{};
	GLuint ebo{};
	GLuint vao{0};

public:
	Sphere(float radius = 1.0f, int sectorCount = 36, int stackCount = 18);
	Sphere(const Sphere& other);
	~Sphere();

	void set(float radius, int sectorCount, int stack_count);
	float getRadius() const { return radius; }
	int getSectorCount() const { return sectorCount; }
	int getStackCount() const { return stackCount; }
	void setRadius(float radius);
	void setSectorCount(int sectorCount);
	void setStackCount(int stackCount);
	
	void draw(const Shader& shader) const;

private:
	
	void buildVertices();
	void setupOGL();
	void clearArrays();
};

