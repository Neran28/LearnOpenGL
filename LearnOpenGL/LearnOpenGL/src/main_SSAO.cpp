#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include "vendor/stb_image/stb_image.h"

#include "vendor/glm/glm.hpp"
#include "vendor/glm/gtc/matrix_transform.hpp"
#include "vendor/glm/gtc/type_ptr.hpp"

#include "Shader.h"
#include "FlyCamera.h"
#include "Model.h"
#include "GLDebug.h"

#include <map>
#include <iostream>
#include <sstream>
#include <random>

#include "Sphere.h"


void framebuffer_size_callback(GLFWwindow* window, int width, int height);
void mouse_callback(GLFWwindow* window, double xpos, double ypos);
void mouse_button_callback(GLFWwindow* window, int button, int action, int mods);
void scroll_callback(GLFWwindow* window, double xoffset, double yoffset);
void processInput(GLFWwindow* window);
unsigned int loadTexture(const char* path, bool gammaCorrection);

// settings
std::string title;
GLFWwindow* window = nullptr;
const unsigned int SCR_WIDTH = 800;
const unsigned int SCR_HEIGHT = 600;

// camera
FlyCamera camera(glm::vec3(5.0f, 3.0f, 6.0f), glm::vec3(0.0f, 1.0f, 0.0f), -130.0f, -25.0f);
float lastX = (float)SCR_WIDTH / 2.0f;
float lastY = (float)SCR_HEIGHT / 2.0f;
bool firstMouse = true;
bool flyingCameraActive = false;

// timing
double delta_time = 0.0;
double time_last_frame = 0.0;
double frame_time;
long total_frames = 0;
float fps[30]{};
double time_fps = 0;

void renderQuad();
void renderCube();

void updatePerformanceStats()
{
    const int frame = total_frames % (sizeof(fps) / sizeof(fps[0]));
    fps[frame] = (float)(1.0 / delta_time);

    if ((time_last_frame - time_fps) > 0.5) {
        time_fps = time_last_frame;
        float sum = 0;
        for (int i = 0; i < (sizeof(fps) / sizeof(fps[0])); ++i)
            sum += fps[i];

        std::stringstream s;
        float roundedFPS = (int)(sum / (sizeof(fps) / sizeof(fps[0])) * 100 + 0.5f);
        roundedFPS = roundedFPS / 100;
        s << title << " - FPS: " << roundedFPS << "    frame time: " << frame_time * 1000 << "ms";
        glfwSetWindowTitle(window, s.str().c_str());
    }
}

int main()
{
    title = "Deferred Rendering Demo";

    // glfw: initialize and configure
    // ------------------------------
    glfwInit();
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

#ifdef __APPLE__
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
#endif

    // glfw window creation
    // --------------------
    window = glfwCreateWindow(SCR_WIDTH, SCR_HEIGHT, title.c_str(), nullptr, nullptr);
    if (window == nullptr)
    {
        std::cout << "Failed to create GLFW window" << std::endl;
        glfwTerminate();
        return -1;
    }
    glfwMakeContextCurrent(window);
    glfwSetFramebufferSizeCallback(window, framebuffer_size_callback);
    glfwSetMouseButtonCallback(window, mouse_button_callback);
    glfwSetCursorPosCallback(window, mouse_callback);
    glfwSetScrollCallback(window, scroll_callback);


    // glad: load all OpenGL function pointers
    // ---------------------------------------
    if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
    {
        std::cout << "Failed to initialize GLAD" << std::endl;
        return -1;
    }

    stbi_set_flip_vertically_on_load(true);

    // configure global opengl state
    // -----------------------------
    glEnable(GL_DEPTH_TEST);

    // build and compile shaders
    // -------------------------
    Shader gbuffer_shader("res/shaders/8_2_g_buffer.vert", "res/shaders/8_2_g_buffer.frag");
    Shader lighting_pass_shader("res/shaders/8_2_deferred_shading.vert", "res/shaders/8_2_deferred_shading.frag");
    Shader light_source_shader("res/shaders/8_2_mono_color.vert", "res/shaders/8_2_mono_color.frag");
    Shader normal_vis("res/shaders/normal_visualization.vert", "res/shaders/normal_visualization.geom", "res/shaders/normal_visualization.frag");
    Shader null_shader("res/shaders/null_shader.vert", "res/shaders/null_shader.frag");
    Shader ssao_shader("res/shaders/8_3_ssao.vert", "res/shaders/8_3_ssao.frag");
    
    // load models
    // -------------
    Model backpack("res/Assets/models/backpack/backpack.obj");
    std::vector<glm::vec3> objectPositions;
    objectPositions.emplace_back(-3.0, -0.5, -3.0);
    objectPositions.emplace_back(0.0, -0.5, -3.0);
    objectPositions.emplace_back(3.0, -0.5, -3.0);
    objectPositions.emplace_back(-3.0, -0.5, 0.0);
    objectPositions.emplace_back(0.0, -0.5, 0.0);
    objectPositions.emplace_back(3.0, -0.5, 0.0);
    objectPositions.emplace_back(-3.0, -0.5, 3.0);
    objectPositions.emplace_back(0.0, -0.5, 3.0);
    objectPositions.emplace_back(3.0, -0.5, 3.0);
    objectPositions.emplace_back(6.0, -0.5, -3.0);
    objectPositions.emplace_back(9.0, -0.5, -3.0);
    objectPositions.emplace_back(-4.0, -0.5, -9.0);

    // configure g-buffer framebuffer
    // -------------
    GLuint g_buffer;
    glGenFramebuffers(1, &g_buffer);
    glBindFramebuffer(GL_FRAMEBUFFER, g_buffer);

    GLuint g_position, g_normal, g_albedo_spec;
    // position color buffer
    glGenTextures(1, &g_position);
    glBindTexture(GL_TEXTURE_2D, g_position);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA16F, SCR_WIDTH, SCR_HEIGHT, 0, GL_RGBA, GL_FLOAT, nullptr);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, g_position, 0);
    // normal color buffer
    glGenTextures(1, &g_normal);
    glBindTexture(GL_TEXTURE_2D, g_normal);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA16F, SCR_WIDTH, SCR_HEIGHT, 0, GL_RGBA, GL_FLOAT, nullptr);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT1, GL_TEXTURE_2D, g_normal, 0);
    // color and specular buffer
    glGenTextures(1, &g_albedo_spec);
    glBindTexture(GL_TEXTURE_2D, g_albedo_spec);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, SCR_WIDTH, SCR_HEIGHT, 0, GL_RGBA, GL_UNSIGNED_BYTE, nullptr);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT2, GL_TEXTURE_2D, g_albedo_spec, 0);

    GLuint attachments[3] = { GL_COLOR_ATTACHMENT0, GL_COLOR_ATTACHMENT1, GL_COLOR_ATTACHMENT2 };
    glDrawBuffers(3, attachments);

    GLuint rbo_depth;
    glGenRenderbuffers(1, &rbo_depth);
    glBindRenderbuffer(GL_RENDERBUFFER, rbo_depth);
    glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_STENCIL, SCR_WIDTH, SCR_HEIGHT);
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_STENCIL_ATTACHMENT, GL_RENDERBUFFER, rbo_depth);

    if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
        std::cout << "Framebuffer not complete!" << std::endl;
    glBindFramebuffer(GL_FRAMEBUFFER, 0);

    // shader configuration
	// --------------------
    GLuint ubo_matrices;
    glGenBuffers(1, &ubo_matrices);
    glBindBuffer(GL_UNIFORM_BUFFER, ubo_matrices);
    glBufferData(GL_UNIFORM_BUFFER, 2 * sizeof(glm::mat4), nullptr, GL_STATIC_DRAW);
    glBindBuffer(GL_UNIFORM_BUFFER, 0);
    glBindBufferBase(GL_UNIFORM_BUFFER, 1, ubo_matrices);

    // man kann im shader ab 420 das binding reinschreiben, damit f�llt das GetUniform() weg
    unsigned int matrices_index = glGetUniformBlockIndex(gbuffer_shader.m_rendererID, "Matrices");
    glUniformBlockBinding(gbuffer_shader.m_rendererID, matrices_index, 1);

    matrices_index = glGetUniformBlockIndex(light_source_shader.m_rendererID, "Matrices");
    glUniformBlockBinding(light_source_shader.m_rendererID, matrices_index, 1);

    matrices_index = glGetUniformBlockIndex(normal_vis.m_rendererID, "Matrices");
    glUniformBlockBinding(normal_vis.m_rendererID, matrices_index, 1);

    matrices_index = glGetUniformBlockIndex(lighting_pass_shader.m_rendererID, "Matrices");
    glUniformBlockBinding(lighting_pass_shader.m_rendererID, matrices_index, 1);

    matrices_index = glGetUniformBlockIndex(null_shader.m_rendererID, "Matrices");
    glUniformBlockBinding(null_shader.m_rendererID, matrices_index, 1);

    matrices_index = glGetUniformBlockIndex(ssao_shader.m_rendererID, "Matrices");
    glUniformBlockBinding(ssao_shader.m_rendererID, matrices_index, 1);

	
    // lighting info
    // -------------
    const unsigned int NR_LIGHTS = 32;
    const float attenuation_linear = 0.7f;
    const float attenuation_quadratic = 1.8f;
    std::vector<glm::vec3> lightPositions;
    std::vector<glm::vec3> lightColors;
    std::vector<float> lightRadii;
    std::vector<Sphere> spheres;
    srand(13);
    for (unsigned int i = 0; i < NR_LIGHTS; i++)
    {
        const float attenuation_constant = 1.0f;

        // calculate slightly random offsets
        float xPos = ((rand() % 100) / 100.0) * 6.0 - 3.0;
        float yPos = ((rand() % 100) / 100.0) * 6.0 - 4.0;
        float zPos = ((rand() % 100) / 100.0) * 6.0 - 3.0;
        lightPositions.emplace_back(xPos, yPos, zPos);
        // also calculate random color
        float rColor = ((rand() % 100) / 200.0f) + 0.5; // between 0.5 and 1.0
        float gColor = ((rand() % 100) / 200.0f) + 0.5; // between 0.5 and 1.0
        float bColor = ((rand() % 100) / 200.0f) + 0.5; // between 0.5 and 1.0
        lightColors.emplace_back(rColor, gColor, bColor);

        const float maxBrightness = std::fmaxf(std::fmaxf(rColor, gColor), bColor);
        // gedimmte Intensit�t ist Intensit�t * 1 / attenuation => Intesit�t / attenuation
        // Man m�chte die Distanz / radius des Lichtes nun anhand der Lichtintensit�t feststellen, da die Intensit�t aber nie 0 erreicht muss man sich auf einen Threshold festlegen.
        // Threshold = 5 / 256
        // l�st man die formel nun nach distanz auf erh�lt man mit der abc formel folgende Gleichung f�r den positiven Wert
        float radius = (-attenuation_linear + std::sqrt(attenuation_linear * attenuation_linear - 4 * attenuation_quadratic * (attenuation_constant - (256.0f / 5.0f) * maxBrightness)))
            / (2.0f * attenuation_quadratic);

        lightRadii.emplace_back(radius);
    }

    // generate normal-oriented hemisphere kernel
    constexpr int kernelsize = 64;
    std::uniform_real_distribution<float> float01(0.0, 1.0);
    std::default_random_engine generator;
    std::vector<glm::vec3> ssaoKernel;
	for (int i = 0; i < kernelsize; ++i)
	{
        glm::vec3 sample(
            float01(generator) * 2.0 - 1.0,
            float01(generator) * 2.0 - 1.0,
            float01(generator) // vec3 points in hemisphere of normal, thus z should be positive
        );
        sample = glm::normalize(sample); // normalisieren, um eine hemisphere zu erhalten
        sample = sample * float01(generator); // mit random value zwischen [0,1] multiplizieren, damit man auch Werte innerhalb der hemisphere erh�lt

		// accelerated interpolation, um mehr samples nahe am fragment zu bekommen
        float scale = static_cast<float>(i) / static_cast<float>(kernelsize);
        scale = glm::mix(0.1f, 1.0f, scale * scale);
        sample = sample * scale;

        ssaoKernel.push_back(sample);
	}

	// the hemisphere kernel should be different for each fragment
	// therefore create noise texture containing rotation vectors used to rotate the kernel (around tangent space)
    std::vector<glm::vec3> ssaoNoise;
    constexpr int noise_texture_size = 16;
	for (int i = 0; i < noise_texture_size; ++i)
	{
        glm::vec3 noise(
            float01(generator) * 2.0 - 1.0,
            float01(generator) * 2.0 - 1.0,
            0.0f
        );
        ssaoNoise.push_back(noise);
	}

    GLuint noise_texture;
    glGenTextures(1, &noise_texture); GLCheckError();
    glBindTexture(GL_TEXTURE_2D, noise_texture);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA16F, 4, 4, 0, GL_RGB, GL_FLOAT, &ssaoNoise[0]);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

    GLuint ssaoFBO;
    glGenFramebuffers(1, &ssaoFBO);
    glBindFramebuffer(GL_FRAMEBUFFER, ssaoFBO);

    GLuint ssao_color_buffer;
    glGenTextures(1, &ssao_color_buffer);
    glBindTexture(GL_TEXTURE_2D, ssao_color_buffer);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RED, SCR_WIDTH, SCR_HEIGHT, 0, GL_RED, GL_FLOAT, nullptr);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, ssao_color_buffer, 0);

	
	
	// unit sphere for drawing point lights
    Sphere s(1.0f, 25, 30);

    // render loop
    // -----------
    while (!glfwWindowShouldClose(window))
    {
        // per-frame time logic
        // --------------------
        ++total_frames;
        float currentFrame = glfwGetTime();
        delta_time = currentFrame - time_last_frame;
        time_last_frame = currentFrame;
        updatePerformanceStats();

        // input
        // -----
        processInput(window);

        // render
        // ------
        // 1. render scene into floating point framebuffer
        glBindFramebuffer(GL_FRAMEBUFFER, g_buffer);
			glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
	        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	        glDepthMask(GL_TRUE);
	        glEnable(GL_DEPTH_TEST);

	        glm::mat4 model = glm::mat4(1.0f);
	        glm::mat4 view = camera.GetViewMatrix();
	        glm::mat4 projection = glm::perspective(glm::radians(camera.fov), static_cast<GLfloat>(SCR_WIDTH) / static_cast<GLfloat>(SCR_HEIGHT), 0.1f, 100.0f);

	        glBindBuffer(GL_UNIFORM_BUFFER, ubo_matrices);
	        glBufferSubData(GL_UNIFORM_BUFFER, 0, sizeof(glm::mat4), glm::value_ptr(view));
	        glBufferSubData(GL_UNIFORM_BUFFER, sizeof(glm::mat4), sizeof(glm::mat4), glm::value_ptr(projection));
	        glBindBuffer(GL_UNIFORM_BUFFER, 0);

	        gbuffer_shader.bind();

	        // render scene into gBuffer
	        for (const glm::vec3& vec : objectPositions)
	        {
	            model = glm::mat4(1.0);
	            model = glm::translate(model, vec);
	            model = glm::scale(model, glm::vec3(0.5f));
	            gbuffer_shader.setMat4("model", model);
	            backpack.Draw(gbuffer_shader);
	        }
        glBindFramebuffer(GL_FRAMEBUFFER, 0);

    	// render shaded scene
        glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        glEnable(GL_STENCIL_TEST);

        // blit g_buffer depth into default framebuffer
        glBindFramebuffer(GL_READ_FRAMEBUFFER, g_buffer);
        glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);
        glBlitFramebuffer(0, 0, SCR_WIDTH, SCR_HEIGHT, 0, 0, SCR_WIDTH, SCR_HEIGHT, GL_DEPTH_BUFFER_BIT, GL_NEAREST);
        glBindFramebuffer(GL_FRAMEBUFFER, 0);

        for (int i = 0; i < lightPositions.size(); ++i)
        {
            // stencil pass
            glClear(GL_STENCIL_BUFFER_BIT);
            glEnable(GL_DEPTH_TEST);
            glDisable(GL_CULL_FACE);
            glDepthMask(GL_FALSE);

            // stencil test ist aktiviert und soll immer passen, da nur der depth test interessiert
            glStencilFunc(GL_ALWAYS, 0, 0);

            glStencilOpSeparate(GL_BACK, GL_KEEP, GL_INCR_WRAP, GL_KEEP);
            glStencilOpSeparate(GL_FRONT, GL_KEEP, GL_DECR_WRAP, GL_KEEP);

            null_shader.bind();
            model = glm::mat4(1.0f);
            model = glm::translate(model, lightPositions[i]);
            model = glm::scale(model, glm::vec3(lightRadii[i]));

            null_shader.setMat4("model", model);
            s.draw(null_shader);

            // render point lighting
            glDisable(GL_DEPTH_TEST);
            glEnable(GL_CULL_FACE);
            glCullFace(GL_BACK);
            glEnable(GL_BLEND);
            glBlendEquation(GL_FUNC_ADD);
            glBlendFunc(GL_ONE, GL_ONE);

            glStencilFunc(GL_NOTEQUAL, 0, 0xFF);

            lighting_pass_shader.bind();
            lighting_pass_shader.setTexture2D("gPosition", g_position, 0);
            lighting_pass_shader.setTexture2D("gNormal", g_normal, 1);
            lighting_pass_shader.setTexture2D("gAlbedoSpec", g_albedo_spec, 2);
            lighting_pass_shader.setVec3("viewPos", camera.position);


            lighting_pass_shader.setVec3("lights.Position", lightPositions[i]);
            lighting_pass_shader.setVec3("lights.Color", lightColors[i]);
            lighting_pass_shader.setFloat("lights.Linear", attenuation_linear);
            lighting_pass_shader.setFloat("lights.Quadratic", attenuation_quadratic);

            model = glm::mat4(1.0f);
            model = glm::translate(model, lightPositions[i]);
            model = glm::scale(model, glm::vec3(lightRadii[i]));
            lighting_pass_shader.setMat4("model", model);
            s.draw(lighting_pass_shader);

            glDisable(GL_BLEND);
        }
    	
        // TODO: Erstmal wird nur die SSAO textur ausgegeben
        // use g-buffer to render SSAO texture
        glBindFramebuffer(GL_FRAMEBUFFER, ssaoFBO);
	        glClear(GL_COLOR_BUFFER_BIT);
    		
	        glDepthMask(GL_TRUE);
	        glEnable(GL_DEPTH_TEST);
	        glDisable(GL_CULL_FACE);
	        glDisable(GL_STENCIL_TEST);

	        ssao_shader.bind();
	        ssao_shader.setTexture2D("gPosition", g_position, 0);
	        ssao_shader.setTexture2D("gNormal", g_normal, 1);
            ssao_shader.setTexture2D("noiseTexture", noise_texture, 2);
	        renderQuad();
    		
        glBindFramebuffer(GL_FRAMEBUFFER, 0);
    	
        // forward-render other geometry
        // ---------------------------

        //// render lights
        light_source_shader.bind();
        for (int i = 0; i < lightPositions.size(); ++i)
        {
            model = glm::mat4(1.0f);
            model = glm::translate(model, lightPositions[i]);
            model = glm::scale(model, glm::vec3(0.125f));
            light_source_shader.setMat4("model", model);
            light_source_shader.setVec3("color", lightColors[i]);
            renderCube();
        }

        // glfw: swap buffers and poll IO events (keys pressed/released, mouse moved etc.)
        // -------------------------------------------------------------------------------
        glfwSwapBuffers(window);
        glfwPollEvents();
    }

    // optional: de-allocate all resources once they've outlived their purpose:
    // ------------------------------------------------------------------------


    glfwTerminate();
    return 0;
}

// renderQuad() renders a 1x1 XY quad in NDC
// -----------------------------------------
unsigned int quadVAO = 0;
unsigned int quadVBO;
void renderQuad()
{
    if (quadVAO == 0)
    {
        float quadVertices[] = {
            // positions        // texture Coords
            -1.0f,  1.0f, 0.0f, 0.0f, 1.0f,
            -1.0f, -1.0f, 0.0f, 0.0f, 0.0f,
             1.0f,  1.0f, 0.0f, 1.0f, 1.0f,
             1.0f, -1.0f, 0.0f, 1.0f, 0.0f,
        };
        // setup plane VAO
        glGenVertexArrays(1, &quadVAO);
        glGenBuffers(1, &quadVBO);
        glBindVertexArray(quadVAO);
        glBindBuffer(GL_ARRAY_BUFFER, quadVBO);
        glBufferData(GL_ARRAY_BUFFER, sizeof(quadVertices), &quadVertices, GL_STATIC_DRAW);
        glEnableVertexAttribArray(0);
        glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void*)0);
        glEnableVertexAttribArray(1);
        glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void*)(3 * sizeof(float)));
    }
    glBindVertexArray(quadVAO);
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
    glBindVertexArray(0);
}

// renderCube() renders a 1x1 3D cube in NDC.
// -------------------------------------------------
unsigned int cubeVAO = 0;
unsigned int cubeVBO = 0;
void renderCube()
{
    // initialize (if necessary)
    if (cubeVAO == 0)
    {
        float vertices[] = {
            // back face
            -1.0f, -1.0f, -1.0f,  0.0f,  0.0f, -1.0f, 0.0f, 0.0f, // bottom-left
             1.0f,  1.0f, -1.0f,  0.0f,  0.0f, -1.0f, 1.0f, 1.0f, // top-right
             1.0f, -1.0f, -1.0f,  0.0f,  0.0f, -1.0f, 1.0f, 0.0f, // bottom-right         
             1.0f,  1.0f, -1.0f,  0.0f,  0.0f, -1.0f, 1.0f, 1.0f, // top-right
            -1.0f, -1.0f, -1.0f,  0.0f,  0.0f, -1.0f, 0.0f, 0.0f, // bottom-left
            -1.0f,  1.0f, -1.0f,  0.0f,  0.0f, -1.0f, 0.0f, 1.0f, // top-left
            // front face
            -1.0f, -1.0f,  1.0f,  0.0f,  0.0f,  1.0f, 0.0f, 0.0f, // bottom-left
             1.0f, -1.0f,  1.0f,  0.0f,  0.0f,  1.0f, 1.0f, 0.0f, // bottom-right
             1.0f,  1.0f,  1.0f,  0.0f,  0.0f,  1.0f, 1.0f, 1.0f, // top-right
             1.0f,  1.0f,  1.0f,  0.0f,  0.0f,  1.0f, 1.0f, 1.0f, // top-right
            -1.0f,  1.0f,  1.0f,  0.0f,  0.0f,  1.0f, 0.0f, 1.0f, // top-left
            -1.0f, -1.0f,  1.0f,  0.0f,  0.0f,  1.0f, 0.0f, 0.0f, // bottom-left
            // left face
            -1.0f,  1.0f,  1.0f, -1.0f,  0.0f,  0.0f, 1.0f, 0.0f, // top-right
            -1.0f,  1.0f, -1.0f, -1.0f,  0.0f,  0.0f, 1.0f, 1.0f, // top-left
            -1.0f, -1.0f, -1.0f, -1.0f,  0.0f,  0.0f, 0.0f, 1.0f, // bottom-left
            -1.0f, -1.0f, -1.0f, -1.0f,  0.0f,  0.0f, 0.0f, 1.0f, // bottom-left
            -1.0f, -1.0f,  1.0f, -1.0f,  0.0f,  0.0f, 0.0f, 0.0f, // bottom-right
            -1.0f,  1.0f,  1.0f, -1.0f,  0.0f,  0.0f, 1.0f, 0.0f, // top-right
            // right face
             1.0f,  1.0f,  1.0f,  1.0f,  0.0f,  0.0f, 1.0f, 0.0f, // top-left
             1.0f, -1.0f, -1.0f,  1.0f,  0.0f,  0.0f, 0.0f, 1.0f, // bottom-right
             1.0f,  1.0f, -1.0f,  1.0f,  0.0f,  0.0f, 1.0f, 1.0f, // top-right         
             1.0f, -1.0f, -1.0f,  1.0f,  0.0f,  0.0f, 0.0f, 1.0f, // bottom-right
             1.0f,  1.0f,  1.0f,  1.0f,  0.0f,  0.0f, 1.0f, 0.0f, // top-left
             1.0f, -1.0f,  1.0f,  1.0f,  0.0f,  0.0f, 0.0f, 0.0f, // bottom-left     
            // bottom face
            -1.0f, -1.0f, -1.0f,  0.0f, -1.0f,  0.0f, 0.0f, 1.0f, // top-right
             1.0f, -1.0f, -1.0f,  0.0f, -1.0f,  0.0f, 1.0f, 1.0f, // top-left
             1.0f, -1.0f,  1.0f,  0.0f, -1.0f,  0.0f, 1.0f, 0.0f, // bottom-left
             1.0f, -1.0f,  1.0f,  0.0f, -1.0f,  0.0f, 1.0f, 0.0f, // bottom-left
            -1.0f, -1.0f,  1.0f,  0.0f, -1.0f,  0.0f, 0.0f, 0.0f, // bottom-right
            -1.0f, -1.0f, -1.0f,  0.0f, -1.0f,  0.0f, 0.0f, 1.0f, // top-right
            // top face
            -1.0f,  1.0f, -1.0f,  0.0f,  1.0f,  0.0f, 0.0f, 1.0f, // top-left
             1.0f,  1.0f , 1.0f,  0.0f,  1.0f,  0.0f, 1.0f, 0.0f, // bottom-right
             1.0f,  1.0f, -1.0f,  0.0f,  1.0f,  0.0f, 1.0f, 1.0f, // top-right     
             1.0f,  1.0f,  1.0f,  0.0f,  1.0f,  0.0f, 1.0f, 0.0f, // bottom-right
            -1.0f,  1.0f, -1.0f,  0.0f,  1.0f,  0.0f, 0.0f, 1.0f, // top-left
            -1.0f,  1.0f,  1.0f,  0.0f,  1.0f,  0.0f, 0.0f, 0.0f  // bottom-left        
        };
        glGenVertexArrays(1, &cubeVAO);
        glGenBuffers(1, &cubeVBO);
        // fill buffer
        glBindBuffer(GL_ARRAY_BUFFER, cubeVBO);
        glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);
        // link vertex attributes
        glBindVertexArray(cubeVAO);
        glEnableVertexAttribArray(0);
        glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)0);
        glEnableVertexAttribArray(1);
        glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)(3 * sizeof(float)));
        glEnableVertexAttribArray(2);
        glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)(6 * sizeof(float)));
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        glBindVertexArray(0);
    }
    // render Cube
    glBindVertexArray(cubeVAO);
    glDrawArrays(GL_TRIANGLES, 0, 36);
    glBindVertexArray(0);
}

// process all input: query GLFW whether relevant keys are pressed/released this frame and react accordingly
// ---------------------------------------------------------------------------------------------------------

void processInput(GLFWwindow* window)
{
    if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
        glfwSetWindowShouldClose(window, true);

    if (flyingCameraActive)
    {
        if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS)
            camera.Move(Camera_Movement::FORWARD, delta_time);
        if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS)
            camera.Move(Camera_Movement::BACKWARD, delta_time);
        if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS)
            camera.Move(Camera_Movement::LEFT, delta_time);
        if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS)
            camera.Move(Camera_Movement::RIGHT, delta_time);
        if (glfwGetKey(window, GLFW_KEY_E) == GLFW_PRESS)
            camera.Move(Camera_Movement::UP, delta_time);
        if (glfwGetKey(window, GLFW_KEY_Q) == GLFW_PRESS)
            camera.Move(Camera_Movement::DOWN, delta_time);
    }

    if (glfwGetKey(window, GLFW_KEY_R) == GLFW_PRESS)
    {
        camera.Reset();
    }

}

void mouse_button_callback(GLFWwindow* window, int button, int action, int mods)
{
    if (button == GLFW_MOUSE_BUTTON_RIGHT && action == GLFW_PRESS)
    {
        glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
        firstMouse = true;
        flyingCameraActive = true;
    }
    else if (button == GLFW_MOUSE_BUTTON_RIGHT && action == GLFW_RELEASE)
    {
        glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_NORMAL);
        firstMouse = false;
        flyingCameraActive = false;
    }
}

// glfw: whenever the window size changed (by OS or user resize) this callback function executes
// ---------------------------------------------------------------------------------------------
void framebuffer_size_callback(GLFWwindow* window, int width, int height)
{
    // make sure the viewport matches the new window dimensions; note that width and 
    // height will be significantly larger than specified on retina displays.
    glViewport(0, 0, width, height);
}

// glfw: whenever the mouse moves, this callback is called
// -------------------------------------------------------
void mouse_callback(GLFWwindow* window, double xpos, double ypos)
{
    if (flyingCameraActive)
    {
        if (firstMouse)
        {
            lastX = xpos;
            lastY = ypos;
            firstMouse = false;
        }

        float xoffset = xpos - lastX;
        float yoffset = lastY - ypos; // reversed since y-coordinates go from bottom to top

        lastX = xpos;
        lastY = ypos;

        camera.RotateWithMouse(xoffset, yoffset);
    }
}

// glfw: whenever the mouse scroll wheel scrolls, this callback is called
// ----------------------------------------------------------------------
void scroll_callback(GLFWwindow* window, double xoffset, double yoffset)
{
    camera.Zoom(yoffset);
}

// utility function for loading a 2D texture from file
// ---------------------------------------------------
unsigned int loadTexture(char const* path, bool gammaCorrection)
{
    unsigned int textureID;
    glGenTextures(1, &textureID);

    int width, height, nrComponents;
    unsigned char* data = stbi_load(path, &width, &height, &nrComponents, 0);
    if (data)
    {
        GLenum internalFormat;
        GLenum dataFormat;
        if (nrComponents == 1)
        {
            internalFormat = dataFormat = GL_RED;
        }
        else if (nrComponents == 3)
        {
            internalFormat = gammaCorrection ? GL_SRGB : GL_RGB;
            dataFormat = GL_RGB;
        }
        else if (nrComponents == 4)
        {
            internalFormat = gammaCorrection ? GL_SRGB_ALPHA : GL_RGBA;
            dataFormat = GL_RGBA;
        }

        glBindTexture(GL_TEXTURE_2D, textureID);
        glTexImage2D(GL_TEXTURE_2D, 0, internalFormat, width, height, 0, dataFormat, GL_UNSIGNED_BYTE, data);
        glGenerateMipmap(GL_TEXTURE_2D);

        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

        stbi_image_free(data);
    }
    else
    {
        std::cout << "Texture failed to load at path: " << path << std::endl;
        stbi_image_free(data);
    }

    return textureID;
}

