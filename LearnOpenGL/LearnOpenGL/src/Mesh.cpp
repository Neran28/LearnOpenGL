#include "Mesh.h"
#include "GLDebug.h"

Mesh::Mesh(const std::vector<Vertex>& vertices, const std::vector<unsigned int>& indices, const std::vector<Texture>& textures)
	: vertices(vertices), indices(indices), textures(textures)
{
	setupMesh();
}

void Mesh::Draw(Shader& shader)
{
	unsigned int diffuseNr = 1;
	unsigned int specularNr = 1;
	unsigned int normalNr = 1;
	unsigned int heightNr = 1;
	for (unsigned int i = 0; i < textures.size(); ++i) {
		glActiveTexture(GL_TEXTURE0 + i); GLCheckError();

		std::string number;
		std::string name;
		Texture::Type type = textures[i].type;
		if (type == Texture::Type::DIFFUSE)
		{
			number = std::to_string(diffuseNr++);
			name = "texture_diffuse";
		}
		else if (type == Texture::Type::SPECULAR)
		{
			number = std::to_string(specularNr++);
			name = "texture_specular";
		}
		else if (type == Texture::Type::NORMAL)
		{
			number = std::to_string(normalNr++);
			name = "texture_normal";
		}
		else if (type == Texture::Type::NORMAL)
		{
			number = std::to_string(heightNr++);
			name = "texture_height";
		}
		std::string uniformName;
		uniformName.append(name).append(number);
		shader.setInt(uniformName, i);

		glBindTexture(GL_TEXTURE_2D, textures[i].id); GLCheckError();
	}
	glActiveTexture(GL_TEXTURE0); GLCheckError();

	// draw mesh
	glBindVertexArray(VAO); GLCheckError();
	glDrawElements(GL_TRIANGLES, indices.size(), GL_UNSIGNED_INT, nullptr); GLCheckError();
	glBindVertexArray(0); GLCheckError();

}

void Mesh::setupMesh()
{
	glGenVertexArrays(1, &VAO); GLCheckError();
	glGenBuffers(1, &VBO); GLCheckError();
	glGenBuffers(1, &EBO); GLCheckError();

	glBindVertexArray(VAO); GLCheckError();
	glBindBuffer(GL_ARRAY_BUFFER, VBO); GLCheckError();
	// A great thing about structs is that their memory layout is sequential for all its items.
	// The effect is that we can simply pass a pointer to the struct and it translates perfectly to a glm::vec3/2 array which
	// again translates to 3/2 floats which translates to a byte array.
	glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(Vertex), &vertices[0], GL_STATIC_DRAW); GLCheckError();

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO); GLCheckError();
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size() * sizeof(GLuint), &indices[0], GL_STATIC_DRAW); GLCheckError();

	// vertex positions
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)0); GLCheckError();
	// vertex normals
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)offsetof(Vertex, normal)); GLCheckError();
	// vertex texture coords
	glEnableVertexAttribArray(2);
	glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)offsetof(Vertex, tex_coord)); GLCheckError();
	// vertex tangent
	glEnableVertexAttribArray(3);
	glVertexAttribPointer(3, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)offsetof(Vertex, tangent)); GLCheckError();
	// vertex bitangent
	glEnableVertexAttribArray(4);
	glVertexAttribPointer(4, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)offsetof(Vertex, bitangent)); GLCheckError();

	glBindVertexArray(0); GLCheckError();
}
