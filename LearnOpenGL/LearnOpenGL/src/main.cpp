#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <assimp/scene.h>
#include <iostream>
#include <sstream>
#include <fstream>
#include <array>

#include "vendor/stb_image/stb_image.h"
#include "vendor/glm/glm.hpp"
#include "vendor/glm/gtc/matrix_transform.hpp"
#include "vendor/glm/gtc/type_ptr.hpp"

#include "GLDebug.h"
#include "Shader.h"
#include "FlyCamera.h"
#include "Model.h"


GLFWwindow* window;
const unsigned int SCR_WIDTH = 800;
const unsigned int SCR_HEIGHT = 600;
std::string title = "";

GLuint VBO, skyboxVBO;
GLuint VAO, skyboxVAO;
GLuint pointLightVAO;
GLuint texture_container;
GLuint texture_smiley;
GLuint texture_container2;
GLuint texture_container2_specular;
GLuint skybox;

std::shared_ptr<Shader> shader_lit;
std::shared_ptr<Shader> shader_white;
std::shared_ptr<Shader> shader_skybox;
std::shared_ptr<Shader> shader_reflective;
std::shared_ptr<Shader> shader_explosion;
std::shared_ptr<Shader> shader_normal_vis;
GLuint ubo_matrices;

glm::mat4 model;
glm::mat4 view;
glm::mat4 projection;
GLint myColorUniformLocation;
GLint modelMatLocation;
GLint viewMatLocation;
GLint projectionMatLocation;

const float cube_data[] = {
	// positions          // normals           // texture coords
	-0.5f, -0.5f, -0.5f,  0.0f,  0.0f, -1.0f,  0.0f, 0.0f,
	 0.5f, -0.5f, -0.5f,  0.0f,  0.0f, -1.0f,  1.0f, 0.0f,
	 0.5f,  0.5f, -0.5f,  0.0f,  0.0f, -1.0f,  1.0f, 1.0f,
	 0.5f,  0.5f, -0.5f,  0.0f,  0.0f, -1.0f,  1.0f, 1.0f,
	-0.5f,  0.5f, -0.5f,  0.0f,  0.0f, -1.0f,  0.0f, 1.0f,
	-0.5f, -0.5f, -0.5f,  0.0f,  0.0f, -1.0f,  0.0f, 0.0f,

	-0.5f, -0.5f,  0.5f,  0.0f,  0.0f, 1.0f,   0.0f, 0.0f,
	 0.5f, -0.5f,  0.5f,  0.0f,  0.0f, 1.0f,   1.0f, 0.0f,
	 0.5f,  0.5f,  0.5f,  0.0f,  0.0f, 1.0f,   1.0f, 1.0f,
	 0.5f,  0.5f,  0.5f,  0.0f,  0.0f, 1.0f,   1.0f, 1.0f,
	-0.5f,  0.5f,  0.5f,  0.0f,  0.0f, 1.0f,   0.0f, 1.0f,
	-0.5f, -0.5f,  0.5f,  0.0f,  0.0f, 1.0f,   0.0f, 0.0f,

	-0.5f,  0.5f,  0.5f, -1.0f,  0.0f,  0.0f,  1.0f, 0.0f,
	-0.5f,  0.5f, -0.5f, -1.0f,  0.0f,  0.0f,  1.0f, 1.0f,
	-0.5f, -0.5f, -0.5f, -1.0f,  0.0f,  0.0f,  0.0f, 1.0f,
	-0.5f, -0.5f, -0.5f, -1.0f,  0.0f,  0.0f,  0.0f, 1.0f,
	-0.5f, -0.5f,  0.5f, -1.0f,  0.0f,  0.0f,  0.0f, 0.0f,
	-0.5f,  0.5f,  0.5f, -1.0f,  0.0f,  0.0f,  1.0f, 0.0f,

	 0.5f,  0.5f,  0.5f,  1.0f,  0.0f,  0.0f,  1.0f, 0.0f,
	 0.5f,  0.5f, -0.5f,  1.0f,  0.0f,  0.0f,  1.0f, 1.0f,
	 0.5f, -0.5f, -0.5f,  1.0f,  0.0f,  0.0f,  0.0f, 1.0f,
	 0.5f, -0.5f, -0.5f,  1.0f,  0.0f,  0.0f,  0.0f, 1.0f,
	 0.5f, -0.5f,  0.5f,  1.0f,  0.0f,  0.0f,  0.0f, 0.0f,
	 0.5f,  0.5f,  0.5f,  1.0f,  0.0f,  0.0f,  1.0f, 0.0f,

	-0.5f, -0.5f, -0.5f,  0.0f, -1.0f,  0.0f,  0.0f, 1.0f,
	 0.5f, -0.5f, -0.5f,  0.0f, -1.0f,  0.0f,  1.0f, 1.0f,
	 0.5f, -0.5f,  0.5f,  0.0f, -1.0f,  0.0f,  1.0f, 0.0f,
	 0.5f, -0.5f,  0.5f,  0.0f, -1.0f,  0.0f,  1.0f, 0.0f,
	-0.5f, -0.5f,  0.5f,  0.0f, -1.0f,  0.0f,  0.0f, 0.0f,
	-0.5f, -0.5f, -0.5f,  0.0f, -1.0f,  0.0f,  0.0f, 1.0f,

	-0.5f,  0.5f, -0.5f,  0.0f,  1.0f,  0.0f,  0.0f, 1.0f,
	 0.5f,  0.5f, -0.5f,  0.0f,  1.0f,  0.0f,  1.0f, 1.0f,
	 0.5f,  0.5f,  0.5f,  0.0f,  1.0f,  0.0f,  1.0f, 0.0f,
	 0.5f,  0.5f,  0.5f,  0.0f,  1.0f,  0.0f,  1.0f, 0.0f,
	-0.5f,  0.5f,  0.5f,  0.0f,  1.0f,  0.0f,  0.0f, 0.0f,
	-0.5f,  0.5f, -0.5f,  0.0f,  1.0f,  0.0f,  0.0f, 1.0f
};

const glm::vec3 cubePositions[] = {
	glm::vec3(0.0f,  0.0f,  0.0f),
	glm::vec3(2.0f,  5.0f, -15.0f),
	glm::vec3(-1.5f, -2.2f, -2.5f),
	glm::vec3(-3.8f, -2.0f, -12.3f),
	glm::vec3(2.4f, -0.4f, -3.5f),
	glm::vec3(-1.7f,  3.0f, -7.5f),
	glm::vec3(1.3f, -2.0f, -2.5f),
	glm::vec3(1.5f,  2.0f, -2.5f),
	glm::vec3(1.5f,  0.2f, -1.5f),
	glm::vec3(-1.3f,  1.0f, -1.5f)
};

std::array<glm::vec3, 5> pointLightPosition = {
	glm::vec3(0.7f,  0.2f,  2.0f),
	glm::vec3(2.3f, -3.3f, -4.0f),
	glm::vec3(-4.0f,  2.0f, -12.0f),
	glm::vec3(0.0f,  0.0f, -3.0f)
};

const std::vector<std::string> skybox_faces = { "right", "left", "top", "bottom", "front", "back" };

const float skyboxVertices[] = {
	// positions          
	-1.0f,  1.0f, -1.0f,
	-1.0f, -1.0f, -1.0f,
	 1.0f, -1.0f, -1.0f,
	 1.0f, -1.0f, -1.0f,
	 1.0f,  1.0f, -1.0f,
	-1.0f,  1.0f, -1.0f,

	-1.0f, -1.0f,  1.0f,
	-1.0f, -1.0f, -1.0f,
	-1.0f,  1.0f, -1.0f,
	-1.0f,  1.0f, -1.0f,
	-1.0f,  1.0f,  1.0f,
	-1.0f, -1.0f,  1.0f,

	 1.0f, -1.0f, -1.0f,
	 1.0f, -1.0f,  1.0f,
	 1.0f,  1.0f,  1.0f,
	 1.0f,  1.0f,  1.0f,
	 1.0f,  1.0f, -1.0f,
	 1.0f, -1.0f, -1.0f,

	-1.0f, -1.0f,  1.0f,
	-1.0f,  1.0f,  1.0f,
	 1.0f,  1.0f,  1.0f,
	 1.0f,  1.0f,  1.0f,
	 1.0f, -1.0f,  1.0f,
	-1.0f, -1.0f,  1.0f,

	-1.0f,  1.0f, -1.0f,
	 1.0f,  1.0f, -1.0f,
	 1.0f,  1.0f,  1.0f,
	 1.0f,  1.0f,  1.0f,
	-1.0f,  1.0f,  1.0f,
	-1.0f,  1.0f, -1.0f,

	-1.0f, -1.0f, -1.0f,
	-1.0f, -1.0f,  1.0f,
	 1.0f, -1.0f, -1.0f,
	 1.0f, -1.0f, -1.0f,
	-1.0f, -1.0f,  1.0f,
	 1.0f, -1.0f,  1.0f
};

// time variables
double delta_time = 0;
double time_lastFrame = 0;
double frame_time;
long totalFrames = 0;
float fps[30]{};
double time_fps = 0;

// camera data
FlyCamera camera(glm::vec3(0.0f, 0.0f, 3.0f));
bool flyingCameraActive = false;

// mouse data
float mouse[2] = { 400, 300 };
bool firstDown = false;

std::shared_ptr<Model> backpack;


void scroll_callback(GLFWwindow* window, double xOffset, double yOffset);
void mouse_button_callback(GLFWwindow* window, int button, int action, int mods);
void cursor_callback(GLFWwindow* window, double xPos, double yPos);
void framebuffer_size_callback(GLFWwindow* window, int width, int height);
void processInput(GLFWwindow* window);
GLuint loadTexture(char const* path);
GLuint loadCubemap(const std::vector<std::string>& faces);
GLuint load_cubemap_from_path(const std::string& path, const std::vector<std::string>& skybox_faces);

void init();
void draw();
void setupShader();
void setupGeometry();

int main(int argc, char** argv)
{
	glfwInit();
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	
	title = "LearnOpenGL";

	window = glfwCreateWindow(SCR_WIDTH, SCR_HEIGHT, title.c_str(), nullptr, nullptr);
	if (window == nullptr)
	{
		std::cout << "Failed to create GLFW window" << std::endl;
		glfwTerminate();
		return -1;
	}

	glfwMakeContextCurrent(window);

	if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
	{
		std::cout << "Failed to initialize GLAD" << std::endl;
		return -1;
	}

	glViewport(0, 0, SCR_WIDTH, SCR_HEIGHT);
	glfwSetWindowSizeCallback(window, framebuffer_size_callback);
	glfwSetCursorPosCallback(window, cursor_callback);
	glfwSetMouseButtonCallback(window, mouse_button_callback);
	glfwSetScrollCallback(window, scroll_callback);

	init();
	setupShader();
	setupGeometry();


	while (!glfwWindowShouldClose(window))
	{
		processInput(window);

		draw();

		glfwPollEvents();
	}

	glDeleteVertexArrays(1, &VAO);
	glDeleteVertexArrays(1, &pointLightVAO);
	glDeleteBuffers(1, &VBO);
	
	glfwTerminate();
	return 0;
}

void init()
{
	const GLubyte* glVersion = glGetString(GL_VERSION); GLCheckError();
	const GLubyte* slVersion = glGetString(GL_SHADING_LANGUAGE_VERSION); GLCheckError();
	std::cout << glVersion << std::endl;
	std::cout << slVersion << std::endl;

	GLint maxAttribs;
	glGetIntegerv(GL_MAX_VERTEX_ATTRIBS, &maxAttribs); GLCheckError();
	std::cout << "Maximum nr of vertex attributes supported: " << maxAttribs << std::endl;

	glfwSwapInterval(0);
	stbi_set_flip_vertically_on_load(true);
	glEnable(GL_DEPTH_TEST); GLCheckError();
}

void updatePerformanceStats()
{
	const int frame = totalFrames % (sizeof(fps) / sizeof(fps[0]));
	fps[frame] = (float)(1.0 / delta_time);

	if ((time_lastFrame - time_fps) > 0.5) {
		time_fps = time_lastFrame;
		float sum = 0;
		for (int i = 0; i < (sizeof(fps) / sizeof(fps[0])); ++i)
			sum += fps[i];

		std::stringstream s;
		float roundedFPS = (int)(sum / (sizeof(fps) / sizeof(fps[0])) * 100 + 0.5f);
		roundedFPS = (float)roundedFPS / 100;
		s << title << " - FPS: " << roundedFPS << "    RenderTime: " << frame_time;
		glfwSetWindowTitle(window, s.str().c_str());
	}
}

void draw()
{
	++totalFrames;
	const double t = glfwGetTime();
	delta_time = t - time_lastFrame;
	time_lastFrame = t;
	updatePerformanceStats();

	glClearColor(0.1f, 0.1f, 0.1f, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); GLCheckError();

	view = glm::mat4(1.0f);
	view = camera.GetViewMatrix();
	projection = glm::perspective(glm::radians(camera.fov), (float)SCR_WIDTH / (float)SCR_HEIGHT, 0.1f, 100.0f);
	
	// lit, white und reflection shader nutzen das
	glBindBuffer(GL_UNIFORM_BUFFER, ubo_matrices);
	glBufferSubData(GL_UNIFORM_BUFFER, 0, sizeof(glm::mat4), glm::value_ptr(view));
	glBufferSubData(GL_UNIFORM_BUFFER, sizeof(glm::mat4), sizeof(glm::mat4), glm::value_ptr(projection));
	glBindBuffer(GL_UNIFORM_BUFFER, 0);
	
	const glm::vec3 lightColor = glm::vec3(1);
	const glm::vec3 diffuseColor = lightColor * glm::vec3(0.8f);
	const glm::vec3 ambientColor = lightColor * glm::vec3(0.05f);

	// draw lights
	shader_white->bind();
	glBindVertexArray(pointLightVAO); GLCheckError();
	for (int i = 0; i < pointLightPosition.size(); ++i) {
		model = glm::mat4(1.0f);
		model = glm::translate(model, pointLightPosition[i]);
		model = glm::scale(model, glm::vec3(0.2f));

		shader_white->setMat4("model", model);
		glDrawArrays(GL_TRIANGLES, 0, 36);
	}

	shader_lit->bind();
	shader_lit->setVec3("viewPos", camera.position);
	shader_lit->setInt("material.texture_diffuse1", 0);
	shader_lit->setInt("material.texture_specular1", 1);
	shader_lit->setFloat("material.shininess", 100.0f);

	shader_lit->setVec3("dirLight.direction", -1.0f, -0.8f, 0.0f);
	shader_lit->setVec3("dirLight.ambient", ambientColor);
	shader_lit->setVec3("dirLight.diffuse", 0.4f, 0.4f, 0.4f);
	shader_lit->setVec3("dirLight.specular", 0.5f, 0.5f, 0.5f);

	shader_lit->setVec3("pointLights[0].position", pointLightPosition[0]);
	shader_lit->setVec3("pointLights[0].ambient", ambientColor);
	shader_lit->setVec3("pointLights[0].diffuse", diffuseColor);
	shader_lit->setVec3("pointLights[0].specular", 1.0f, 1.0f, 1.0f);
	shader_lit->setFloat("pointLights[0].attenuation_constant", 1.0f);
	shader_lit->setFloat("pointLights[0].attenuation_linear", 0.09f);
	shader_lit->setFloat("pointLights[0].attenuation_quadratic", 0.032f);

	shader_lit->setVec3("pointLights[1].position", pointLightPosition[1]);
	shader_lit->setVec3("pointLights[1].ambient", ambientColor);
	shader_lit->setVec3("pointLights[1].diffuse", diffuseColor);
	shader_lit->setVec3("pointLights[1].specular", 1.0f, 1.0f, 1.0f);
	shader_lit->setFloat("pointLights[1].attenuation_constant", 1.0f);
	shader_lit->setFloat("pointLights[1].attenuation_linear", 0.09f);
	shader_lit->setFloat("pointLights[1].attenuation_quadratic", 0.032f);

	shader_lit->setVec3("pointLights[2].position", pointLightPosition[2]);
	shader_lit->setVec3("pointLights[2].ambient", ambientColor);
	shader_lit->setVec3("pointLights[2].diffuse", diffuseColor);
	shader_lit->setVec3("pointLights[2].specular", 1.0f, 1.0f, 1.0f);
	shader_lit->setFloat("pointLights[2].attenuation_constant", 1.0f);
	shader_lit->setFloat("pointLights[2].attenuation_linear", 0.09f);
	shader_lit->setFloat("pointLights[2].attenuation_quadratic", 0.032f);

	shader_lit->setVec3("pointLights[3].position", pointLightPosition[3]);
	shader_lit->setVec3("pointLights[3].ambient", ambientColor);
	shader_lit->setVec3("pointLights[3].diffuse", diffuseColor);
	shader_lit->setVec3("pointLights[3].specular", 1.0f, 1.0f, 1.0f);
	shader_lit->setFloat("pointLights[3].attenuation_constant", 1.0f);
	shader_lit->setFloat("pointLights[3].attenuation_linear", 0.09f);
	shader_lit->setFloat("pointLights[3].attenuation_quadratic", 0.032f);

	shader_lit->setVec3("spotLight.position", camera.position);
	shader_lit->setVec3("spotLight.direction", camera.front);
	shader_lit->setVec3("spotLight.ambient", 0.0f, 0.0f, 0.0f);
	shader_lit->setVec3("spotLight.diffuse", 1.0f, 1.0f, 1.0f);
	shader_lit->setVec3("spotLight.specular", 1.0f, 1.0f, 1.0f);
	shader_lit->setFloat("spotLight.attenuation_constant", 1.0f);
	shader_lit->setFloat("spotLight.attenuation_linear", 0.09f);
	shader_lit->setFloat("spotLight.attenuation_quadratic", 0.032f);
	shader_lit->setFloat("spotLight.cutOff", glm::cos(glm::radians(12.5f)));
	shader_lit->setFloat("spotLight.outerCutOff", glm::cos(glm::radians(15.0f)));

	// draw cubes
	glBindVertexArray(VAO); GLCheckError();
	glActiveTexture(GL_TEXTURE0); GLCheckError();
	glBindTexture(GL_TEXTURE_2D, texture_container2); GLCheckError();
	glActiveTexture(GL_TEXTURE1); GLCheckError();
	glBindTexture(GL_TEXTURE_2D, texture_container2_specular); GLCheckError();

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_CUBE_MAP, skybox);

	for (unsigned int i = 0; i < 10; ++i) {
		const float angle = 20.0f * i;
		model = glm::mat4(1.0f);
		model = glm::translate(model, cubePositions[i]);
		model = glm::rotate(model, glm::radians(angle), glm::vec3(1.0f, 0.3f, 0.5f));
		shader_lit->bind();
		shader_lit->setMat4("model", model);

		glDrawArrays(GL_TRIANGLES, 0, 36); GLCheckError();

		shader_normal_vis->bind();
		shader_normal_vis->setMat4("model", model);
		glDrawArrays(GL_TRIANGLES, 0, 36); GLCheckError();
	}

	// draw reflective backpack
	shader_reflective->bind();
	shader_reflective->setInt("skybox", 0);
	shader_reflective->setVec3("cameraPos", camera.position);
	model = glm::mat4(1.0f);
	model = glm::translate(model, glm::vec3(0.0f, 0.0f, 10.0f));
	model = glm::scale(model, glm::vec3(0.5f, 0.5f, 0.5f));
	shader_reflective->setMat4("model", model);
	backpack->Draw(*shader_reflective);

	// draw exploding backpack
	shader_explosion->bind();
	shader_explosion->setVec3("viewPos", camera.position);
	shader_explosion->setFloat("material.shininess", 100.0f);

	shader_explosion->setVec3("dirLight.direction", -1.0f, -0.8f, 0.0f);
	shader_explosion->setVec3("dirLight.ambient", ambientColor);
	shader_explosion->setVec3("dirLight.diffuse", 0.4f, 0.4f, 0.4f);
	shader_explosion->setVec3("dirLight.specular", 0.5f, 0.5f, 0.5f);

	shader_explosion->setVec3("pointLights[0].position", pointLightPosition[0]);
	shader_explosion->setVec3("pointLights[0].ambient", ambientColor);
	shader_explosion->setVec3("pointLights[0].diffuse", diffuseColor);
	shader_explosion->setVec3("pointLights[0].specular", 1.0f, 1.0f, 1.0f);
	shader_explosion->setFloat("pointLights[0].attenuation_constant", 1.0f);
	shader_explosion->setFloat("pointLights[0].attenuation_linear", 0.09f);
	shader_explosion->setFloat("pointLights[0].attenuation_quadratic", 0.032f);

	shader_explosion->setVec3("pointLights[1].position", pointLightPosition[1]);
	shader_explosion->setVec3("pointLights[1].ambient", ambientColor);
	shader_explosion->setVec3("pointLights[1].diffuse", diffuseColor);
	shader_explosion->setVec3("pointLights[1].specular", 1.0f, 1.0f, 1.0f);
	shader_explosion->setFloat("pointLights[1].attenuation_constant", 1.0f);
	shader_explosion->setFloat("pointLights[1].attenuation_linear", 0.09f);
	shader_explosion->setFloat("pointLights[1].attenuation_quadratic", 0.032f);

	shader_explosion->setVec3("pointLights[2].position", pointLightPosition[2]);
	shader_explosion->setVec3("pointLights[2].ambient", ambientColor);
	shader_explosion->setVec3("pointLights[2].diffuse", diffuseColor);
	shader_explosion->setVec3("pointLights[2].specular", 1.0f, 1.0f, 1.0f);
	shader_explosion->setFloat("pointLights[2].attenuation_constant", 1.0f);
	shader_explosion->setFloat("pointLights[2].attenuation_linear", 0.09f);
	shader_explosion->setFloat("pointLights[2].attenuation_quadratic", 0.032f);

	shader_explosion->setVec3("pointLights[3].position", pointLightPosition[3]);
	shader_explosion->setVec3("pointLights[3].ambient", ambientColor);
	shader_explosion->setVec3("pointLights[3].diffuse", diffuseColor);
	shader_explosion->setVec3("pointLights[3].specular", 1.0f, 1.0f, 1.0f);
	shader_explosion->setFloat("pointLights[3].attenuation_constant", 1.0f);
	shader_explosion->setFloat("pointLights[3].attenuation_linear", 0.09f);
	shader_explosion->setFloat("pointLights[3].attenuation_quadratic", 0.032f);

	shader_explosion->setVec3("spotLight.position", camera.position);
	shader_explosion->setVec3("spotLight.direction", camera.front);
	shader_explosion->setVec3("spotLight.ambient", 0.0f, 0.0f, 0.0f);
	shader_explosion->setVec3("spotLight.diffuse", 1.0f, 1.0f, 1.0f);
	shader_explosion->setVec3("spotLight.specular", 1.0f, 1.0f, 1.0f);
	shader_explosion->setFloat("spotLight.attenuation_constant", 1.0f);
	shader_explosion->setFloat("spotLight.attenuation_linear", 0.09f);
	shader_explosion->setFloat("spotLight.attenuation_quadratic", 0.032f);
	shader_explosion->setFloat("spotLight.cutOff", glm::cos(glm::radians(12.5f)));
	shader_explosion->setFloat("spotLight.outerCutOff", glm::cos(glm::radians(15.0f)));

	shader_explosion->setFloat("time", glfwGetTime());

	model = glm::mat4(1.0f);
	model = glm::translate(model, glm::vec3(12.0f, 0.0f, 12.0f));
	model = glm::scale(model, glm::vec3(0.5f, 0.5f, 0.5f));
	shader_explosion->setMat4("model", model);
	backpack->Draw(*shader_explosion);

	// draw skybox last to save fragment calls depth is handled in shader
	glDepthFunc(GL_LEQUAL);
	view = glm::mat4(glm::mat3(camera.GetViewMatrix())); // drop translation part
	shader_skybox->bind();
	shader_skybox->setMat4("view", view);
	shader_skybox->setMat4("projection", projection);
	shader_skybox->setInt("cubemap", 0);
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_CUBE_MAP, skybox);
	glBindVertexArray(skyboxVAO);
	glDrawArrays(GL_TRIANGLES, 0, 36);
	// restore states
	glDepthFunc(GL_LESS);
	
	glBindVertexArray(0); GLCheckError();

	glfwSwapBuffers(window);
	
	frame_time = glfwGetTime() - t;
}


void setupShader()
{
	shader_lit = std::make_shared<Shader>("res/shaders/lit.vert", "res/shaders/lit.frag");
	shader_white = std::make_shared<Shader>("res/shaders/white.vert", "res/shaders/white.frag");
	shader_skybox = std::make_shared<Shader>("res/shaders/skybox.vert", "res/shaders/skybox.frag");
	shader_reflective = std::make_shared<Shader>("res/shaders/environmental_refl_refr.vert", "res/shaders/environmental_refl_refr.frag");
	shader_explosion = std::make_shared<Shader>("res/shaders/explode.vert", "res/shaders/explode.geom", "res/shaders/explode_lit.frag");
	shader_normal_vis = std::make_shared<Shader>("res/shaders/normal_visualization.vert", "res/shaders/normal_visualization.geom", "res/shaders/normal_visualization.frag");
	
	glGenBuffers(1, &ubo_matrices);
	glBindBuffer(GL_UNIFORM_BUFFER, ubo_matrices);
	glBufferData(GL_UNIFORM_BUFFER, 2 * sizeof(glm::mat4), nullptr, GL_STATIC_DRAW);
	glBindBuffer(GL_UNIFORM_BUFFER, 0);
	glBindBufferBase(GL_UNIFORM_BUFFER, 1, ubo_matrices);
	
	shader_lit->bind();
	modelMatLocation = glGetUniformLocation(shader_lit->m_rendererID, "model"); GLCheckError();
	viewMatLocation = glGetUniformLocation(shader_lit->m_rendererID, "view"); GLCheckError();
	projectionMatLocation = glGetUniformLocation(shader_lit->m_rendererID, "projection"); GLCheckError();
	shader_lit->unbind();

	// man kann im shader ab 420 das binding reinschreiben, damit f�llt das GetUniform() weg
	unsigned int matrices_index = glGetUniformBlockIndex(shader_lit->m_rendererID, "Matrices");
	glUniformBlockBinding(shader_lit->m_rendererID, matrices_index, 1);
	matrices_index = glGetUniformBlockIndex(shader_white->m_rendererID, "Matrices");
	glUniformBlockBinding(shader_white->m_rendererID, matrices_index, 1);
	matrices_index = glGetUniformBlockIndex(shader_reflective->m_rendererID, "Matrices");
	glUniformBlockBinding(shader_reflective->m_rendererID, matrices_index, 1);
	matrices_index = glGetUniformBlockIndex(shader_explosion->m_rendererID, "Matrices");
	glUniformBlockBinding(shader_explosion->m_rendererID, matrices_index, 1);
	matrices_index = glGetUniformBlockIndex(shader_normal_vis->m_rendererID, "Matrices");
	glUniformBlockBinding(shader_normal_vis->m_rendererID, matrices_index, 1);
	
}

void setupGeometry()
{
	glGenVertexArrays(1, &VAO); GLCheckError();
	glGenVertexArrays(1, &skyboxVAO); GLCheckError();
	glGenVertexArrays(1, &pointLightVAO); GLCheckError();

	glGenBuffers(1, &VBO); GLCheckError();
	glGenBuffers(1, &skyboxVBO); GLCheckError();

	glBindVertexArray(VAO); GLCheckError();
	glBindBuffer(GL_ARRAY_BUFFER, VBO); GLCheckError();
	glBufferData(GL_ARRAY_BUFFER, sizeof(cube_data), cube_data, GL_STATIC_DRAW); GLCheckError();
	// positions
	glEnableVertexAttribArray(0); GLCheckError();
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)0); GLCheckError();
	// normals
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)(3 * sizeof(float))); GLCheckError();
	glEnableVertexAttribArray(1); GLCheckError();
	// tex coords
	glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)(6 * sizeof(float))); GLCheckError();
	glEnableVertexAttribArray(2); GLCheckError();

	// light source mesh
	glBindVertexArray(pointLightVAO); GLCheckError();
	glBindBuffer(GL_ARRAY_BUFFER, VBO); GLCheckError();
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)0); GLCheckError();
	glEnableVertexAttribArray(0);

	glBindVertexArray(skyboxVAO);
	glBindBuffer(GL_ARRAY_BUFFER, skyboxVBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(skyboxVertices), skyboxVertices, GL_STATIC_DRAW); GLCheckError();
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);

	glBindVertexArray(0);

	
	
	texture_container = loadTexture("res/textures/container.jpg");
	texture_smiley = loadTexture("res/textures/awesomeface.png");
	texture_container2 = loadTexture("res/textures/container2.png");
	texture_container2_specular = loadTexture("res/textures/container2_specular.png");

	backpack = std::make_shared<Model>("res/Assets/Models/backpack/backpack.obj");

	skybox = load_cubemap_from_path("res/textures/skybox/", skybox_faces);
}

void processInput(GLFWwindow* window)
{
	if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
		glfwSetWindowShouldClose(window, true);

	if (glfwGetKey(window, GLFW_KEY_L) == GLFW_PRESS)
			glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

	if (glfwGetKey(window, GLFW_KEY_P) == GLFW_PRESS)
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

	if (flyingCameraActive)
	{
		if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS)
			camera.Move(Camera_Movement::FORWARD, delta_time);
		if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS)
			camera.Move(Camera_Movement::BACKWARD, delta_time);
		if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS)
			camera.Move(Camera_Movement::LEFT, delta_time);
		if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS)
			camera.Move(Camera_Movement::RIGHT, delta_time);
		if (glfwGetKey(window, GLFW_KEY_E) == GLFW_PRESS)
			camera.Move(Camera_Movement::UP, delta_time);
		if (glfwGetKey(window, GLFW_KEY_Q) == GLFW_PRESS)
			camera.Move(Camera_Movement::DOWN, delta_time);
	}

	if (glfwGetKey(window, GLFW_KEY_R) == GLFW_PRESS)
	{
		camera.Reset();
	}
}

void scroll_callback(GLFWwindow* window, double xOffset, double yOffset)
{
	camera.Zoom(yOffset);
}

void cursor_callback(GLFWwindow* window, double xPos, double yPos)
{
	if (flyingCameraActive)
	{
		if (firstDown)
		{
			mouse[0] = xPos;
			mouse[1] = yPos;
			firstDown = false;
		}
		float xOffset = xPos - mouse[0];
		float yOffset = mouse[1] - yPos; // reversed since y-coordinates range from top to bottom
		camera.RotateWithMouse(xOffset, yOffset, true);

		mouse[0] = xPos;
		mouse[1] = yPos;
	}
}

void mouse_button_callback(GLFWwindow* window, int button, int action, int mods)
{
	if (button == GLFW_MOUSE_BUTTON_RIGHT && action == GLFW_PRESS)
	{
		glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
		firstDown = true;
		flyingCameraActive = true;
	}
	else if (button == GLFW_MOUSE_BUTTON_RIGHT && action == GLFW_RELEASE)
	{
		glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_NORMAL);
		firstDown = false;
		flyingCameraActive = false;
	}
}

void framebuffer_size_callback(GLFWwindow* window, int width, int height)
{
	glViewport(0, 0, width, height); GLCheckError();
}

GLuint loadTexture(char const* path)
{
	GLuint textureID;
	glGenTextures(1, &textureID); GLCheckError();

	int width, height, nrChannels;
	unsigned char* data = stbi_load(path, &width, &height, &nrChannels, 0);

	if (data) {
		GLenum format;
		if (nrChannels == 1)
			format = GL_RED;
		else if (nrChannels == 3)
			format = GL_RGB;
		else if (nrChannels == 4)
			format = GL_RGBA;

		glBindTexture(GL_TEXTURE_2D, textureID); GLCheckError();
		glTexImage2D(GL_TEXTURE_2D, 0, format, width, height, 0, format, GL_UNSIGNED_BYTE, data); GLCheckError();
		glGenerateMipmap(GL_TEXTURE_2D); GLCheckError();

		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT); GLCheckError();
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT); GLCheckError();
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR); GLCheckError();
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR); GLCheckError();

		stbi_image_free(data);
	}
	else
	{
		std::cout << "Failed to load texture at path: " << path << std::endl;
		stbi_image_free(data);
	}

	return textureID;
}

// loads a cubemap texture from 6 individual texture faces
// order:
// +X (right)
// -X (left)
// +Y (top)
// -Y (bottom)
// +Z (front) 
// -Z (back)
// -------------------------------------------------------
GLuint loadCubemap(const std::vector<std::string>& faces)
{
	stbi_set_flip_vertically_on_load(false);
	GLuint textureID;
	glGenTextures(1, &textureID);
	glBindTexture(GL_TEXTURE_CUBE_MAP, textureID);

	int width, height, nrChannels;
	for (unsigned int i = 0; i < faces.size(); ++i)
	{
		unsigned char* data = stbi_load(faces[i].c_str(), &width, &height, &nrChannels, 0);
		if(data)
		{
			glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, data);
			stbi_image_free(data);
		}
		else
		{
			std::cout << "Cubemap texture failed to load at path: " << faces[i] << std::endl;
			stbi_image_free(data);
		}
	}
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
	stbi_set_flip_vertically_on_load(true);

	return textureID;
}

GLuint load_cubemap_from_path(const std::string& path, const std::vector<std::string>& skybox_faces)
{
	std::vector<std::string> path_to_faces;
	path_to_faces.reserve(skybox_faces.size());
	for (const std::string& name : skybox_faces)
	{
		path_to_faces.push_back(path + name + ".jpg");
	}
	return loadCubemap(path_to_faces);
}