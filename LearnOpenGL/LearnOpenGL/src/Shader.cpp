#include "Shader.h"
#include "GLDebug.h"

#include <fstream>
#include <sstream>
#include <iostream>

Shader::Shader(const char* vertexShaderSourcePath, const char* fragmentShaderSourcePath)
{
	// read shader from file and write into c-string since OpenGL uses c-strings
	std::string vertexCode;
	std::string fragmentCode;
	std::ifstream vShaderFile;
	std::ifstream fShaderFile;

	// ensure ifstream objects can throw exceptions
	vShaderFile.exceptions(std::ifstream::failbit | std::ifstream::badbit);
	fShaderFile.exceptions(std::ifstream::failbit | std::ifstream::badbit);
	
	try
	{
		vShaderFile.open(vertexShaderSourcePath);
		fShaderFile.open(fragmentShaderSourcePath);
		std::stringstream vShaderStream, fShaderStream;

		vShaderStream << vShaderFile.rdbuf();
		fShaderStream << fShaderFile.rdbuf();

		vShaderFile.close();
		fShaderFile.close();

		vertexCode = vShaderStream.str();
		fragmentCode = fShaderStream.str();
	}
	catch (std::ifstream::failure e)
	{
		std::cout << "ERROR::SHADER::FILE_NOT_SUCCESFULLY_READ" << std::endl;
	}

	const char* vShaderCode = vertexCode.c_str();
	const char* fShaderCode = fragmentCode.c_str();

	// compile shader
	GLuint vertexShader, fragmentShader;

	vertexShader = glCreateShader(GL_VERTEX_SHADER); GLCheckError();
	glShaderSource(vertexShader, 1, &vShaderCode, NULL); GLCheckError();
	glCompileShader(vertexShader); GLCheckError();
	checkShaderCompileStatus(vertexShader, ShaderType::VERTEX);

	fragmentShader = glCreateShader(GL_FRAGMENT_SHADER); GLCheckError();
	glShaderSource(fragmentShader, 1, &fShaderCode, NULL); GLCheckError();
	glCompileShader(fragmentShader); GLCheckError();
	checkShaderCompileStatus(fragmentShader, ShaderType::FRAGMENT);

	m_rendererID = glCreateProgram();
	glAttachShader(m_rendererID, vertexShader);
	glAttachShader(m_rendererID, fragmentShader);
	glLinkProgram(m_rendererID);
	checkProgramLinkStatus();

	glDeleteShader(vertexShader);
	glDeleteShader(fragmentShader);
}

Shader::Shader(const char* vertexShaderSourcePath, const char* geometryShaderSourcePath, const char* fragmentShaderSourcePath)
{
	// read shader from file and write into c-string since OpenGL uses c-strings
	std::string vertexCode;
	std::string geometryCode;
	std::string fragmentCode;
	std::ifstream vShaderFile;
	std::ifstream gShaderFile;
	std::ifstream fShaderFile;

	// ensure ifstream objects can throw exceptions
	vShaderFile.exceptions(std::ifstream::failbit | std::ifstream::badbit);
	gShaderFile.exceptions(std::ifstream::failbit | std::ifstream::badbit);
	fShaderFile.exceptions(std::ifstream::failbit | std::ifstream::badbit);

	try
	{
		vShaderFile.open(vertexShaderSourcePath);
		gShaderFile.open(geometryShaderSourcePath);
		fShaderFile.open(fragmentShaderSourcePath);
		std::stringstream vShaderStream, gShaderStream, fShaderStream;

		vShaderStream << vShaderFile.rdbuf();
		gShaderStream << gShaderFile.rdbuf();
		fShaderStream << fShaderFile.rdbuf();

		vShaderFile.close();
		gShaderFile.close();
		fShaderFile.close();

		vertexCode = vShaderStream.str();
		geometryCode = gShaderStream.str();
		fragmentCode = fShaderStream.str();
	}
	catch (std::ifstream::failure e)
	{
		std::cout << "ERROR::SHADER::FILE_NOT_SUCCESFULLY_READ" << std::endl;
	}

	const char* vShaderCode = vertexCode.c_str();
	const char* gShaderCode = geometryCode.c_str();
	const char* fShaderCode = fragmentCode.c_str();

	// compile shader
	GLuint vertexShader, geometryShader, fragmentShader;

	vertexShader = glCreateShader(GL_VERTEX_SHADER); GLCheckError();
	glShaderSource(vertexShader, 1, &vShaderCode, nullptr); GLCheckError();
	glCompileShader(vertexShader); GLCheckError();
	checkShaderCompileStatus(vertexShader, ShaderType::VERTEX);
	
	geometryShader = glCreateShader(GL_GEOMETRY_SHADER); GLCheckError();
	glShaderSource(geometryShader, 1, &gShaderCode, nullptr); GLCheckError();
	glCompileShader(geometryShader); GLCheckError();
	checkShaderCompileStatus(geometryShader, ShaderType::GEOMETRY);
	
	fragmentShader = glCreateShader(GL_FRAGMENT_SHADER); GLCheckError();
	glShaderSource(fragmentShader, 1, &fShaderCode, nullptr); GLCheckError();
	glCompileShader(fragmentShader); GLCheckError();
	checkShaderCompileStatus(fragmentShader, ShaderType::FRAGMENT);

	m_rendererID = glCreateProgram();
	glAttachShader(m_rendererID, vertexShader);
	glAttachShader(m_rendererID, geometryShader);
	glAttachShader(m_rendererID, fragmentShader);
	glLinkProgram(m_rendererID);
	checkProgramLinkStatus();

	glDeleteShader(vertexShader);
	glDeleteShader(geometryShader);
	glDeleteShader(fragmentShader);
}

void Shader::bind() const
{
	glUseProgram(m_rendererID); GLCheckError();
}

void Shader::unbind() const
{
	glUseProgram(0); GLCheckError();
}

void Shader::setBool(const std::string& name, bool value) 
{
	glUniform1i(getUniformLocation(name), (GLint)value); GLCheckError();
}

void Shader::setInt(const std::string& name, int value) 
{
	glUniform1i(getUniformLocation(name), value); GLCheckError();
}

void Shader::setFloat(const std::string& name, float value) 
{
	glUniform1f(getUniformLocation(name), value); GLCheckError();
}

void Shader::setVec2(const std::string& name, const glm::vec2& value) 
{
	glUniform2fv(getUniformLocation(name), 1, &value[0]); GLCheckError();
}

void Shader::setVec2(const std::string& name, float x, float y) 
{
	glUniform2f(getUniformLocation(name), x, y); GLCheckError();
}

void Shader::setVec3(const std::string& name, const glm::vec3& value) 
{
	glUniform3fv(getUniformLocation(name), 1, &value[0]); GLCheckError();
}

void Shader::setVec3(const std::string& name, float x, float y, float z) 
{
	glUniform3f(getUniformLocation(name), x, y, z); GLCheckError();
}

void Shader::setVec4(const std::string& name, const glm::vec4& value) 
{
	glUniform4fv(getUniformLocation(name), 1, &value[0]); GLCheckError();
}

void Shader::setVec4(const std::string& name, float x, float y, float z, float w) 
{
	glUniform4f(getUniformLocation(name), x, y, z, w); GLCheckError();
}

void Shader::setMat2(const std::string& name, const glm::mat2& mat) 
{
	glUniformMatrix2fv(getUniformLocation(name), 1, GL_FALSE, &mat[0][0]); GLCheckError();
}

void Shader::setMat3(const std::string& name, const glm::mat3& mat) 
{
	glUniformMatrix3fv(getUniformLocation(name), 1, GL_FALSE, &mat[0][0]); GLCheckError();
}

void Shader::setMat4(const std::string& name, const glm::mat4& mat) 
{
	glUniformMatrix4fv(getUniformLocation(name), 1, GL_FALSE, &mat[0][0]); GLCheckError();
}

void Shader::setTexture2D(const std::string& name, GLuint id, GLuint unit)
{
	glActiveTexture(GL_TEXTURE0 + unit); GLCheckError();
	glBindTexture(GL_TEXTURE_2D, id);
	glUniform1i(getUniformLocation(name), unit); GLCheckError();
}

void Shader::setTextureCubemap(const std::string& name, GLuint id, GLuint unit)
{
	glActiveTexture(GL_TEXTURE0 + unit); GLCheckError();
	glBindTexture(GL_TEXTURE_CUBE_MAP, id);
	glUniform1i(getUniformLocation(name), unit); GLCheckError();
}

void Shader::unsetTexture2D(GLuint unit) const
{
	glActiveTexture(GL_TEXTURE0 + unit); GLCheckError();
	glBindTexture(GL_TEXTURE_2D, 0); GLCheckError();
}

void Shader::unsetTextureCubemap(GLuint unit) const
{
	glActiveTexture(GL_TEXTURE0 + unit); GLCheckError();
	glBindTexture(GL_TEXTURE_CUBE_MAP, 0); GLCheckError();
}

void Shader::setMat4(GLint location, const glm::mat4& mat) const
{
	glUniformMatrix4fv(location, 1, GL_FALSE, &mat[0][0]); GLCheckError();
}

GLint Shader::getUniformLocation(const std::string& name) 
{
	auto search = m_UniformLocationCache.find(name);
	if (m_UniformLocationCache.find(name) != m_UniformLocationCache.end())
		return search->second;

	GLint location = glGetUniformLocation(m_rendererID, name.c_str()); GLCheckError();
	if (location < 0)
	{
		// wenn -1 als location benutzt wird wird der call von OpenGL ignoriert und es kommt zu keinem Fehler
		std::cout << "Warning: uniform '" << name << "' does not exist!" << std::endl;
	}

	m_UniformLocationCache[name] = location;
	return location;
}

void Shader::checkProgramLinkStatus() const
{
	GLint success;
	glGetProgramiv(m_rendererID, GL_LINK_STATUS, &success);
	if (!success)
	{
		char infoLog[512];
		glGetProgramInfoLog(m_rendererID, 512, nullptr, infoLog);
		std::cout << "ERROR::PROGRAM::LINK_FAILED\n" << infoLog << std::endl;
	}
}

void Shader::checkShaderCompileStatus(GLuint shaderId, ShaderType type)
{
	GLint success;
	glGetShaderiv(shaderId, GL_COMPILE_STATUS, &success);
	if (!success)
	{
		char infoLog[512];
		glGetShaderInfoLog(shaderId, 512, nullptr, infoLog);

		std::string s_type;
		switch (type)
		{
		case ShaderType::VERTEX:
			s_type = "VERTEX";
			break;
		case ShaderType::GEOMETRY:
			s_type = "GEOMETRY";
			break;
		case ShaderType::FRAGMENT:
			s_type = "FRAGMENT";
			break;
		default:
			s_type = "UNKNOWN";
			break;
		}
		
		std::cout << "ERROR::SHADER::" << s_type << "::COMPILATION_FAILED\n" << infoLog << std::endl;
	}
}
