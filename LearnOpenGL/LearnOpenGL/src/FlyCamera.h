#pragma once
#include <glad/glad.h>
#include "vendor/glm/glm.hpp"
#include "vendor/glm/gtc/matrix_transform.hpp"

enum class Camera_Movement {
	FORWARD,
	BACKWARD,
	LEFT,
	RIGHT,
	UP,
	DOWN
};

// Default camera values
const float YAW			= -90.0f;
const float PITCH		= 0.0f;
const float SPEED		= 2.5f;
const float SENSITIVITY = 0.1f;
const float FOV			= 60.0f;

class FlyCamera
{
public:
	glm::vec3 position;
	glm::vec3 front;
	glm::vec3 up;
	glm::vec3 right;
	glm::vec3 worldUp;

	float yaw;
	float pitch;

	float movementSpeed;
	float mouseSensitivity;
	float fov;

	FlyCamera(const glm::vec3& position = glm::vec3(0.0f, 0.0f, 0.0f), const glm::vec3& up = glm::vec3(0.0f, 1.0f, 0.0f), float yaw = YAW, float pitch = PITCH);

	void Move(Camera_Movement direction, float deltaTime);
	void RotateWithMouse(float xOffset, float yOffset, bool constrainPitch = true);
	void Zoom(float value);
	void Reset();

	glm::mat4 GetViewMatrix() const;

private:
	float startYaw;
	float startPitch;
	glm::vec3 startPosition;
	glm::vec3 startFront;
	glm::vec3 startUp;

	void updateCameraVectors();
};

