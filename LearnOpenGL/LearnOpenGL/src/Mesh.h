#pragma once
#include <glad/glad.h>
#include <vector>

#include "Shader.h"
#include "vendor/glm/glm.hpp"

struct Vertex {
	glm::vec3 position;
	glm::vec3 normal;
	glm::vec2 tex_coord;
	glm::vec3 tangent;
	glm::vec3 bitangent;
};

struct Texture {
	enum class Type { DIFFUSE, SPECULAR, NORMAL, HEIGHT };

	unsigned int id;
	Type type;
	std::string path;
};

class Mesh
{
public:
	std::vector<Vertex> vertices;
	std::vector<unsigned int> indices;
	std::vector<Texture> textures;

	Mesh(const std::vector<Vertex>& vertices, const std::vector<unsigned int>& indices, const std::vector<Texture>& textures);
	void Draw(Shader &shader);

	GLuint GetVAO() const { return VAO; }

private:
	GLuint VAO, VBO, EBO;

	void setupMesh();
};

