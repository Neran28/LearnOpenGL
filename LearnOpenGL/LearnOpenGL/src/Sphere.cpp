#include "Sphere.h"
#include "GLDebug.h"

#include <cmath>

static const int MIN_SECTOR_COUNT = 3;
static const int MIN_STACK_COUNT = 2;
static const float PI = 3.14159265358979323846f;

Sphere::Sphere(float radius, int sectorCount, int stackCount)
{
	glGenBuffers(1, &vbo);
	glGenBuffers(1, &ebo);
	glGenVertexArrays(1, &vao);
	set(radius, sectorCount, stackCount);
	setupOGL();
}

Sphere::Sphere(const Sphere& other)
{
	glGenBuffers(1, &vbo);
	glGenBuffers(1, &ebo);
	glGenVertexArrays(1, &vao);
	radius = other.radius;
	sectorCount = other.sectorCount;
	stackCount = other.stackCount;
	vertices = other.vertices;
	indices = other.indices;
	setupOGL();
}

void Sphere::set(const float radius, const int sectorCount, const int stack_count)
{
	this->radius = radius;
	this->sectorCount = sectorCount;
	this->stackCount = stack_count;
	
	if (sectorCount < MIN_SECTOR_COUNT)
		this->sectorCount = MIN_SECTOR_COUNT;

	if (stack_count < MIN_STACK_COUNT)
		this->stackCount = MIN_STACK_COUNT;

	buildVertices();
	
}

Sphere::~Sphere()
{
	glDeleteBuffers(1, &vbo);
	glDeleteBuffers(1, &ebo);
	glDeleteVertexArrays(1, &vao);
}

void Sphere::setRadius(float new_radius)
{
	if(radius != new_radius)
		set(new_radius, sectorCount, stackCount);
}

void Sphere::setSectorCount(int new_sectorCount)
{
	if (sectorCount != new_sectorCount)
		set(radius, new_sectorCount, stackCount);
}

void Sphere::setStackCount(int new_stackCount)
{
	if (stackCount != new_stackCount)
		set(radius, sectorCount, new_stackCount);
}

void Sphere::draw(const Shader& shader) const
{
	shader.bind();
	
	glBindVertexArray(vao); GLCheckError();
	glDrawElements(GL_TRIANGLES, indices.size(), GL_UNSIGNED_INT, nullptr); GLCheckError();
	glBindVertexArray(0); GLCheckError();
}

void Sphere::buildVertices()
{
	clearArrays();

	// sector Winkel --> von 0 bis 360 grad
	const float sectorStep = 2 * PI / sectorCount;
	// stack Winkel von -90� (unten) bis 90� (oben)
	const float stackStep = PI / stackCount;
	const float inverseLength = 1.0f / radius;
	Vertex vertex{};
	
	for (int i = 0; i <= stackCount; ++i)
	{
		const float stackAngle = -PI / 2 + i * stackStep;
		const float xz = radius * cosf(stackAngle);
		const float y = radius * sinf(stackAngle);

		// j <= sectorCount, weil erster und letzter vertex sind gleich, die texcoord ist aber anders (0,0) vs (1,1)
		for (int j = 0; j <= sectorCount; ++j)
		{
			const float sectorAngle = j * sectorStep;

			// vertex position
			const float x = xz * sinf(sectorAngle);
			const float z = xz * cosf(sectorAngle);
			vertex.position = glm::vec3{ x, y, z };

			// normal
			const float nx = x * inverseLength;
			const float ny = y * inverseLength;
			const float nz = z * inverseLength;
			vertex.normal = glm::vec3{ nx, ny, nz };

			// texture coordinates
			const float u = static_cast<float>(j) / sectorCount;
			const float v = static_cast<float>(i) / stackCount;
			vertex.texCoord = glm::vec2{ u, v };

			vertices.emplace_back(vertex);
		}
	}

	// compute indices
	// v1 ist der erste index vom aktuellen stack
	// v2 ist der index des n�chsten stacks
	//  v1--v1+1
	//  |  / |
	//  | /  |
	//  v2--v2+1
	// triangulation eines sectors in einem stack mit CCW ordering
	// => (v1, v2, v1+1) und (v1+1, v2, v2+1)
	for (int i = 0; i < stackCount; ++i)
	{
		unsigned int v1 = i * (sectorCount + 1);
		unsigned int v2 = v1 + sectorCount + 1;

		for (int j = 0; j < sectorCount; ++j, ++v1, ++v2)
		{
			if(i != 0)
			{
				indices.emplace_back(v1);
				indices.emplace_back(v2);
				indices.emplace_back(v1 + 1);
			}

			if(i != (stackCount - 1))
			{
				indices.emplace_back(v1 + 1);
				indices.emplace_back(v2);
				indices.emplace_back(v2 + 1);
			}
		}
	}
}

void Sphere::setupOGL()
{
	glBindVertexArray(vao);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebo);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size() * sizeof(GLuint), indices.data(), GL_STATIC_DRAW);

	glBindBuffer(GL_ARRAY_BUFFER, vbo);
	glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(Vertex), vertices.data(), GL_STATIC_DRAW);

	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)0); GLCheckError();
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)offsetof(Vertex, normal)); GLCheckError();
	glEnableVertexAttribArray(2);
	glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)offsetof(Vertex, texCoord)); GLCheckError();

	glBindVertexArray(0); GLCheckError();
}


/// <summary>
/// deallocate all arrays
/// </summary>
void Sphere::clearArrays()
{
	std::vector<Vertex>().swap(vertices);
	std::vector<unsigned int>().swap(indices);
}



