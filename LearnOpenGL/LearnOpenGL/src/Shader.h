#pragma once

#include <glad/glad.h>
#include <string>
#include <unordered_map>

#include "vendor/glm/glm.hpp"


class Shader
{
	enum class ShaderType { VERTEX, GEOMETRY, FRAGMENT };
private:
	std::unordered_map<std::string, GLint> m_UniformLocationCache;

public:
	GLuint m_rendererID;

	Shader(const char* vertexShaderSourcePath, const char* fragmentShaderSourcePath);
	Shader(const char* vertexShaderSourcePath, const char* geometryShaderSourcePath, const char* fragmentShaderSourcePath);

	void bind() const;
	void unbind() const;

	void setBool(const std::string& name, bool value);
	void setInt(const std::string& name, int value);
	void setFloat(const std::string& name, float value);

	void setVec2(const std::string& name, const glm::vec2& value);
	void setVec2(const std::string& name, float x, float y);
	void setVec3(const std::string& name, const glm::vec3& value);
	void setVec3(const std::string& name, float x, float y, float z);
	void setVec4(const std::string& name, const glm::vec4& value);
	void setVec4(const std::string& name, float x, float y, float z, float w);

	void setMat2(const std::string& name, const glm::mat2& mat);
	void setMat3(const std::string& name, const glm::mat3& mat);
	void setMat4(const std::string& name, const glm::mat4& mat);

	void setTexture2D(const std::string& name, GLuint id, GLuint unit = 0);
	void setTextureCubemap(const std::string& name, GLuint id, GLuint unit = 0);

	void unsetTexture2D(GLuint unit) const;
	void unsetTextureCubemap(GLuint unit) const;
	
	void setMat4(GLint location, const glm::mat4& mat) const;


private:
	static void checkShaderCompileStatus(GLuint shaderId, ShaderType type);
	void checkProgramLinkStatus() const;

	GLint getUniformLocation(const std::string& name);
};

