#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include "vendor/stb_image/stb_image.h"

#include <iostream>
#include <sstream>
#include <filesystem>

#include "vendor/glm/glm.hpp"
#include "vendor/glm/gtc/matrix_transform.hpp"
#include "vendor/glm/gtc/type_ptr.hpp"

#include "Shader.h"
#include "FlyCamera.h"
#include "Model.h"
#include "GLDebug.h"


// settings
std::string title;
GLFWwindow* window = nullptr;
const unsigned int SCR_WIDTH = 800;
const unsigned int SCR_HEIGHT = 600;

// camera
FlyCamera camera(glm::vec3(0.0f, 0.0f, 3.0f));
float lastX = (float)SCR_WIDTH / 2.0;
float lastY = (float)SCR_HEIGHT / 2.0;
bool firstMouse = true;
bool flyingCameraActive = false;

// timing
double delta_time = 0.0;
double time_last_frame = 0.0;
double render_time;
long total_frames = 0;
float fps[30]{};
double time_fps = 0;

// matrices
glm::mat4 model;
glm::mat4 view;
glm::mat4 projection;

glm::vec3 cubeSceneTranslation = glm::vec3(20.0f, 10.0, 0.0f);
bool shadows = true;

// OpenGL objects
GLuint plane_vao;

GLuint woodTexture;
GLuint stoneTexture;
GLuint stoneNormalMap;
GLuint wallDiffuseTexture;
GLuint wallNormalMap;
GLuint wallHeightMap;
GLuint toyBoxDiffuseTexture;
GLuint toyBoxNormalMap;
GLuint toyBoxHeightMap;

bool useNormalMapping = true;
bool normalKeyPressed = false;

bool useParallaxMapping = true;
bool parallaxKeyPressed = false;
float heightScale = 1.0f;

void framebuffer_size_callback(GLFWwindow* window, int width, int height);
void mouse_callback(GLFWwindow* window, double xpos, double ypos);
void mouse_button_callback(GLFWwindow* window, int button, int action, int mods);
void scroll_callback(GLFWwindow* window, double xoffset, double yoffset);
void processInput(GLFWwindow* window);
unsigned int loadTexture(const char* path);

void renderScene(Shader& shader);
void renderCubeScene(Shader& shader);
void renderCube();
void renderQuad();

void updatePerformanceStats()
{
	const int frame = total_frames % (sizeof(fps) / sizeof(fps[0]));
	fps[frame] = (float)(1.0 / delta_time);

	if ((time_last_frame - time_fps) > 0.5) {
		time_fps = time_last_frame;
		float sum = 0;
		for (int i = 0; i < (sizeof(fps) / sizeof(fps[0])); ++i)
			sum += fps[i];

		std::stringstream s;
		float roundedFPS = (int)(sum / (sizeof(fps) / sizeof(fps[0])) * 100 + 0.5f);
		roundedFPS = roundedFPS / 100;
		s << title << " - FPS: " << roundedFPS << "    Render Time: " << render_time * 100 << "ms";
		glfwSetWindowTitle(window, s.str().c_str());
	}
}

int main()
{
	// glfw: initialize and configure
	// ------------------------------
	glfwInit();
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

#ifdef __APPLE__
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
#endif

	// glfw window creation
	// --------------------
	window = glfwCreateWindow(SCR_WIDTH, SCR_HEIGHT, "Parallax Mapping", nullptr, nullptr);
	if (window == nullptr)
	{
		std::cout << "Failed to create GLFW window" << std::endl;
		glfwTerminate();
		return -1;
	}

	glfwMakeContextCurrent(window);
	glfwSetMouseButtonCallback(window, mouse_button_callback);
	glfwSetFramebufferSizeCallback(window, framebuffer_size_callback);
	glfwSetCursorPosCallback(window, mouse_callback);
	glfwSetScrollCallback(window, scroll_callback);

	title = "Parallax Mapping";

	// glad: load all OpenGL function pointers
	// ---------------------------------------
	if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
	{
		std::cout << "Failed to initialize GLAD" << std::endl;
		return -1;
	}

	//glfwSwapInterval(0);
	//stbi_set_flip_vertically_on_load(1); // muss hier beim tutorial ausbleiben, da tangent und bitangent nicht auf y geflipped berechnet werden
	// configure global opengl state
	// -----------------------------
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_CULL_FACE);

	// build and compile shaders
	// -------------------------
	Shader depthmap_shader("res/shaders/3_1_simple_depth.vert", "res/shaders/3_1_simple_depth.frag");
	Shader shadow_shader("res/shaders/3_1_shadow_mapping.vert", "res/shaders/3_1_shadow_mapping.frag");

	Shader depthmap_cubemap_shader("res/shaders/3_1_depth_cubemap.vert", "res/shaders/3_1_depth_cubemap.geom", "res/shaders/3_1_depth_cubemap.frag");
	Shader shadow_cubemap_shader("res/shaders/3_1_shadow_mapping_cube.vert", "res/shaders/3_1_shadow_mapping_cube.frag");

	Shader debugDepthQuad_shader("res/shaders/3_1_debug_depth_quad.vert", "res/shaders/3_1_debug_depth_quad.frag");

	Shader white_shader("res/shaders/white.vert", "res/shaders/white.frag");

	unsigned int ubo_matrices;
	glGenBuffers(1, &ubo_matrices);
	glBindBuffer(GL_UNIFORM_BUFFER, ubo_matrices);
	glBufferData(GL_UNIFORM_BUFFER, 2 * sizeof(glm::mat4), nullptr, GL_STATIC_DRAW);
	glBindBuffer(GL_UNIFORM_BUFFER, 0);
	glBindBufferBase(GL_UNIFORM_BUFFER, 1, ubo_matrices);
	unsigned int matrices_index = glGetUniformBlockIndex(white_shader.m_rendererID, "Matrices");
	glUniformBlockBinding(white_shader.m_rendererID, matrices_index, 1);

	// geometry data
	float planeVertices[] = {
		// positions            // normals         // texcoords
		 25.0f, -0.5f,  25.0f,  0.0f, 1.0f, 0.0f,  25.0f,  0.0f,
		-25.0f, -0.5f, -25.0f,  0.0f, 1.0f, 0.0f,   0.0f, 25.0f,
		-25.0f, -0.5f,  25.0f,  0.0f, 1.0f, 0.0f,   0.0f,  0.0f,

		-25.0f, -0.5f, -25.0f,  0.0f, 1.0f, 0.0f,   0.0f, 25.0f,
		 25.0f, -0.5f,  25.0f,  0.0f, 1.0f, 0.0f,  25.0f,  0.0f,
		 25.0f, -0.5f, -25.0f,  0.0f, 1.0f, 0.0f,  25.0f, 25.0f
	};

	// plane VAO
	unsigned int planeVBO;
	glGenVertexArrays(1, &plane_vao);
	glGenBuffers(1, &planeVBO);
	glBindVertexArray(plane_vao);
	glBindBuffer(GL_ARRAY_BUFFER, planeVBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(planeVertices), planeVertices, GL_STATIC_DRAW);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)0);
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)(3 * sizeof(float)));
	glEnableVertexAttribArray(2);
	glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)(6 * sizeof(float)));
	glBindVertexArray(0);

	// load textures
	// -------------
	woodTexture = loadTexture("res/textures/wood.png");
	stoneTexture = loadTexture("res/textures/brickwall.jpg");
	stoneNormalMap = loadTexture("res/textures/brickwall_normal.jpg");
	wallDiffuseTexture = loadTexture("res/textures/bricks2.jpg");
	wallNormalMap = loadTexture("res/textures/bricks2_normal.jpg");
	wallHeightMap = loadTexture("res/textures/bricks2_disp.jpg");
	toyBoxDiffuseTexture = loadTexture("res/textures/toy_box_diffuse.png");
	toyBoxNormalMap = loadTexture("res/textures/toy_box_normal.png");
	toyBoxHeightMap = loadTexture("res/textures/toy_box_disp.png");


	// set up vertex data (and buffer(s)) and configure vertex attributes
	// ------------------------------------------------------------------
	GLuint depthMapFBO;
	glGenFramebuffers(1, &depthMapFBO);

	const unsigned int SHADOW_WIDTH = 1024, SHADOW_HEIGHT = 1024;
	GLuint depthMap;
	glGenTextures(1, &depthMap);
	glBindTexture(GL_TEXTURE_2D, depthMap);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT16, SHADOW_WIDTH, SHADOW_HEIGHT, 0, GL_DEPTH_COMPONENT, GL_FLOAT, nullptr);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	//glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_MODE, GL_COMPARE_REF_TO_TEXTURE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);
	float border_color[] = { 1.0f, 1.0f, 1.0f, 1.0f };
	// eine border color zu nutzen fixt aber nicht falls etwas au�erhalb der far plane liegt.
	glTexParameterfv(GL_TEXTURE_2D, GL_TEXTURE_BORDER_COLOR, border_color);


	glBindFramebuffer(GL_FRAMEBUFFER, depthMapFBO);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, depthMap, 0);
	// f�r ein framebuffer wird ein color buffer ben�tigt, hiermit sagt man rendere in keinen color buffer und nehme auch keinen f�r source pixel
	glDrawBuffer(GL_NONE);
	glReadBuffer(GL_NONE);
	glBindFramebuffer(GL_FRAMEBUFFER, 0);

	GLuint depth_cubemap;
	glGenTextures(1, &depth_cubemap);
	glBindTexture(GL_TEXTURE_CUBE_MAP, depth_cubemap);
	for (unsigned int i = 0; i < 6; ++i)
	{
		glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, 0, GL_DEPTH_COMPONENT, SHADOW_WIDTH, SHADOW_HEIGHT, 0, GL_DEPTH_COMPONENT, GL_FLOAT, nullptr);
	}
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);


	// shader configuration
	// --------------------
	debugDepthQuad_shader.bind();
	debugDepthQuad_shader.setInt("depthMap", 1);

	// lighting information
	glm::vec3 lightPosition(-2.0f, 4.0f, -1.0f);
	glm::vec3 light_position_cube(cubeSceneTranslation);

	// render loop
	// -----------
	while (!glfwWindowShouldClose(window))
	{
		// per-frame time logic
		// --------------------
		++total_frames;
		const float currentFrame = glfwGetTime();
		delta_time = currentFrame - time_last_frame;
		time_last_frame = currentFrame;
		updatePerformanceStats();

		// input
		// -----
		processInput(window);

		// render
		// ------
		glClearColor(0.1f, 0.1f, 0.1f, 1.0f);

		const double time_start_render = glfwGetTime();
		// 1. rendere zuerst in depth buffer
		// configure shader and matrices
		glm::vec3 transformedLightpos = lightPosition;
		transformedLightpos.x = lightPosition.x + sin(glfwGetTime());
		depthmap_shader.bind();
		float near_plane = 0.1f, far_plane = 7.5f;
		glm::mat4 lightProjection = glm::ortho(-10.0f, 10.0f, -10.0f, 10.0f, near_plane, far_plane);
		glm::mat4 lightView = glm::lookAt(transformedLightpos, glm::vec3(0.0f), glm::vec3(0.0f, 1.0f, 0.0f));
		glm::mat4 light_space_matrix = lightProjection * lightView;
		depthmap_shader.setMat4("lightSpaceMatrix", light_space_matrix);

		glViewport(0, 0, SHADOW_WIDTH, SHADOW_HEIGHT);
		// render directional shadow map
		glBindFramebuffer(GL_FRAMEBUFFER, depthMapFBO);
		glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, depthMap, 0);
		glClear(GL_DEPTH_BUFFER_BIT);
		renderScene(depthmap_shader);

		// render omnidirectional shadow map
		glFramebufferTexture(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, depth_cubemap, 0);
		glClear(GL_DEPTH_BUFFER_BIT);
		// set shader and matrices
		float aspect = static_cast<float>(SHADOW_WIDTH) / static_cast<float>(SHADOW_HEIGHT);
		far_plane = 25.0f;
		light_position_cube.z = sin(glfwGetTime() * 0.5) * 3.0;

		glm::mat4 light_proj_cube = glm::perspective(glm::radians(90.0f), aspect, near_plane, far_plane);
		std::vector<glm::mat4> light_cube_transforms;
		light_cube_transforms.push_back(light_proj_cube * glm::lookAt(light_position_cube, light_position_cube + glm::vec3(1.0f, 0.0f, 0.0f), glm::vec3(0.0f, -1.0f, 0.0f)));
		light_cube_transforms.push_back(light_proj_cube * glm::lookAt(light_position_cube, light_position_cube + glm::vec3(-1.0f, 0.0f, 0.0f), glm::vec3(0.0f, -1.0f, 0.0f)));
		light_cube_transforms.push_back(light_proj_cube * glm::lookAt(light_position_cube, light_position_cube + glm::vec3(0.0f, 1.0f, 0.0f), glm::vec3(0.0f, 0.0f, 1.0f)));
		light_cube_transforms.push_back(light_proj_cube * glm::lookAt(light_position_cube, light_position_cube + glm::vec3(0.0f, -1.0f, 0.0f), glm::vec3(0.0f, 0.0f, -1.0f)));
		light_cube_transforms.push_back(light_proj_cube * glm::lookAt(light_position_cube, light_position_cube + glm::vec3(0.0f, 0.0f, 1.0f), glm::vec3(0.0f, -1.0f, 0.0f)));
		light_cube_transforms.push_back(light_proj_cube * glm::lookAt(light_position_cube, light_position_cube + glm::vec3(0.0f, 0.0f, -1.0f), glm::vec3(0.0f, -1.0f, 0.0f)));

		depthmap_cubemap_shader.bind();
		for (unsigned int i = 0; i < 6; ++i)
		{
			depthmap_cubemap_shader.setMat4("shadowMatrices[" + std::to_string(i) + "]", light_cube_transforms[i]);
		}
		depthmap_cubemap_shader.setFloat("far_plane", far_plane);
		depthmap_cubemap_shader.setVec3("lightPos", light_position_cube);
		renderCubeScene(depthmap_cubemap_shader);
		// render scene

		glBindFramebuffer(GL_FRAMEBUFFER, 0);

		// 2. Render scene as normal with shadow mapping using the depth map
		glViewport(0, 0, SCR_WIDTH, SCR_HEIGHT);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		// configure shader and matrices
		shadow_shader.bind();
		view = camera.GetViewMatrix();
		projection = glm::perspective(glm::radians(camera.fov), static_cast<float>(SCR_WIDTH) / static_cast<float>(SCR_HEIGHT), 0.1f, 100.0f);

		shadow_shader.setMat4("view", view);
		shadow_shader.setMat4("projection", projection);

		shadow_shader.setVec3("viewPos", camera.position);
		shadow_shader.setVec3("lightPos", transformedLightpos);
		shadow_shader.setMat4("lightSpaceMatrix", light_space_matrix);

		shadow_shader.setTexture2D("shadowMap", depthMap, 1);
		renderScene(shadow_shader);
		shadow_shader.unsetTexture2D(1);

		// render cube scene
		shadow_cubemap_shader.bind();
		shadow_cubemap_shader.setMat4("projection", projection);
		shadow_cubemap_shader.setMat4("view", view);
		shadow_cubemap_shader.setVec3("lightPos", light_position_cube);
		shadow_cubemap_shader.setVec3("viewPos", camera.position);
		shadow_cubemap_shader.setInt("shadows", shadows);
		shadow_cubemap_shader.setFloat("far_plane", far_plane);
		shadow_cubemap_shader.setTextureCubemap("shadowMap", depth_cubemap, 1);
		renderCubeScene(shadow_cubemap_shader);
		shadow_cubemap_shader.unsetTextureCubemap(1);

		// render Depth map to quad for visual debugging
		// ---------------------------------------------
		//debugDepthQuad_shader.bind();
		//debugDepthQuad_shader.setFloat("near_plane", near_plane);
		//debugDepthQuad_shader.setFloat("far_plane", far_plane);
		//glActiveTexture(GL_TEXTURE0);
		//glBindTexture(GL_TEXTURE_2D, depthMap);
		//renderQuad();

		// render light sources
		// -------------------------------------------
		white_shader.bind();
		model = glm::mat4(1.0f);
		model = glm::translate(model, transformedLightpos);
		model = glm::scale(model, glm::vec3(0.1f, 0.1f, 0.1f));
		white_shader.setMat4("model", model);
		glBindBuffer(GL_UNIFORM_BUFFER, ubo_matrices);
		glBufferSubData(GL_UNIFORM_BUFFER, 0, sizeof(glm::mat4), glm::value_ptr(view));
		glBufferSubData(GL_UNIFORM_BUFFER, sizeof(glm::mat4), sizeof(glm::mat4), glm::value_ptr(projection));
		glBindBuffer(GL_UNIFORM_BUFFER, 0);
		renderCube();

		// glfw: swap buffers and poll IO events (keys pressed/released, mouse moved etc.)
		// -------------------------------------------------------------------------------
		glfwSwapBuffers(window);
		render_time = glfwGetTime() - time_start_render;

		glfwPollEvents();

	}

	// optional: de-allocate all resources once they've outlived their purpose:
	// ------------------------------------------------------------------------

	glfwTerminate();
	return 0;
}

void renderCubeScene(Shader& shader)
{
	shader.setTexture2D("diffuseTexture", woodTexture, 0);

	// room cube
	glm::mat4 model = glm::mat4(1.0f);
	model = glm::translate(model, cubeSceneTranslation);
	model = glm::scale(model, glm::vec3(5.0f));
	shader.setMat4("model", model);
	glDisable(GL_CULL_FACE); // note that we disable culling here since we render 'inside' the cube instead of the usual 'outside' which throws off the normal culling methods.
	shader.setInt("reverse_normals", 1); // A small little hack to invert normals when drawing cube from the inside so lighting still works.
	renderCube();
	shader.setInt("reverse_normals", 0); // and of course disable it
	glEnable(GL_CULL_FACE);
	// cubes
	model = glm::mat4(1.0f);
	model = glm::translate(model, glm::vec3(4.0f, -3.5f, 0.0) + cubeSceneTranslation);
	model = glm::scale(model, glm::vec3(0.5f));
	shader.setMat4("model", model);
	renderCube();
	model = glm::mat4(1.0f);
	model = glm::translate(model, glm::vec3(2.0f, 3.0f, 1.0) + cubeSceneTranslation);
	model = glm::scale(model, glm::vec3(0.75f));
	shader.setMat4("model", model);
	renderCube();
	model = glm::mat4(1.0f);
	model = glm::translate(model, glm::vec3(-3.0f, -1.0f, 0.0) + cubeSceneTranslation);
	model = glm::scale(model, glm::vec3(0.5f));
	shader.setMat4("model", model);
	renderCube();
	model = glm::mat4(1.0f);
	model = glm::translate(model, glm::vec3(-1.5f, 1.0f, 1.5) + cubeSceneTranslation);
	model = glm::scale(model, glm::vec3(0.5f));
	shader.setMat4("model", model);
	renderCube();
	model = glm::mat4(1.0f);
	model = glm::translate(model, glm::vec3(-1.5f, 2.0f, -3.0) + cubeSceneTranslation);
	model = glm::rotate(model, glm::radians(60.0f), glm::normalize(glm::vec3(1.0, 0.0, 1.0)));
	model = glm::scale(model, glm::vec3(0.75f));
	shader.setMat4("model", model);
	renderCube();

	shader.unsetTexture2D(0);
}

void renderScene(Shader& shader)
{
	shader.setInt("useNormalMap", 0);
	shader.setInt("useParallaxMapping", 0);
	shader.setTexture2D("diffuseTexture", woodTexture, 0);
	// floor
	model = glm::mat4(1.0f);
	shader.setMat4("model", model);
	glBindVertexArray(plane_vao);
	glDrawArrays(GL_TRIANGLES, 0, 6);
	// cubes
	model = glm::mat4(1.0f);
	model = glm::translate(model, glm::vec3(0.0f, 1.5f, 0.0f));
	model = glm::scale(model, glm::vec3(0.5f));
	shader.setMat4("model", model);
	renderCube();
	model = glm::mat4(1.0f);
	model = glm::translate(model, glm::vec3(2.0f, 0.0f, 1.0));
	model = glm::scale(model, glm::vec3(0.5f));
	shader.setMat4("model", model);
	renderCube();
	model = glm::mat4(1.0f);
	model = glm::translate(model, glm::vec3(-1.0f, 0.0f, 2.0));
	model = glm::rotate(model, glm::radians(60.0f), glm::normalize(glm::vec3(1.0, 0.0, 1.0)));
	model = glm::scale(model, glm::vec3(0.25));
	shader.setMat4("model", model);
	renderCube();

	// quad
	shader.setTexture2D("diffuseTexture", stoneTexture, 0);
	shader.setTexture2D("normalMap", stoneNormalMap, 2);
	shader.setInt("useNormalMap", useNormalMapping);
	model = glm::mat4(1.0f);
	model = glm::translate(model, glm::vec3(-1.5f, 3.0f, -1.5f));
	model = glm::rotate(model, glm::radians(-90.0f), glm::vec3(1.0f, 0.0f, 0.0f));
	shader.setMat4("model", model);
	renderQuad();

	// parallax mapped wall quad
	model = glm::mat4(1.0f);
	model = glm::translate(model, glm::vec3(-1.5f, 3.0f, -3.5f));
	shader.setMat4("model", model);
	shader.setTexture2D("diffuseTexture", wallDiffuseTexture, 0);
	shader.setTexture2D("normalMap", wallNormalMap, 2);
	shader.setTexture2D("depthMap", wallHeightMap, 3);
	shader.setFloat("heightScale", heightScale);
	shader.setInt("useParallaxMapping", useParallaxMapping);
	renderQuad();

	// parallax mapped toy box quad
	model = glm::mat4(1.0f);
	model = glm::translate(model, glm::vec3(-4.5f, 3.0f, -0.5f));
	model = glm::rotate(model, glm::radians(90.0f), glm::vec3(0, 1, 0));
	shader.setMat4("model", model);
	shader.setTexture2D("diffuseTexture", toyBoxDiffuseTexture, 0);
	shader.setTexture2D("normalMap", toyBoxNormalMap, 2);
	shader.setTexture2D("depthMap", toyBoxHeightMap, 3);
	renderQuad();

	shader.unsetTexture2D(0);
	shader.unsetTexture2D(2);
	shader.unsetTexture2D(3);
}

// renderCube() renders a 1x1 3D cube in NDC.
// -------------------------------------------------
unsigned int cubeVAO = 0;
unsigned int cubeVBO = 0;
void renderCube()
{
	// initialize (if necessary)
	if (cubeVAO == 0)
	{
		float vertices[] = {
			// back face
			-1.0f, -1.0f, -1.0f,  0.0f,  0.0f, -1.0f, 0.0f, 0.0f, // bottom-left
			 1.0f,  1.0f, -1.0f,  0.0f,  0.0f, -1.0f, 1.0f, 1.0f, // top-right
			 1.0f, -1.0f, -1.0f,  0.0f,  0.0f, -1.0f, 1.0f, 0.0f, // bottom-right         
			 1.0f,  1.0f, -1.0f,  0.0f,  0.0f, -1.0f, 1.0f, 1.0f, // top-right
			-1.0f, -1.0f, -1.0f,  0.0f,  0.0f, -1.0f, 0.0f, 0.0f, // bottom-left
			-1.0f,  1.0f, -1.0f,  0.0f,  0.0f, -1.0f, 0.0f, 1.0f, // top-left
			// front face
			-1.0f, -1.0f,  1.0f,  0.0f,  0.0f,  1.0f, 0.0f, 0.0f, // bottom-left
			 1.0f, -1.0f,  1.0f,  0.0f,  0.0f,  1.0f, 1.0f, 0.0f, // bottom-right
			 1.0f,  1.0f,  1.0f,  0.0f,  0.0f,  1.0f, 1.0f, 1.0f, // top-right
			 1.0f,  1.0f,  1.0f,  0.0f,  0.0f,  1.0f, 1.0f, 1.0f, // top-right
			-1.0f,  1.0f,  1.0f,  0.0f,  0.0f,  1.0f, 0.0f, 1.0f, // top-left
			-1.0f, -1.0f,  1.0f,  0.0f,  0.0f,  1.0f, 0.0f, 0.0f, // bottom-left
			// left face
			-1.0f,  1.0f,  1.0f, -1.0f,  0.0f,  0.0f, 1.0f, 0.0f, // top-right
			-1.0f,  1.0f, -1.0f, -1.0f,  0.0f,  0.0f, 1.0f, 1.0f, // top-left
			-1.0f, -1.0f, -1.0f, -1.0f,  0.0f,  0.0f, 0.0f, 1.0f, // bottom-left
			-1.0f, -1.0f, -1.0f, -1.0f,  0.0f,  0.0f, 0.0f, 1.0f, // bottom-left
			-1.0f, -1.0f,  1.0f, -1.0f,  0.0f,  0.0f, 0.0f, 0.0f, // bottom-right
			-1.0f,  1.0f,  1.0f, -1.0f,  0.0f,  0.0f, 1.0f, 0.0f, // top-right
			// right face
			 1.0f,  1.0f,  1.0f,  1.0f,  0.0f,  0.0f, 1.0f, 0.0f, // top-left
			 1.0f, -1.0f, -1.0f,  1.0f,  0.0f,  0.0f, 0.0f, 1.0f, // bottom-right
			 1.0f,  1.0f, -1.0f,  1.0f,  0.0f,  0.0f, 1.0f, 1.0f, // top-right         
			 1.0f, -1.0f, -1.0f,  1.0f,  0.0f,  0.0f, 0.0f, 1.0f, // bottom-right
			 1.0f,  1.0f,  1.0f,  1.0f,  0.0f,  0.0f, 1.0f, 0.0f, // top-left
			 1.0f, -1.0f,  1.0f,  1.0f,  0.0f,  0.0f, 0.0f, 0.0f, // bottom-left     
			// bottom face
			-1.0f, -1.0f, -1.0f,  0.0f, -1.0f,  0.0f, 0.0f, 1.0f, // top-right
			 1.0f, -1.0f, -1.0f,  0.0f, -1.0f,  0.0f, 1.0f, 1.0f, // top-left
			 1.0f, -1.0f,  1.0f,  0.0f, -1.0f,  0.0f, 1.0f, 0.0f, // bottom-left
			 1.0f, -1.0f,  1.0f,  0.0f, -1.0f,  0.0f, 1.0f, 0.0f, // bottom-left
			-1.0f, -1.0f,  1.0f,  0.0f, -1.0f,  0.0f, 0.0f, 0.0f, // bottom-right
			-1.0f, -1.0f, -1.0f,  0.0f, -1.0f,  0.0f, 0.0f, 1.0f, // top-right
			// top face
			-1.0f,  1.0f, -1.0f,  0.0f,  1.0f,  0.0f, 0.0f, 1.0f, // top-left
			 1.0f,  1.0f , 1.0f,  0.0f,  1.0f,  0.0f, 1.0f, 0.0f, // bottom-right
			 1.0f,  1.0f, -1.0f,  0.0f,  1.0f,  0.0f, 1.0f, 1.0f, // top-right     
			 1.0f,  1.0f,  1.0f,  0.0f,  1.0f,  0.0f, 1.0f, 0.0f, // bottom-right
			-1.0f,  1.0f, -1.0f,  0.0f,  1.0f,  0.0f, 0.0f, 1.0f, // top-left
			-1.0f,  1.0f,  1.0f,  0.0f,  1.0f,  0.0f, 0.0f, 0.0f  // bottom-left        
		};
		glGenVertexArrays(1, &cubeVAO); GLCheckError();
		glGenBuffers(1, &cubeVBO);
		// fill buffer
		glBindBuffer(GL_ARRAY_BUFFER, cubeVBO);
		glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);
		// link vertex attributes
		glBindVertexArray(cubeVAO);
		glEnableVertexAttribArray(0); GLCheckError();
		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)0);
		glEnableVertexAttribArray(1);
		glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)(3 * sizeof(float)));
		glEnableVertexAttribArray(2);
		glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)(6 * sizeof(float)));
		glBindBuffer(GL_ARRAY_BUFFER, 0);
		glBindVertexArray(0); GLCheckError();
	}
	// render Cube
	glBindVertexArray(cubeVAO); GLCheckError();
	glDrawArrays(GL_TRIANGLES, 0, 36); GLCheckError();
	glBindVertexArray(0); GLCheckError();
}

// renderQuad() renders a 1x1 XY quad in NDC
// -----------------------------------------
unsigned int quadVAO = 0;
unsigned int quadVBO;
void renderQuad()
{
	if (quadVAO == 0)
	{
		// positions
		glm::vec3 pos1(-1.0f, 1.0f, 0.0f);
		glm::vec3 pos2(-1.0f, -1.0f, 0.0f);
		glm::vec3 pos3(1.0f, -1.0f, 0.0f);
		glm::vec3 pos4(1.0f, 1.0f, 0.0f);
		// texture coordinates
		glm::vec2 uv1(0.0f, 1.0f);
		glm::vec2 uv2(0.0f, 0.0f);
		glm::vec2 uv3(1.0f, 0.0f);
		glm::vec2 uv4(1.0f, 1.0f);
		// normal vector
		glm::vec3 nm(0.0f, 0.0f, 1.0f);

		// calculate tangent/bitangent vectors of both triangles
		glm::vec3 tangent1, bitangent1;
		glm::vec3 tangent2, bitangent2;
		// triangle 1
		// ----------
		glm::vec3 edge1 = pos2 - pos1;
		glm::vec3 edge2 = pos3 - pos1;
		glm::vec2 deltaUV1 = uv2 - uv1;
		glm::vec2 deltaUV2 = uv3 - uv1;

		float f = 1.0f / (deltaUV1.x * deltaUV2.y - deltaUV2.x * deltaUV1.y);

		tangent1.x = f * (deltaUV2.y * edge1.x - deltaUV1.y * edge2.x);
		tangent1.y = f * (deltaUV2.y * edge1.y - deltaUV1.y * edge2.y);
		tangent1.z = f * (deltaUV2.y * edge1.z - deltaUV1.y * edge2.z);

		bitangent1.x = f * (-deltaUV2.x * edge1.x + deltaUV1.x * edge2.x);
		bitangent1.y = f * (-deltaUV2.x * edge1.y + deltaUV1.x * edge2.y);
		bitangent1.z = f * (-deltaUV2.x * edge1.z + deltaUV1.x * edge2.z);

		if (glm::dot(glm::cross(nm, tangent1), bitangent1) < 0.0f) {
			tangent1 = tangent1 * -1.0f;
		}

		// triangle 2
		// ----------
		edge1 = pos3 - pos1;
		edge2 = pos4 - pos1;
		deltaUV1 = uv3 - uv1;
		deltaUV2 = uv4 - uv1;

		f = 1.0f / (deltaUV1.x * deltaUV2.y - deltaUV2.x * deltaUV1.y);

		tangent2.x = f * (deltaUV2.y * edge1.x - deltaUV1.y * edge2.x);
		tangent2.y = f * (deltaUV2.y * edge1.y - deltaUV1.y * edge2.y);
		tangent2.z = f * (deltaUV2.y * edge1.z - deltaUV1.y * edge2.z);


		bitangent2.x = f * (-deltaUV2.x * edge1.x + deltaUV1.x * edge2.x);
		bitangent2.y = f * (-deltaUV2.x * edge1.y + deltaUV1.x * edge2.y);
		bitangent2.z = f * (-deltaUV2.x * edge1.z + deltaUV1.x * edge2.z);

		if (glm::dot(glm::cross(nm, tangent2), bitangent2) < 0.0f) {
			tangent2 = tangent2 * -1.0f;
		}

		float quadVertices[] = {
			// positions            // normal         // texcoords  // tangent                          // bitangent
			pos1.x, pos1.y, pos1.z, nm.x, nm.y, nm.z, uv1.x, uv1.y, tangent1.x, tangent1.y, tangent1.z, bitangent1.x, bitangent1.y, bitangent1.z,
			pos2.x, pos2.y, pos2.z, nm.x, nm.y, nm.z, uv2.x, uv2.y, tangent1.x, tangent1.y, tangent1.z, bitangent1.x, bitangent1.y, bitangent1.z,
			pos3.x, pos3.y, pos3.z, nm.x, nm.y, nm.z, uv3.x, uv3.y, tangent1.x, tangent1.y, tangent1.z, bitangent1.x, bitangent1.y, bitangent1.z,

			pos1.x, pos1.y, pos1.z, nm.x, nm.y, nm.z, uv1.x, uv1.y, tangent2.x, tangent2.y, tangent2.z, bitangent2.x, bitangent2.y, bitangent2.z,
			pos3.x, pos3.y, pos3.z, nm.x, nm.y, nm.z, uv3.x, uv3.y, tangent2.x, tangent2.y, tangent2.z, bitangent2.x, bitangent2.y, bitangent2.z,
			pos4.x, pos4.y, pos4.z, nm.x, nm.y, nm.z, uv4.x, uv4.y, tangent2.x, tangent2.y, tangent2.z, bitangent2.x, bitangent2.y, bitangent2.z
		};
		// configure plane VAO
		glGenVertexArrays(1, &quadVAO);
		glGenBuffers(1, &quadVBO);
		glBindVertexArray(quadVAO);
		glBindBuffer(GL_ARRAY_BUFFER, quadVBO);
		glBufferData(GL_ARRAY_BUFFER, sizeof(quadVertices), &quadVertices, GL_STATIC_DRAW);
		glEnableVertexAttribArray(0);
		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 14 * sizeof(float), (void*)0);
		glEnableVertexAttribArray(1);
		glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 14 * sizeof(float), (void*)(3 * sizeof(float)));
		glEnableVertexAttribArray(2);
		glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 14 * sizeof(float), (void*)(6 * sizeof(float)));
		glEnableVertexAttribArray(3);
		glVertexAttribPointer(3, 3, GL_FLOAT, GL_FALSE, 14 * sizeof(float), (void*)(8 * sizeof(float)));
		glEnableVertexAttribArray(4);
		glVertexAttribPointer(4, 3, GL_FLOAT, GL_FALSE, 14 * sizeof(float), (void*)(11 * sizeof(float)));
	}
	glBindVertexArray(quadVAO);
	glDrawArrays(GL_TRIANGLES, 0, 6);
	glBindVertexArray(0);
}


// process all input: query GLFW whether relevant keys are pressed/released this frame and react accordingly
// ---------------------------------------------------------------------------------------------------------
void processInput(GLFWwindow* window)
{
	if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
		glfwSetWindowShouldClose(window, true);

	if (glfwGetKey(window, GLFW_KEY_L) == GLFW_PRESS)
		glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

	if (glfwGetKey(window, GLFW_KEY_P) == GLFW_PRESS)
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

	if (flyingCameraActive)
	{
		if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS)
			camera.Move(Camera_Movement::FORWARD, delta_time);
		if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS)
			camera.Move(Camera_Movement::BACKWARD, delta_time);
		if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS)
			camera.Move(Camera_Movement::LEFT, delta_time);
		if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS)
			camera.Move(Camera_Movement::RIGHT, delta_time);
		if (glfwGetKey(window, GLFW_KEY_E) == GLFW_PRESS)
			camera.Move(Camera_Movement::UP, delta_time);
		if (glfwGetKey(window, GLFW_KEY_Q) == GLFW_PRESS)
			camera.Move(Camera_Movement::DOWN, delta_time);
	}

	if (glfwGetKey(window, GLFW_KEY_R) == GLFW_PRESS)
	{
		camera.Reset();
	}

	if (glfwGetKey(window, GLFW_KEY_N) == GLFW_PRESS && !normalKeyPressed)
	{
		useNormalMapping = !useNormalMapping;
		normalKeyPressed = true;
		std::cout << "Normal Mapping: " << (useNormalMapping ? "On" : "Off") << std::endl;
	}
	if (glfwGetKey(window, GLFW_KEY_N) == GLFW_RELEASE)
	{
		normalKeyPressed = false;
	}

	if (glfwGetKey(window, GLFW_KEY_P) == GLFW_PRESS && !parallaxKeyPressed)
	{
		useParallaxMapping = !useParallaxMapping;
		parallaxKeyPressed = true;
		std::cout << "Parallax Mapping: " << (useParallaxMapping ? "On" : "Off") << std::endl;
	}
	if (glfwGetKey(window, GLFW_KEY_P) == GLFW_RELEASE)
	{
		parallaxKeyPressed = false;
	}

	if (glfwGetKey(window, GLFW_KEY_COMMA) == GLFW_PRESS)
	{
		heightScale -= 0.5 * delta_time;
		if (heightScale < 0.0f)
			heightScale = 0.0f;

		std::cout << "Height Scale: " << heightScale << std::endl;
	}
	else if (glfwGetKey(window, GLFW_KEY_PERIOD) == GLFW_PRESS)
	{
		heightScale += 0.5 * delta_time;
		if (heightScale > 1.0f)
			heightScale = 1.0f;

		std::cout << "Height Scale: " << heightScale << std::endl;
	}
}

void mouse_button_callback(GLFWwindow* window, int button, int action, int mods)
{
	if (button == GLFW_MOUSE_BUTTON_RIGHT && action == GLFW_PRESS)
	{
		glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
		firstMouse = true;
		flyingCameraActive = true;
	}
	else if (button == GLFW_MOUSE_BUTTON_RIGHT && action == GLFW_RELEASE)
	{
		glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_NORMAL);
		firstMouse = false;
		flyingCameraActive = false;
	}
}

// glfw: whenever the window size changed (by OS or user resize) this callback function executes
// ---------------------------------------------------------------------------------------------
void framebuffer_size_callback(GLFWwindow* window, int width, int height)
{
	// make sure the viewport matches the new window dimensions; note that width and 
	// height will be significantly larger than specified on retina displays.
	glViewport(0, 0, width, height);
}

// glfw: whenever the mouse moves, this callback is called
// -------------------------------------------------------
void mouse_callback(GLFWwindow* window, double xpos, double ypos)
{
	if (flyingCameraActive)
	{
		if (firstMouse)
		{
			lastX = xpos;
			lastY = ypos;
			firstMouse = false;
		}

		float xoffset = xpos - lastX;
		float yoffset = lastY - ypos; // reversed since y-coordinates go from bottom to top

		lastX = xpos;
		lastY = ypos;

		camera.RotateWithMouse(xoffset, yoffset);
	}
}

// glfw: whenever the mouse scroll wheel scrolls, this callback is called
// ----------------------------------------------------------------------
void scroll_callback(GLFWwindow* window, double xoffset, double yoffset)
{
	camera.Zoom(yoffset);
}

// utility function for loading a 2D texture from file
// ---------------------------------------------------
unsigned int loadTexture(char const* path)
{
	unsigned int textureID;
	glGenTextures(1, &textureID);

	int width, height, nrComponents;
	unsigned char* data = stbi_load(path, &width, &height, &nrComponents, 0);
	if (data)
	{
		GLenum format;
		if (nrComponents == 1)
			format = GL_RED;
		else if (nrComponents == 3)
			format = GL_RGB;
		else if (nrComponents == 4)
			format = GL_RGBA;

		glBindTexture(GL_TEXTURE_2D, textureID);
		glTexImage2D(GL_TEXTURE_2D, 0, format, width, height, 0, format, GL_UNSIGNED_BYTE, data);
		glGenerateMipmap(GL_TEXTURE_2D);

		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

		stbi_image_free(data);
	}
	else
	{
		std::cout << "Texture failed to load at path: " << path << std::endl;
		stbi_image_free(data);
	}

	return textureID;
}

