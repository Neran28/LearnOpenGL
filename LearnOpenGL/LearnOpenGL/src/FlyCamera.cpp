#include "FlyCamera.h"

#include <iostream>

FlyCamera::FlyCamera(const glm::vec3& position, const glm::vec3& up, float yaw, float pitch)
    : position(position), worldUp(up), yaw(yaw), pitch(pitch), front(glm::vec3(0.0f, 0.0f, -1.0f)), movementSpeed(SPEED), mouseSensitivity(SENSITIVITY), fov(FOV)
{
    updateCameraVectors();
    startPosition = this->position;
    startFront = this->front;
    startUp = this->up;
    startYaw = this->yaw;
    startPitch = this->pitch;
}

void FlyCamera::Move(Camera_Movement direction, float deltaTime)
{
    float velocity = movementSpeed * deltaTime;
    switch (direction)
    {
    case Camera_Movement::FORWARD:
        position += front * velocity;
        break;
    case Camera_Movement::BACKWARD:
        position -= front * velocity;
        break;
    case Camera_Movement::LEFT:
        position -= right * velocity;
        break;
    case Camera_Movement::RIGHT:
        position += right * velocity;
        break;
    case Camera_Movement::UP:
        position += up * velocity;
        break;
    case Camera_Movement::DOWN:
        position -= up * velocity;
        break;
    default:
        break;
    }
}

void FlyCamera::RotateWithMouse(float xOffset, float yOffset, bool constrainPitch)
{
    xOffset *= mouseSensitivity;
    yOffset *= mouseSensitivity;
    yaw += xOffset;
    pitch += yOffset;

    if (constrainPitch)
    {
        if (pitch > 89.9f)
            pitch = 89.9f;
        if (pitch < -89.9f)
            pitch = -89.9f;
    }

    updateCameraVectors();

    //std::cout << "Yaw: " << yaw << " Pitch: " << pitch << std::endl;
    std::cout << "F: " << front.x << ", " << front.y << ", " << front.z << std::endl;
    std::cout << "R: " << right.x << ", " << right.y << ", " << right.z << std::endl;
    std::cout << "U: " << up.x << ", " << up.y << ", " << up.z << std::endl;
}

void FlyCamera::Zoom(float value)
{
    fov -= value;
    if (fov < 1.0f)
        fov = 1.0f;
    if (fov > 45.0f)
        fov = 45.0f;
}

void FlyCamera::Reset()
{
    pitch = startPitch;
    yaw = startYaw;
    position = startPosition;
    front = startFront;
    up = startUp;
}

glm::mat4 FlyCamera::GetViewMatrix() const
{
    return glm::lookAt(position, position + front, up);
}

// calculates front vector from the Camera's updated euler angles
void FlyCamera::updateCameraVectors()
{
    glm::vec3 direction;
    direction.x = cos(glm::radians(yaw)) * cos(glm::radians(pitch));
    direction.y = sin(glm::radians(pitch));
    direction.z = sin(glm::radians(yaw)) * cos(glm::radians(pitch));
    front = glm::normalize(direction);
    
    right = glm::normalize(glm::cross(front, worldUp));
    up = glm::normalize(glm::cross(right, front));
}
