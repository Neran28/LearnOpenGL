#version 330 core

out vec4 FragColor;

in vec2 TexCoord;

uniform sampler2DMS screenTextureMS;

int viewport_width = 800;
int viewport_height = 600;

void main()
{
    //texelFetch requires a vec of ints for indexing (since we're indexing pixel locations)
	//texture coords is range [0, 1], we need range [0, viewport_dim].
	//texture coords are essentially a percentage, so we can multiply text coords by total size 
	ivec2 vpCoords = ivec2(viewport_width, viewport_height);
	vpCoords.x = int(vpCoords.x * TexCoord.x); 
	vpCoords.y = int(vpCoords.y * TexCoord.y);

	//do a simple average since this is just a demo
	vec4 sample1 = texelFetch(screenTextureMS, vpCoords, 0);
	vec4 sample2 = texelFetch(screenTextureMS, vpCoords, 1);
	vec4 sample3 = texelFetch(screenTextureMS, vpCoords, 2);
	vec4 sample4 = texelFetch(screenTextureMS, vpCoords, 3);
	FragColor = (sample1 + sample2 + sample3 + sample4) / 4.0;
} 
