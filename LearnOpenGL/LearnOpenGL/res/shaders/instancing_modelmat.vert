// use with default_textured.frag
#version 330 core

layout(location = 0) in vec3 vPos;
layout(location = 2) in vec2 vTexCoord;
layout(location = 3) in mat4 instanceMatrix;	// locations 3,4,5,6 ergeben die matrix

out vec2 TexCoord;

uniform mat4 view;
uniform mat4 projection;

void main() {
	TexCoord = vTexCoord;
    gl_Position = projection * view * instanceMatrix * vec4(vPos, 1.0f); 
}
