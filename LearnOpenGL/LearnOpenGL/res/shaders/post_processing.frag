#version 330 core

in vec2 TexCoord;

out vec4 FragColor;

uniform sampler2D screenTexture;

const float offset = 1.0 / 300.0;



vec4 inverseColor() {
	return vec4(vec3(1.0 - texture(screenTexture, TexCoord)), 1.0);
}

// verschiedene kernel operationen
vec4 kernelEffect() {
		// kernels 
	vec2 offsets[9] = vec2[] (
	vec2(-offset,  offset),
	vec2( 0.0f,	   offset),
	vec2( offset,  offset),
	vec2(-offset,  0.0f),
	vec2( 0.0f,	   0.0f),
	vec2( offset,  0.0f),
	vec2(-offset, -offset),
	vec2( 0.0f,	  -offset),
	vec2( offset, -offset)
	);

	// sharp filter
//	float kernel[9] = float[] (
//		-1, -1, -1,
//		-1,  9, -1,
//		-1, -1, -1
//	);

//	// gauss blur filter
//	float kernel[9] = float[] (
//		1.0/16, 2.0/16, 1.0/16,
//		2.0/16, 4.0/16, 2.0/16,
//		1.0/16, 2.0/16, 1.0/16
//	);

	// edge detection bzw. outline (inverted w�re es edge in eigener farbe anzeigen)
	float kernel[9] = float[] (
		1, 1, 1,
		1,-8, 1,
		1, 1, 1
	);

	vec3 sampleTex[9];
	for (int i = 0; i < 9; ++i) {
		sampleTex[i] = vec3(texture(screenTexture, TexCoord.st + offsets[i]));
	}

	vec3 col = vec3 (0.0);
	for (int i = 0; i < 9; ++i) {
		col += sampleTex[i] * kernel[i];
	}

	return vec4(col, 1.0);
}

// ka was das sein soll 
vec4 mein_lustiger_effekt() {
	vec4 FragColor = texture(screenTexture, TexCoord);
	if(TexCoord.x < 0.5 && TexCoord.y >= 0.5)
		FragColor = vec4(FragColor.r*0.5, FragColor.g * 0.25, FragColor.b = 0.25, 1.0);
	else if(TexCoord.x >= 0.5 && TexCoord.y >= 0.5)
		FragColor = vec4(FragColor.r*0.25, FragColor.g * 0.5, FragColor.b = 0.25, 1.0);
	else if(TexCoord.x < 0.5 && TexCoord.y < 0.5)
		FragColor = vec4(FragColor.r*0.25, FragColor.g * 0.25, FragColor.b = 0.5, 1.0);
	else if(TexCoord.x >= 0.5 && TexCoord.y < 0.5)
	{
		float average = 0.2126 * FragColor.r + 0.7152 * FragColor.g + 0.0722 * FragColor.b;
		FragColor = vec4(average, average, average, 1.0);
	}

	return FragColor;
}

vec4 grayscale_vergleich() {
	// sRGB to grayscale
	vec4 FragColor = texture(screenTexture, TexCoord);
	float average;
	if(TexCoord.x < 0.5) {
		average = 0.2126 * FragColor.r + 0.7152 * FragColor.g + 0.0722 * FragColor.b;	// sRGB to grayscale, summe der faktoren ist 1!
	}
	else {
		average = pow(0.2126 * pow(FragColor.r, 2.2) + 0.7152 * pow(FragColor.g, 2.2) + 0.0722 * pow(FragColor.b, 2.2), 1.0/2.2);	// irgendwie mit gamma
	}

	FragColor = vec4(average, average, average, 1.0);
	return FragColor;
}

void main() {

	FragColor = inverseColor();
//	FragColor = grayscale_vergleich();
//	FragColor = kernelEffect();
//	FragColor = mein_lustiger_effekt();
}