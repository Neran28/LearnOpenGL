#version 330 core
out vec4 FragColor;

in VS_OUT {
    vec3 FragPos;
    vec3 Normal;
    vec3 TangentLightPos;
    vec3 TangentViewPos;
    vec3 TangentFragPos;
    vec2 TexCoord;
    vec4 FragPosLightSpace;
} fs_in;

uniform sampler2D diffuseTexture;
uniform sampler2D normalMap;
uniform sampler2D depthMap;

uniform bool useParallaxMapping;
uniform bool useNormalMap;
uniform float heightScale;

// for directional shadow map
uniform sampler2D shadowMap;
uniform vec3 lightPos;

uniform vec3 viewPos;

// return 1.0 if light, 0.0 if in shadow
float sampleShadowMap(vec2 projCoord, float currentDepth) {
    float closestDepth = texture(shadowMap, projCoord).r;
    return currentDepth < closestDepth ? 1.0 : 0.0;
}

float sampleShadowMapBilinear(vec2 projCoord, float currentDepth, vec2 texelSize) {
    // texel entspricht projCoord im pixel space
	vec2 texel = projCoord/texelSize + vec2(0.5);
	vec2 fraction = fract(texel);
	vec2 startTexel = (texel - fraction - vec2(0.5)) * texelSize;

	float blTexel = sampleShadowMap(startTexel, currentDepth);
	float brTexel = sampleShadowMap(startTexel + vec2(texelSize.x, 0.0), currentDepth);
	float tlTexel = sampleShadowMap(startTexel + vec2(0.0, texelSize.y), currentDepth);
	float trTexel = sampleShadowMap(startTexel + texelSize, currentDepth);

	float mixA = mix(blTexel, brTexel, fraction.x);
	float mixB = mix(tlTexel, trTexel, fraction.x);
	return mix(mixA, mixB, fraction.y);
}

float shadowCalculation(vec4 fragPosLightSpace) {
    // perform perspective divide projCoord is in Range [-1, 1]
    vec3 projCoord = fragPosLightSpace.xyz / fragPosLightSpace.w;
    // transform NDC to texture coordinates in range [0, 1]
    projCoord = projCoord * 0.5 + 0.5;

    // early return falls das fragment aus light sicht au�erhalb der farplane der shadow map liegt
    // Alles au�erhalb immer lit machen ohne if w�re es immer im shadow weil z = 1.0 immer kleiner als ein Wert > 1.0 ist
    if(projCoord.z > 1.0)
        return 1.0;
     
    // sample depth texture using transformed coordinate
    float closestDepth = texture(shadowMap, projCoord.xy).r;
    float currentDepth = projCoord.z;

    vec3 normal = normalize(fs_in.Normal);
    vec3 lightDir = normalize(lightPos - fs_in.FragPos);
    // falls oberfl�che zu lightDir orthogonal dann kleiner offset, ansonsten gr��er, gr��er = n�her an light source
    float bias = max(0.05 * (1.0 - dot(normal, lightDir)), 0.005);

    vec2 texelSize = 1.0 / textureSize(shadowMap, 0);

      // komischer versuch um PCF ordentlich zu samplen
//    vec3 vShadowTexDDX = dFdx(projCoord);
//    vec3 vShadowTexDDY = dFdy(projCoord);
//
//    mat2x2 matScreenToShadow = mat2x2(vShadowTexDDX.xy, vShadowTexDDY.xy);
//    mat2x2 matShadowToScreen = inverse(matScreenToShadow);
//    vec2 rightShadowTexelLocation = vec2(texelSize.x, 0.0);
//    vec2 upShadowTexelLocation = vec2(0.0, texelSize.y);
//    vec2 rightTexelDepthRatio = matShadowToScreen * rightShadowTexelLocation;
//    vec2 upTexelDepthRatio = matShadowToScreen * upShadowTexelLocation;
//
//    float upTexelDepthDelta = upTexelDepthRatio.x * vShadowTexDDX.z + upTexelDepthRatio.y * vShadowTexDDY.z;
//    float rightTexelDepthDelta = rightTexelDepthRatio.x * vShadowTexDDX.z + rightTexelDepthRatio.y * vShadowTexDDY.z;
//
//    float depthcompare = 0.0;

    // Percentage closer filtering
    float lit = 0.0;
    int pcfSize = 1;
    for (int x = -pcfSize; x <= pcfSize; ++x)
    {
        for (int y = -pcfSize; y <= pcfSize; ++y)
        {
            // depthcompare += abs(rightTexelDepthDelta * x + upTexelDepthDelta * y);
            
            //lit += sampleShadowMap(projCoord.xy + vec2(x, y) * texelSize, currentDepth - bias);
            lit += sampleShadowMapBilinear(projCoord.xy + vec2(x, y) * texelSize, currentDepth - bias, texelSize);
        }
    }
    lit /= ((pcfSize*2+1) * (pcfSize*2+1));

    //lit = sampleShadowMap(projCoord.xy, currentDepth - bias);
    //lit = sampleShadowMapBilinear(projCoord.xy, currentDepth - bias, texelSize);
    return lit;
}

// viewDirection is in tangent space
vec2 ParallaxMapping(vec2 texCoord, vec3 viewDirection) {
    // on steep viewing angle choose a high amount of layers
    const float minDepthLayers = 16;
    const float maxDepthLayers = 64;
    float numDepthLayers = mix(maxDepthLayers, minDepthLayers, abs(dot(vec3(0, 0, 1), viewDirection)));
    float layerDepth = 1.0 / numDepthLayers;
    float currentLayerDepth = 0.0;

    vec2 P = viewDirection.xy * heightScale;
    vec2 deltaTexCoord = P / numDepthLayers;

    // initial values
    vec2 currentTexCoord = texCoord;
    float currentDepthValue = texture(depthMap, currentTexCoord).r;
    // loop over each depth layer until the tc offset along P returns a depth below the displaced surface
    while(currentLayerDepth < currentDepthValue) 
    {
        currentTexCoord -= deltaTexCoord;
        currentDepthValue = texture(depthMap, currentTexCoord).r;
        currentLayerDepth += layerDepth;
    }

    // parallax occlusion mapping extension
    vec2 previousTexCoord = currentTexCoord + deltaTexCoord;
    float afterDepth = currentDepthValue - currentLayerDepth;
    float beforeDepth = texture(depthMap, previousTexCoord).r - currentLayerDepth + layerDepth;

    float weight = afterDepth / (afterDepth - beforeDepth);
    vec2 displacedTexCoord = previousTexCoord * weight + (1.0 - weight) * currentTexCoord;

    // default parallax mapping
//    float depth = texture(depthMap, texCoord).r;
//    vec2 displacedTexCoord = texCoord - viewDirection.xy * depth * heightScale;

    return displacedTexCoord;
}

void main()
{
    vec2 texCoord = fs_in.TexCoord;
    // offset texture coordinates using parallax mapping
    if(useParallaxMapping)
    {
        vec3 viewDir = normalize(fs_in.TangentViewPos - fs_in.TangentFragPos);
        
        texCoord = ParallaxMapping(texCoord, viewDir);
        if(texCoord.x > 1.0 || texCoord.y > 1.0 || texCoord.x < 0.0 || texCoord.y < 0.0)
            discard;
    }

    vec3 color = texture(diffuseTexture, texCoord).rgb;
    vec3 normal = normalize(fs_in.Normal);
    vec3 lightColor = vec3(0.8);

    // ambient
    vec3 ambient = 0.3 * color;
    float diff;
    float spec;

    if(useNormalMap)
    {
        normal = texture(normalMap, texCoord).rgb;
        normal = normalize(normal * 2.0 - 1.0);
        
        vec3 TangentLightDir = normalize(fs_in.TangentLightPos - fs_in.TangentFragPos);
        diff = max(dot(TangentLightDir, normal), 0.0);

        vec3 TangentViewDir = normalize(fs_in.TangentViewPos - fs_in.TangentFragPos);
        vec3 halfwayDir = normalize(TangentLightDir + TangentViewDir);
        spec = pow(max(dot(normal, halfwayDir), 0.0), 64.0);
    }
    else
    {
        // diffuse
        vec3 lightDir = normalize(lightPos - fs_in.FragPos);
        diff = max(dot(lightDir, normal), 0.0);
        // specular
        vec3 viewDir = normalize(viewPos - fs_in.FragPos);
        vec3 halfwayDir = normalize(lightDir + viewDir);  
        spec = pow(max(dot(normal, halfwayDir), 0.0), 64.0);
    }

    vec3 diffuse = diff * lightColor;
    vec3 specular = spec * lightColor;    
    // calculate shadow - 0 = not in shadow, 1 = in shadow
    float shadowMultiplier = shadowCalculation(fs_in.FragPosLightSpace);       
    vec3 lighting = (ambient + shadowMultiplier * (diffuse + specular)) * color;    
    
    FragColor = vec4(lighting, 1.0);
}