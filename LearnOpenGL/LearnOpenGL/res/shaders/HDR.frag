#version 330

out vec4 FragColor;

in vec2 TexCoords;

uniform sampler2D hdrSceneTexture;
uniform sampler2D bloomTexture;
uniform bool hdr;
uniform bool bloom;
uniform float exposure;

const float gamma = 2.2;

void main() {
	vec3 hdrColor = texture(hdrSceneTexture, TexCoords).rgb;
	vec3 bloomColor = texture(bloomTexture, TexCoords).rgb;
	if(bloom)
		hdrColor += bloomColor;

	// das tonemapping k�nnte man auch im fragment shader hdr_lit.frag machen statt einen floating point buffer zu nutzen.
	vec3 toneMapped = hdrColor;
	
	if(hdr)
	{
		// exposure tone mapping
		toneMapped = vec3(1.0) - exp(-hdrColor * exposure);
		// reinhard tone mapping
//		toneMapped = hdrColor / (hdrColor + vec3(1.0));
	}
	// gamma correction
	toneMapped = pow(toneMapped, vec3(1.0/gamma));
    FragColor = vec4(toneMapped, 1.0);
}