#version 330 core

out float FragColor;

in vec2 TexCoords;

uniform sampler2D gPosition;
uniform sampler2D gNormal;
uniform sampler2D noiseTexture;

uniform vec3 samples[64];

// tile noise texture over screen, based on screen dimensins divided by noise size
const vec2 noiseScale = vec2(800.0/4.0, 600.0/4.0); // screen = 800x600

layout (std140) uniform Matrices
{
	mat4 view;
	mat4 projection;
};

void main()
{    
    vec3 fragPos = texture(gPosition, TexCoords).xyz;
	vec3 normal = texture(gNormal, TexCoords).xyz;
	vec3 randomRot = texture(noiseTexture, TexCoords * noiseScale).xyz;
}