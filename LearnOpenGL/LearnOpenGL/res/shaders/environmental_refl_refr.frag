#version 330 core

out vec4 FragColor;

in vec3 Normal;
in vec3 worldPosition;

uniform vec3 cameraPos;
uniform samplerCube skybox;

void main() {
	vec3 N = normalize(Normal);

	vec3 I = normalize(worldPosition - cameraPos);
	vec3 R = reflect(I, N);

//	float ratio = 1.00 / 2.42;
//	vec3 R = refract(I, N, ratio);

	R = vec3(-R.x, R.y, R.z);	// flip x because its also flipped when rendering the skybox
	FragColor = vec4(texture(skybox, R).rgb, 1.0);
}