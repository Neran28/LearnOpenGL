#version 330 core

layout(location = 0) in vec3 vPos;
layout(location = 1) in vec3 vNormal;

out VS_OUT {
	vec3 Normal;
} vs_out;

uniform mat4 model;

layout (std140) uniform Matrices
{
	mat4 view;
	mat4 projection;
};

void main() {
	gl_Position = view * model * vec4(vPos, 1.0);
	mat3 normalMatrix = mat3(transpose(inverse(view * model)));
	vs_out.Normal = normalize(vec3(vec4(normalMatrix * vNormal, 0.0)));
}