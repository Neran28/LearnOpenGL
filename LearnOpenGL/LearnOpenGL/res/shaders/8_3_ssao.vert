#version 330 core
layout (location = 0) in vec3 vPos;
layout (location = 1) in vec3 vNormal;
layout (location = 2) in vec2 vTexCoords;

out vec3 FragPos;
out vec2 TexCoords;
out vec3 Normal;

layout (std140) uniform Matrices
{
	mat4 view;
	mat4 projection;
};

uniform mat4 model;

void main()
{
    vec4 viewPos = view * model * vec4(vPos, 1.0);
    FragPos = viewPos.xyz;
    TexCoords = vTexCoords;
    
    mat3 normalMatrix = transpose(inverse(mat3(view*model)));
    Normal = normalMatrix * vNormal;

    gl_Position = projection * viewPos;
}