#version 330 core

out vec4 FragColor;

in vec2 TexCoords;

uniform sampler2D image;

uniform bool horizontal;
uniform float weight[5] = float [] (0.2270270270, 0.1945945946, 0.1216216216, 0.0540540541, 0.0162162162);

uniform bool useLinearFiltering;
// weight ist linear, f�r bilinearFiltering addiert man die beiden sample weights
uniform float weightsLinearFiltering[3] = float [] (0.2270270270, 0.3162162162, 0.0702702703);
// offsetL(t1, t2) = ( offsetD(t1) * weightD (t1) + offsetD(t2)  * weightD(t2) ) / weightL(t1,t2)
const float offsets[3] = float [] (0.0, 1.3846153846, 3.2307692308);

void main() {
	vec2 texOffset = 1.0 / textureSize(image, 0);
	vec3 result = texture(image, TexCoords).rgb * weight[0];

	if(useLinearFiltering)
	{
		if (horizontal)
		{
			for (int i = 1; i < 3; ++i)
			{
				result += texture(image, TexCoords + vec2(texOffset.x * offsets[i], 0.0)).rgb * weightsLinearFiltering[i];
				result += texture(image, TexCoords - vec2(texOffset.x * offsets[i], 0.0)).rgb * weightsLinearFiltering[i];
			}
		}
		else 
		{
			for (int i = 1; i < 3; ++i)
			{
				result += texture(image, TexCoords + vec2(0.0, texOffset.y * offsets[i])).rgb * weightsLinearFiltering[i];
				result += texture(image, TexCoords - vec2(0.0, texOffset.y * offsets[i])).rgb * weightsLinearFiltering[i];
			}
		}
	}
	else 
	{
		if (horizontal)
		{
			for (int i = 1; i < 5; ++i)
			{
				result += texture(image, TexCoords + vec2(texOffset.x * i, 0.0)).rgb * weight[i];
				result += texture(image, TexCoords - vec2(texOffset.x * i, 0.0)).rgb * weight[i];
			}
		}
		else 
		{
			for (int i = 1; i < 5; ++i)
			{
				result += texture(image, TexCoords + vec2(0.0, texOffset.y * i)).rgb * weight[i];
				result += texture(image, TexCoords - vec2(0.0, texOffset.y * i)).rgb * weight[i];
			}
		}
	}

	FragColor = vec4(result, 1.0);
}