#version 330 core

layout(location = 0) in vec2 vPos;
layout(location = 1) in vec3 vColor;
layout(location = 2) in vec2 vOffset;

out vec3 Color;

//uniform vec2 offsets[100];

void main() 
{
	//vec2 offset = offsets[gl_InstanceID];
	vec2 pos = vPos * (gl_InstanceID / 100.0);
	gl_Position = vec4(pos + vOffset, 0.0, 1.0);
	Color = vColor;
}