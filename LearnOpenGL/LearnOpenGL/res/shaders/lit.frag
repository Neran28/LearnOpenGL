#version 330 core

struct Material {
    sampler2D texture_diffuse1;
    sampler2D texture_specular1;
    float shininess; // darf nicht 0 sein
};


struct DirLight {
    vec3 direction;

    vec3 ambient;
    vec3 diffuse;
    vec3 specular;
};

struct PointLight {
    vec3 position;

    vec3 ambient;
    vec3 diffuse;
    vec3 specular;

    float attenuation_constant;
    float attenuation_linear;
    float attenuation_quadratic;
};
#define NR_POINT_LIGHTS 4

struct SpotLight {
    vec3 position;
    vec3 direction;
    float cutOff;
    float outerCutOff;

    vec3 ambient;
    vec3 diffuse;
    vec3 specular;

    float attenuation_constant;
    float attenuation_linear;
    float attenuation_quadratic;
};

in VS_OUT {
    vec3 FragPos;
    vec3 Normal;
    vec2 TexCoords;
} fs_in;

out vec4 fragColor;

uniform Material material;
uniform DirLight dirLight;
uniform PointLight pointLights[NR_POINT_LIGHTS];
uniform SpotLight spotLight;

uniform vec3 viewPos;

vec3 CalcDirLight(DirLight light, vec3 normal, vec3 viewDir);
vec3 CalcPointLight(PointLight light, vec3 normal, vec3 fragPos, vec3 viewDir); // wieso fragPos �bergeben wenn es bereits ein in param ist
vec3 CalcSpotLight(SpotLight light, vec3 normal, vec3 fragPos, vec3 viewDir);

void main()
{
    vec3 outColor = vec3(0.0);

    vec3 N = normalize(fs_in.Normal); // could be no longer normalized after interpolation
    vec3 V = normalize(viewPos - fs_in.FragPos);

    // compute directional lighting contribution
    outColor += CalcDirLight(dirLight, N, V);

    // compute point lights contribution
    for(int i = 0; i < NR_POINT_LIGHTS; ++i) 
    {
        outColor += CalcPointLight(pointLights[i], N, fs_in.FragPos, V);
    }

    // compute spot light contribution
    outColor += CalcSpotLight(spotLight, N, fs_in.FragPos, V);

    fragColor = vec4(outColor, 1.0);
}

vec3 CalcDirLight(DirLight light, vec3 normal, vec3 viewDir) {
    vec3 L = normalize(-light.direction); // vector soll zur light zeigen
    
    // diffuse shading
    float d = max(dot(normal, L), 0.0);

    // specular shading
    vec3 H = normalize(L + viewDir);
    float s = pow(max(0.0, dot(H, normal)), material.shininess);

    // combine color
    vec3 matDiffuse = vec3(texture(material.texture_diffuse1, fs_in.TexCoords));
    vec3 ambient = light.ambient * matDiffuse;
    vec3 diffuse = light.diffuse * matDiffuse * d;
    vec3 specular = light.specular * vec3(texture(material.texture_specular1, fs_in.TexCoords)) * s;

    return (ambient + diffuse + specular);
}

vec3 CalcPointLight(PointLight light, vec3 normal, vec3 fragPos, vec3 viewDir) {
    vec3 L = normalize(light.position - fragPos);

    // diffuse shading
    float d = max(dot(normal, L), 0.0);

    // specular shading
    vec3 H = normalize(L + viewDir);
    float s = pow(max(0.0, dot(H, normal)), material.shininess);

    // attenuation
    float distance = length(light.position - fragPos);
    float attenuation = 1.0 / (light.attenuation_constant + light.attenuation_linear * distance +
                        light.attenuation_quadratic * distance * distance);

    // combine color
    vec3 matDiffuse = texture(material.texture_diffuse1, fs_in.TexCoords).rgb;
    vec3 ambient = light.ambient * matDiffuse;
    vec3 diffuse = light.diffuse * matDiffuse * d;
    vec3 specular = light.specular * texture(material.texture_specular1, fs_in.TexCoords).rgb * s;
    ambient *= attenuation;
    diffuse *= attenuation;
    specular *= attenuation;
    
    return (ambient + diffuse + specular);
}

vec3 CalcSpotLight(SpotLight light, vec3 normal, vec3 fragPos, vec3 viewDir) {
    vec3 L = normalize(light.position - fragPos);

    // diffuse shading
    float d = max(dot(normal, L), 0.0);

    // specular shading
    vec3 H = normalize(L + viewDir);
    float s = pow(max(0.0, dot(H, normal)), material.shininess);

    // phong lighting
    vec3 matDiffuse = vec3(texture(material.texture_diffuse1, fs_in.TexCoords));
    vec3 ambient = light.ambient * matDiffuse;
    vec3 diffuse = light.diffuse * matDiffuse * d;
    vec3 specular = light.specular * vec3(texture(material.texture_specular1, fs_in.TexCoords)) * s;

    // soft edges spotlight
    float theta = dot(normalize(-light.direction), L);
    float epsilon = light.cutOff - light.outerCutOff;
    float spotIntensity = clamp((theta - light.outerCutOff) / epsilon, 0.0, 1.0);
    diffuse *= spotIntensity;
    specular *= spotIntensity;

    // attenuation
    float distance = length(light.position - fragPos);
    float attenuation = 1.0 / (light.attenuation_constant + light.attenuation_linear * distance +
                        light.attenuation_quadratic * distance * distance);
    ambient *= attenuation;
    diffuse *= attenuation;
    specular *= attenuation;
  
    return (ambient + diffuse + specular);
}