#version 330 core

out vec4 FragColor;

//in vec2 TexCoords;

struct Light {
	vec3 Position;
	vec3 Color;
	float Linear;
	float Quadratic;
	//float Radius;
};
const int NR_LIGHTS = 32;

uniform sampler2D gPosition;
uniform sampler2D gNormal;
uniform sampler2D gAlbedoSpec;

uniform vec3 viewPos;
//uniform Light lights[NR_LIGHTS];
uniform Light lights;

layout (std140) uniform Matrices
{
	mat4 view;
	mat4 projection;
};

void main() {
	vec2 gScreenSize = textureSize(gNormal, 0);
	vec2 TexCoords = gl_FragCoord.xy / gScreenSize;
	vec3 FragPos = texture(gPosition, TexCoords).rgb;
	vec3 N = normalize(texture(gNormal, TexCoords).rgb);
	vec4 albedoSpec = texture(gAlbedoSpec, TexCoords);
	vec3 albedo = albedoSpec.rgb;
	float material_specular = albedoSpec.a;

	vec3 viewPos_vs = (view * vec4(viewPos, 1.0)).xyz;
	vec3 lightPos = (view * vec4(lights.Position, 1.0)).xyz;

	// ambient
	vec3 lighting = vec3(0.0);
	vec3 ambient = 0.1 * albedo;
	vec3 V = normalize(viewPos_vs - FragPos);

	// diffuse
	vec3 L = normalize(lightPos - FragPos);
	vec3 diff = max(dot(N, L), 0.0) * albedo * lights.Color;
	// specular
	vec3 H = normalize(V + L);
	float s = pow(max(dot(N, H), 0.0), 16.0);
	vec3 spec = lights.Color * material_specular * s;

	// attenuation
	float dist = length(lightPos - FragPos);
	float attenuation = 1.0 / (1.0 + lights.Linear * dist + lights.Quadratic * dist * dist);
	ambient *= attenuation;
	diff *= attenuation;
	spec *= attenuation;
		
	lighting = ambient + diff + spec;
	//lighting += lights.Color * 0.03;
	FragColor = vec4(lighting, 1.0);
}