#version 330 core

out vec4 FragColor;

in vec2 TexCoord;

uniform sampler2D tex;

void main() {

	vec2 texelSize = 1.0 / textureSize(tex, 0);
	vec2 texel = TexCoord/texelSize + vec2(0.5);
	vec2 fraction = fract(texel);
	vec2 startTexel = (texel - fraction - vec2(0.5)) * texelSize;

	vec4 blTexel = texture(tex, startTexel);
	vec4 brTexel = texture(tex, startTexel + vec2(texelSize.x, 0.0));
	vec4 tlTexel = texture(tex, startTexel + vec2(0.0, texelSize.y));
	vec4 trTexel = texture(tex, startTexel + texelSize);

	vec4 c1 = mix(blTexel, brTexel, fraction.x);
	vec4 c2 = mix(tlTexel, trTexel, fraction.x);
	vec4 c = mix(c1, c2, fraction.y);

	//c = texture(tex, TexCoord);
	FragColor = c;
}

