#version 330 core
out vec4 FragColor;

in vec2 TexCoords;

uniform sampler2D texture1;

float near = 0.1;
float far = 100.0;

float LinearizeDepth(float depth) {
    // back to NDC
    // die formel ist abh�ngig von glDepthRange() (default ist [0;1])
    //          depth = (((farZ-nearZ) * z_ndc) + nearZ + farZ) / 2.0;
    // default: depth = (z_ndc + 1) / 2.0
    float z_ndc = depth * 2.0 - 1.0;
    
    // near und far ist hier das der projection matrix
    return (2.0 * near * far) / (far + near - z_ndc * (far - near)); 
}

void main()
{    
    //float depth = LinearizeDepth(gl_FragCoord.z) / far;
    FragColor = texture(texture1, TexCoords);
}
