#version 420 core

out vec4 FragColor;

in VS_OUT {
    vec3 FragPos;
    vec3 Normal;
    vec2 TexCoord;
} fs_in;

uniform sampler2D diffuseTexture;

// for omnidirectional shadow map
uniform samplerCube shadowMap;
uniform vec3 lightPos;
uniform float far_plane;
uniform bool shadows;

uniform vec3 viewPos;

// array of offset direction for sampling
vec3 gridSamplingDisk[20] = vec3[]
(
   vec3(1, 1,  1), vec3( 1, -1,  1), vec3(-1, -1,  1), vec3(-1, 1,  1), 
   vec3(1, 1, -1), vec3( 1, -1, -1), vec3(-1, -1, -1), vec3(-1, 1, -1),
   vec3(1, 1,  0), vec3( 1, -1,  0), vec3(-1, -1,  0), vec3(-1, 1,  0),
   vec3(1, 0,  1), vec3(-1,  0,  1), vec3( 1,  0, -1), vec3(-1, 0, -1),
   vec3(0, 1,  1), vec3( 0, -1,  1), vec3( 0, -1, -1), vec3( 0, 1, -1)
);

// return 1.0 if light, 0.0 if in shadow
float sampleShadowMap(vec3 projCoord, float currentDepth) {
    float closestDepth = texture(shadowMap, projCoord).r;
    closestDepth *= far_plane;
    return currentDepth < closestDepth ? 1.0 : 0.0;
}

//float sampleShadowMapBilinear(vec2 projCoord, float currentDepth, vec2 texelSize) {
//    // texel entspricht projCoord im pixel space
//	vec2 texel = projCoord/texelSize + vec2(0.5);
//	vec2 fraction = fract(texel);
//	vec2 startTexel = (texel - fraction - vec2(0.5)) * texelSize;
//
//    vec3 
//	float blTexel = sampleShadowMap(startTexel, currentDepth);
//	float brTexel = sampleShadowMap(startTexel + vec2(texelSize.x, 0.0), currentDepth);
//	float tlTexel = sampleShadowMap(startTexel + vec2(0.0, texelSize.y), currentDepth);
//	float trTexel = sampleShadowMap(startTexel + texelSize, currentDepth);
//
//	float mixA = mix(blTexel, brTexel, fraction.x);
//	float mixB = mix(tlTexel, trTexel, fraction.x);
//	return mix(mixA, mixB, fraction.y);
//}

float shadowCalculation(vec3 fragPos)
{
    vec3 fragToLight = fragPos - lightPos;

    float currentDepth = length(fragToLight);
    // early terminate if outside of lights far plane
//    if(currentDepth > far_plane)
//        return 1.0;
    

    // PCF 1.0 - Das ist aber nicht wirklich gut, weil man in die plane "reinsampled" was 0 sinn macht.
    // Mit samples = 4.0 in etwa gleich schnell wie 3.0
//    float bias = 0.05;
//    float lit = 0.0;
//    float samples = 4.0;
//    float offset = 0.1;
//    for (float x = -offset; x < offset; x+= offset / (samples * 0.5))
//    {
//        for (float y = -offset; y < offset; y+= offset / (samples * 0.5))
//        {
//            for (float z = -offset; z < offset; z+= offset / (samples * 0.5))
//            {
//                lit += sampleShadowMap(fragToLight + vec3(x, y, z), currentDepth - bias);
//            }
//        }
//    }
//    lit /= (samples * samples * samples);

    // PCF 2.0 - Das ist aber nicht wirklich gut, weil man in die plane "reinsampled" was 0 sinn macht.
    // Langsamer als 3.0
//    vec2 asd = textureSize(shadowMap, 0);
//    vec3 texelSize = asd.xxx;
//    float lit = 0.0;
//    float bias = 0.05;
//    int samples = 1;
//    for (float x = -samples; x <= samples; ++x)
//    {
//        for (float y = -samples; y <= samples; ++y)
//        {
//            for (float z = -samples; z <= samples; ++z)
//            {
//                lit += sampleShadowMap(fragToLight + vec3(x, y, z) * 0.1, currentDepth - bias);
//            }
//        }
//    }
//    lit /= ((samples*2+1) * (samples*2+1) * (samples*2+1));

    // PCF 3.0 - Am schnellsten ca. 0.01s schneller als 1.0 mit samples = 4.0
    // aber sieht schlechter als 1.0 aus.
    float lit = 0.0;
    float bias = 0.15;
    int samples = 20;
    float viewDistance = length(viewPos - fragPos);
    float diskRadius = (1.0 + (viewDistance / far_plane)) / 25.0;
    for (int i = 0; i < samples; ++i)
    {
        lit += sampleShadowMap(fragToLight + gridSamplingDisk[i] * diskRadius, currentDepth - bias);
    }
    lit /= float(samples);


//    float closestDepth = texture(shadowMap, fragToLight).r;
//    closestDepth *= far_plane;
//    FragColor = vec4(vec3(closestDepth / far_plane), 1.0);
    return lit;
}

void main()
{           
    vec3 color = texture(diffuseTexture, fs_in.TexCoord).rgb;
    vec3 normal = normalize(fs_in.Normal);
    vec3 lightColor = vec3(0.3);
    // ambient
    vec3 ambient = 0.3 * color;
    // diffuse
    vec3 lightDir = normalize(lightPos - fs_in.FragPos);
    float diff = max(dot(lightDir, normal), 0.0);
    vec3 diffuse = diff * lightColor;
    // specular
    vec3 viewDir = normalize(viewPos - fs_in.FragPos);
    float spec = 0.0;
    vec3 halfwayDir = normalize(lightDir + viewDir);  
    spec = pow(max(dot(normal, halfwayDir), 0.0), 64.0);
    vec3 specular = spec * lightColor;    
    // calculate shadow - 1 = not in shadow, 0 = in shadow
    float shadowMultiplier = shadows ? shadowCalculation(fs_in.FragPos) : 1.0;       
    vec3 lighting = (ambient + shadowMultiplier * (diffuse + specular)) * color;    
    
    FragColor = vec4(lighting, 1.0);
}