#version 330

layout (location = 0) in vec3 aPos;

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;

float scale = 1.05;

void main()
{
    gl_Position = projection * view *  model * mat4(mat3(scale)) * vec4(aPos, 1.0);
}