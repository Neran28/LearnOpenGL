#version 330 core

layout (location = 0) in vec3 vPos;
layout (location = 1) in vec3 vNormal;
layout (location = 2) in vec2 vTexCoord;

out VS_OUT {
    vec3 FragPos;
    vec3 Normal;
    vec2 TexCoord;
} vs_out;

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;

uniform bool reverse_normals;

void main()
{    
    vs_out.FragPos = vec3(model * vec4(vPos, 1.0));
    
    vec3 N = reverse_normals ? - vNormal : vNormal;
    mat3 normalMatrix = transpose(inverse(mat3(model)));
    vs_out.Normal = normalize(normalMatrix * N);
    
    vs_out.TexCoord = vTexCoord;
    gl_Position = projection * view * vec4(vs_out.FragPos, 1.0);
}