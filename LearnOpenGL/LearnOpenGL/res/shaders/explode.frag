#version 330 core

out vec4 FragColor;

in GS_OUT {
	vec2 TexCoord;
	vec3 Normal;
	vec3 FragPos;
} fs_in;

uniform sampler2D texture_diffuse1;

void main()
{
    FragColor = texture(texture_diffuse1, fs_in.TexCoord);
}