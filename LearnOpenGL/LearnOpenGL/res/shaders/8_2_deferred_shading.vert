#version 330 core

layout (location = 0) in vec3 vPos;
//layout (location = 1) in vec2 vTexCoords;

//out vec2 TexCoords;

layout (std140) uniform Matrices
{
	mat4 view;
	mat4 projection;
};

uniform mat4 model;

void main() {
	//TexCoords = vTexCoords;
	gl_Position = projection * view * model * vec4(vPos, 1.0);
}