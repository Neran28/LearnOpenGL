#version 330 core

out vec4 FragColor;

in vec2 TexCoord;

float rectLength = 0.8;
float rectHeight = 0.1;
float Y_PAD = 0.015;
vec2 rect1 = vec2(0.1, 0.1);


void drawRect(vec2 start, float w, float h, vec3 c1, vec3 c2) {

	vec3 color;
	bool drawToFragment = false;

	if( (TexCoord.x >= start.x && TexCoord.x <= (start.x + w)) && (TexCoord.y >= start.y && TexCoord.y <= (start.y + h))) {
		drawToFragment = true;
		vec2 relativeCoord;
		relativeCoord.x = (TexCoord.x - start.x) / w;
		relativeCoord.y = (TexCoord.y - start.y) / h;

		// draw linear space
		// physical linear brightness
		if(relativeCoord.y > 0.5) {
			color = c2 * relativeCoord.x + (1-relativeCoord.x) * c1;
		}
		// draw sRGB space
		// perceived linear brighness
		else {
			color = c2 * relativeCoord.x + (1-relativeCoord.x) * c1;
			color.r = pow(color.r, 2.2);
			color.g = pow(color.g, 2.2);
			color.b = pow(color.b, 2.2);
		}
	}

	if(drawToFragment)
		FragColor = vec4(color, 1.0);
}

void main() {
	FragColor = vec4(1);

	drawRect(rect1, rectLength, rectHeight, vec3(1.0, 0.0, 0.0), vec3(0.0, 1.0, 0.0));
	rect1.y += rectHeight + Y_PAD;
	drawRect(rect1, rectLength, rectHeight, vec3(0.0, 0.0, 0.0), vec3(1.0, 1.0, 1.0));

	FragColor.r = pow(FragColor.r, (1.0/2.2));
	FragColor.g = pow(FragColor.g, (1.0/2.2));
	FragColor.b = pow(FragColor.b, (1.0/2.2));
}

