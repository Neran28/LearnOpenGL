#version 330 core

layout(location = 0) in vec3 vPos;
layout(location = 1) in vec3 vNormal;

out vec3 Normal;
out vec3 worldPosition;

uniform mat4 model;

layout (std140) uniform Matrices
{
	mat4 view;
	mat4 projection;
};

void main() {
	Normal = mat3(transpose(inverse(model))) * vNormal;
	worldPosition = vec3(model * vec4(vPos, 1.0));
	gl_Position = projection * view * vec4(worldPosition, 1.0);
}