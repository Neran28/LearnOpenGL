#version 330 core

layout (triangles) in;
layout (triangle_strip, max_vertices = 3) out;

out GS_OUT {
	vec2 TexCoord;
	vec3 Normal;
	vec3 FragPos;
} gs_out;


in VS_OUT {
	vec2 TexCoord;
	vec3 FragPos;
	vec3 Normal;
} gs_in[];

layout (std140) uniform Matrices
{
	mat4 view;
	mat4 projection;
};

uniform float time;

vec4 explode(vec4 position, vec3 normal) {
	float magnitude = 2.0;
	vec3 direction = normal * ((sin(time) + 1.0) / 2.0) * magnitude;
	return position + vec4(direction, 0.0);
}

// oder cross(a, b) und a = v1 - v0 und b = v2 - v0. Daumen ist a Zeigefinger ist b und mittelfinger der rechten Hand ist cross
vec3 GetNormal() {
	vec3 a = vec3(gl_in[0].gl_Position) - vec3(gl_in[1].gl_Position);
	vec3 b = vec3(gl_in[2].gl_Position) - vec3(gl_in[1].gl_Position);
	return normalize(cross(b, a));
}

void main() {
	vec3 normal = GetNormal();

	vec4 vPos = explode(gl_in[0].gl_Position, normal);
	gl_Position = projection * view * vPos;
	gs_out.TexCoord = gs_in[0].TexCoord;
	gs_out.Normal = gs_in[0].Normal;
	gs_out.FragPos = vPos.xyz;
	EmitVertex();
	vPos = explode(gl_in[1].gl_Position, normal);
	gl_Position = projection * view * vPos;
	gs_out.TexCoord = gs_in[1].TexCoord;
	gs_out.Normal = gs_in[1].Normal;
	gs_out.FragPos = vPos.xyz;
	EmitVertex();
	vPos = explode(gl_in[2].gl_Position, normal);
	gl_Position = projection * view * vPos;
	gs_out.TexCoord = gs_in[2].TexCoord;
	gs_out.Normal = gs_in[2].Normal;
	gs_out.FragPos = vPos.xyz;
	EmitVertex();
	EndPrimitive();
//    vec3 normal = GetNormal();
//
//    gl_Position = projection * explode(gl_in[0].gl_Position, normal);
//    TexCoord = gs_in[0].TexCoord;
//    EmitVertex();
//    gl_Position = projection * explode(gl_in[1].gl_Position, normal);
//    TexCoord = gs_in[1].TexCoord;
//    EmitVertex();
//    gl_Position = projection * explode(gl_in[2].gl_Position, normal);
//    TexCoord = gs_in[2].TexCoord;
//    EmitVertex();
//    EndPrimitive();
}