#version 330 core

layout(location = 0) in vec3 vPos;

out vec3 TexCoord;

uniform mat4 view;
uniform mat4 projection;

void main() {
	TexCoord = vec3(-vPos.x, vPos.y, vPos.z);
	// nach dem vertex shader kommt perspective division. Dort wird x y z durch w geteilt. Dadurch erh�lt man die normalized device coordinates.
	// In den normalized device coordinates ist Z die Tiefe, die f�r das depth testing benutzt wird. Setzt man Z gleich W ist die Tiefe dann immer 1.0
	// also das Maximum das im depth buffer stehen kann. Bei einem LEQUAL Vergleich wird nun was discarded falls zuvor was reingeschrieben wurde.
	vec4 position = projection * view * vec4(vPos, 1.0);
	gl_Position = position.xyww;
}