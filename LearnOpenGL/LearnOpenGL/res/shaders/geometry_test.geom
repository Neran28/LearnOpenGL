#version 330 core

layout(points) in;
layout(triangle_strip, max_vertices = 5) out;

// die winding order bei einem strip wird intern nach jedem triangle gewechselt
// also wenn man 123 234 345 rendern will, dann werden die vertices der einzelnen Dreiecke in dieser order intern verarbeitet
// 123 324 345 - dadurch wird die winding order eingehalten.

in VS_OUT {
	vec3 Color;
} gs_in[];

out vec3 Color;

void build_house(vec4 position) {
	Color = gs_in[0].Color;
	gl_Position = position + vec4(-0.2, -0.2, 0.0, 0.0); // bottom left
	EmitVertex();
	gl_Position = position + vec4( 0.2, -0.2, 0.0, 0.0); // bottom right
	EmitVertex();
	gl_Position = position + vec4(-0.2,  0.2, 0.0, 0.0); // top left
	EmitVertex();
	gl_Position = position + vec4( 0.2,  0.2, 0.0, 0.0); // top right
	EmitVertex();
	gl_Position = position + vec4( 0.0,  0.4, 0.0, 0.0); // top
	Color = vec3(1.0, 1.0, 1.0);
	EmitVertex();
	EndPrimitive();
}

void main() {
	build_house(gl_in[0].gl_Position);
}