#version 330 core

layout(triangles) in;
layout(triangle_strip, max_vertices = 18) out;	// 1 plane is rendered = 2 triangles. for each of those triangles 6 triangles are formed.
												// which results in a total of 36 triangles (=cube) after all geometry shader calls.
uniform mat4 shadowMatrices[6]; // projection * view

out vec4 FragPos;

void main() {
	for (int face = 0; face < 6; ++face)
	{
		gl_Layer = face;
		for (int i = 0; i < 3; ++i) // for each vertex of the triangle input
		{
			FragPos = gl_in[i].gl_Position;
			gl_Position = shadowMatrices[face] * FragPos;
			EmitVertex();
		}
		EndPrimitive();
	}
}