#version 330 core

layout (location = 0) in vec3 vPos;
layout (location = 1) in vec3 vNormal;
layout (location = 2) in vec2 vTexCoord;

out VS_OUT {
	vec2 TexCoord;
	vec3 FragPos;
	vec3 Normal;
} vs_out;


uniform mat4 model;

layout (std140) uniform Matrices
{
	mat4 view;
	mat4 projection;
};


void main()
{
	gl_Position = model * vec4(vPos, 1.0);
	vs_out.FragPos = vec3(model * vec4(vPos, 1.0));
	vs_out.Normal = mat3(transpose(inverse(model))) * vNormal;
	vs_out.TexCoord = vTexCoord;
}