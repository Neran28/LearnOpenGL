#version 330 core

layout(location = 0) in vec3 vPos;
layout(location = 1) in vec3 vColor;

uniform float hOffset;

out vec3 vertexColor;
out vec3 vertexPos;

void main()
{
	gl_Position = vec4(vPos.x + hOffset, vPos.y, vPos.z, 1.0f);
	vertexColor = vColor;
	vertexPos = vPos;
}