#version 330 core

struct Material {
    sampler2D diffuse;
    sampler2D specular;
    float shininess;
};

struct Light {
    vec3 position; // not necessary when using directional lights
    vec3 direction;
    float cutOff;
    float outerCutOff;

    vec3 ambient;
    vec3 diffuse;
    vec3 specular;

    float attenuation_constant;
    float attenuation_linear;
    float attenuation_quadratic;
};

in vec2 texCoord;
in vec3 FragPos;
in vec3 Normal;

out vec4 fragColor;

uniform Material material;
uniform Light light;
uniform vec3 viewPos;



void main()
{
    vec3 result = vec3(0.0);

    vec3 N = normalize(Normal);
    vec3 L = normalize(light.position - FragPos);

    vec3 matDiffuse = vec3(texture(material.diffuse, texCoord));
    vec3 ambient = matDiffuse * light.ambient;
    
    float theta = dot(L, normalize(-light.direction));
    float epsilon = light.cutOff - light.outerCutOff;
    float intensity = clamp((theta - light.outerCutOff) / epsilon, 0.0, 1.0);

    float d = max(0.0, dot(N, L));

    // specular
    vec3 V = normalize(viewPos - FragPos);
    vec3 R = reflect(-L, N);
    float s = pow(max(0.0, dot(V, R)), material.shininess);

    vec3 diffuse = d * matDiffuse * light.diffuse;
    vec3 specular = s * texture(material.specular, texCoord).rgb * light.specular;

    float distance = length(light.position - FragPos);
    float attenuation = 1.0 / (light.attenuation_constant + light.attenuation_linear * distance +
                        light.attenuation_quadratic * distance * distance);
        
    diffuse *= intensity;
    specular *= intensity;

    ambient  *= attenuation;
    diffuse *= attenuation;
    specular *= attenuation;
    result = ambient + diffuse + specular;

    fragColor = vec4(result, 1.0);
}