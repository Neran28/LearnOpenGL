#version 330 core

in vec2 texCoord;
in vec3 FragPos;
in vec3 Normal;
in vec3 lightPos_view;

uniform vec3 objectColor;
uniform vec3 lightColor;

out vec4 fragColor;


void main()
{
    float ambientStrength = 0.1;
    vec3 ambient = ambientStrength * lightColor;

    vec3 N = normalize(Normal);
    vec3 L = normalize(lightPos_view - FragPos);

    float diff = max(0.0, dot(N, L));
    vec3 diffuse = diff * lightColor;

    vec3 specular = vec3(0, 0, 0);
    if(diff > 0)
    {
        float specStrength = 0.5f;
        vec3 V = normalize(-FragPos);
        vec3 R = reflect(-L, N);
        float spec = pow(max(0.0, dot(V, R)), 32);
        specular = spec * specStrength * lightColor;
    }

    vec3 result = (ambient + diffuse + specular) * objectColor;
    fragColor = vec4(result, 1.0);
}