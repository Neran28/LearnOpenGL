#version 330 core

layout (location = 0) in vec3 vPos;
layout (location = 1) in vec3 vNormal;


uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;

uniform vec3 lightPos;
uniform vec3 lightColor;

out vec3 lightingColor;

void main()
{
	gl_Position = projection * view * model * vec4(vPos, 1.0);
	vec3 position_view = vec3(view * model * vec4(vPos, 1.0));
	vec3 Normal = mat3(transpose(inverse(view * model))) * vNormal;
	vec3 lightPos_view = vec3(view * vec4(lightPos, 1.0));

	float ambientStrength = 0.1;
    vec3 ambient = ambientStrength * lightColor;

    vec3 N = normalize(Normal);
    vec3 L = normalize(lightPos_view - position_view);

    float diff = max(0.0, dot(N, L));
    vec3 diffuse = diff * lightColor;

    vec3 specular = vec3(0, 0, 0);
    if(diff > 0)
    {
        float specStrength = 1;
        vec3 V = normalize(-position_view);
        vec3 R = reflect(-L, N);
        float spec = pow(max(0.0, dot(V, R)), 32);
        specular = spec * specStrength * lightColor;
    }

    lightingColor = ambient + diffuse + specular;
}