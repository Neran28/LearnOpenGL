#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <iostream>

#include "../src/GLDebug.h"
#include "../src/Shader.h"


GLFWwindow* window;
GLuint VBO;
GLuint VAO;
GLuint EBO;

std::shared_ptr<Shader> ex02;
std::shared_ptr<Shader> ex03;
GLint myColorUniformLocation;


const float cube_data[] = {
	 0.5f, -0.5f, 0.0f,  1.0f, 0.0f, 0.0f, // bottom right
	-0.5f, -0.5f, 0.0f,	 0.0f, 1.0f, 0.0f, // bottom left
	 0.0f,  0.5f, 0.0f,  0.0f, 0.0f, 1.0f  // top
};

const unsigned int triangle_indices[] = {
	0, 1, 2, // first triangle
};


void framebuffer_size_callback(GLFWwindow* window, int width, int height);
void processInput(GLFWwindow* window);

void init();
void draw();
void setupShader();
void setupGeometry();

int main(int argc, char** argv)
{
	glfwInit();
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

	window = glfwCreateWindow(800, 600, "LearnOpenGL", NULL, NULL);
	if (window == NULL)
	{
		std::cout << "Failed to create GLFW window" << std::endl;
		glfwTerminate();
		return -1;
	}

	glfwMakeContextCurrent(window);

	if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
	{
		std::cout << "Failed to initialize GLAD" << std::endl;
		return -1;
	}

	glViewport(0, 0, 800, 600);
	glfwSetWindowSizeCallback(window, framebuffer_size_callback);

	init();
	setupShader();
	setupGeometry();


	while (!glfwWindowShouldClose(window))
	{
		processInput(window);

		draw();

		glfwPollEvents();
	}


	glfwTerminate();
	return 0;
}

void init()
{
	const GLubyte* glVersion = glGetString(GL_VERSION); GLCheckError();
	const GLubyte* slVersion = glGetString(GL_SHADING_LANGUAGE_VERSION); GLCheckError();
	std::cout << glVersion << std::endl;
	std::cout << slVersion << std::endl;

	GLint maxAttribs;
	glGetIntegerv(GL_MAX_VERTEX_ATTRIBS, &maxAttribs); GLCheckError();
	std::cout << "Maximum nr of vertex attributes supported: " << maxAttribs << std::endl;
}

void draw()
{
	glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT);

	//float timeValue = glfwGetTime();
	//float greenValue = (sin(timeValue) / 2.0f) + 0.5f;
	//glUniform4f(myColorUniformLocation, 0.0f, greenValue, 0.0f, 1.0f);

	// ex02
	ex02->bind();
	float hOffset = 0.5f;
	ex02->setFloat("hOffset", hOffset);

	// ex03
	// bottom left is black because posValues are negative on the left side and clamped to 0.0f after rasterization
	ex03->bind();
	glBindVertexArray(VAO); GLCheckError();
	glDrawElements(GL_TRIANGLES, sizeof(triangle_indices) / sizeof(triangle_indices[0]), GL_UNSIGNED_INT, 0); GLCheckError();
	glBindVertexArray(0); GLCheckError();

	glfwSwapBuffers(window);
}


void setupShader()
{
	ex02 = std::make_shared<Shader>(Shader("res/shaders/ex02.vert", "res/shaders/ex02.frag"));
	ex03 = std::make_shared<Shader>(Shader("res/shaders/ex03.vert", "res/shaders/ex03.frag"));
}

void setupGeometry()
{
	glGenVertexArrays(1, &VAO); GLCheckError();
	glBindVertexArray(VAO); GLCheckError();

	glGenBuffers(1, &VBO); GLCheckError();
	glBindBuffer(GL_ARRAY_BUFFER, VBO); GLCheckError();
	glBufferData(GL_ARRAY_BUFFER, sizeof(cube_data), cube_data, GL_STATIC_DRAW); GLCheckError();

	glEnableVertexAttribArray(0); GLCheckError();
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(float), (void*)0); GLCheckError();

	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(float), (void*)(3 * sizeof(float))); GLCheckError();
	glEnableVertexAttribArray(1); GLCheckError();

	glGenBuffers(1, &EBO);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO); GLCheckError();
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(triangle_indices), triangle_indices, GL_STATIC_DRAW); GLCheckError();
	glBindVertexArray(0); GLCheckError();
}

void processInput(GLFWwindow* window)
{
	if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
		glfwSetWindowShouldClose(window, true);

	if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS)
		glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

	if (glfwGetKey(window, GLFW_KEY_E) == GLFW_PRESS)
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

}

void framebuffer_size_callback(GLFWwindow* window, int width, int height)
{
	glViewport(0, 0, width, height); GLCheckError();
}