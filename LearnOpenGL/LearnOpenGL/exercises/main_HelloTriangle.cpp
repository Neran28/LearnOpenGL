#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <iostream>

#include "../src/GLDebug.h"

GLFWwindow* window;
GLuint VBO[2];
GLuint VAO[2];
GLuint orangeProgram;
GLuint yellowProgram;


const float vertices1[] = {
	 1.0f,  1.0f, 0.0f,	// top right
	 1.0f,  0.0f, 0.0f, // right
	 0.0f,  0.0f, 0.0f  // center
};

const float vertices2[] = {
	-1.0f,  1.0f, 0.0f,	// top right
	-1.0f,  0.0f, 0.0f, // right
	 0.0f,  0.0f, 0.0f  // center
};

const char* vertexShaderSource =
"#version 330 core\n"
"layout(location = 0) in vec3 aPos;\n"
"void main()\n"
"{\n"
"  gl_Position = vec4(aPos.x, aPos.y, aPos.z, 1.0);\n"
"}\0";

const char* orangeShaderSource =
"#version 330 core\n"
"out vec4 fragColor;\n"
"void main()\n"
"{\n"
"  fragColor = vec4(1.0f, 0.5f, 0.2f, 1.0);\n"
"}\0";

const char* yellowShaderSource =
"#version 330 core\n"
"out vec4 fragColor;\n"
"void main()\n"
"{\n"
"  fragColor = vec4(1.0f, 1.0f, 0.2f, 1.0);\n"
"}\0";

void framebuffer_size_callback(GLFWwindow* window, int width, int height);
void processInput(GLFWwindow* window);

void checkShaderCompileStatus(GLuint shaderId);
void checkProgramLinkStatus(GLuint programId);
void init();
void draw();
void setupShader();
void setupGeometry();

int main(int argc, char** argv)
{
	glfwInit();
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

	window = glfwCreateWindow(800, 600, "LearnOpenGL", NULL, NULL);
	if (window == NULL)
	{
		std::cout << "Failed to create GLFW window" << std::endl;
		glfwTerminate();
		return -1;
	}

	glfwMakeContextCurrent(window);

	if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
	{
		std::cout << "Failed to initialize GLAD" << std::endl;
		return -1;
	}

	glViewport(0, 0, 800, 600);
	glfwSetWindowSizeCallback(window, framebuffer_size_callback);

	init();
	setupShader();
	setupGeometry();


	while (!glfwWindowShouldClose(window))
	{
		processInput(window);

		draw();

		glfwPollEvents();
	}


	glfwTerminate();
	return 0;
}


void init()
{
	const GLubyte* glVersion = glGetString(GL_VERSION); GLCheckError();
	const GLubyte* slVersion = glGetString(GL_SHADING_LANGUAGE_VERSION); GLCheckError();
	std::cout << glVersion << std::endl;
	std::cout << slVersion << std::endl;

	//	glPolygonMode(GL_FRONT_AND_BACK, GL_LINE); GLCheckError();
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL); GLCheckError();
}

void draw()
{
	glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT);

	glBindVertexArray(VAO[0]); GLCheckError();
	glUseProgram(orangeProgram); GLCheckError();
	glDrawArrays(GL_TRIANGLES, 0, 3);
	glBindVertexArray(0); GLCheckError();

	glBindVertexArray(VAO[1]); GLCheckError();
	glUseProgram(yellowProgram); GLCheckError();
	glDrawArrays(GL_TRIANGLES, 0, 3); GLCheckError();
	glBindVertexArray(0); GLCheckError();

	glfwSwapBuffers(window);
}

void setupShader()
{
	GLuint vertexShader;
	GLuint fragmentShader;

	vertexShader = glCreateShader(GL_VERTEX_SHADER);
	glShaderSource(vertexShader, 1, &vertexShaderSource, NULL);
	glCompileShader(vertexShader);
	checkShaderCompileStatus(vertexShader);

	fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(fragmentShader, 1, &orangeShaderSource, NULL);
	glCompileShader(fragmentShader);
	checkShaderCompileStatus(fragmentShader);

	orangeProgram = glCreateProgram();
	glAttachShader(orangeProgram, vertexShader);
	glAttachShader(orangeProgram, fragmentShader);
	glLinkProgram(orangeProgram);
	checkProgramLinkStatus(orangeProgram);

	glUseProgram(orangeProgram); GLCheckError();
	glDeleteShader(fragmentShader); GLCheckError();

	fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(fragmentShader, 1, &yellowShaderSource, NULL);
	glCompileShader(fragmentShader);
	checkShaderCompileStatus(fragmentShader);

	yellowProgram = glCreateProgram();
	glAttachShader(yellowProgram, vertexShader);
	glAttachShader(yellowProgram, fragmentShader);
	glLinkProgram(yellowProgram);
	checkProgramLinkStatus(yellowProgram);

	glDeleteShader(vertexShader); GLCheckError();
	glDeleteShader(fragmentShader); GLCheckError();
}

void setupGeometry()
{
	glGenVertexArrays(2, VAO); GLCheckError();
	glBindVertexArray(VAO[0]); GLCheckError();

	glGenBuffers(2, VBO); GLCheckError();
	glBindBuffer(GL_ARRAY_BUFFER, VBO[0]); GLCheckError();
	glBufferData(GL_ARRAY_BUFFER, sizeof(vertices1), vertices1, GL_STATIC_DRAW); GLCheckError();

	glEnableVertexAttribArray(0); GLCheckError();
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), (void*)0); GLCheckError();

	glBindVertexArray(VAO[1]); GLCheckError();
	glBindBuffer(GL_ARRAY_BUFFER, VBO[1]); GLCheckError();
	glBufferData(GL_ARRAY_BUFFER, sizeof(vertices2), vertices2, GL_STATIC_DRAW); GLCheckError();

	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), (void*)0); GLCheckError();
	glEnableVertexAttribArray(0); GLCheckError();

	glBindVertexArray(0); GLCheckError();
}

void checkProgramLinkStatus(GLuint programId)
{
	GLint success;
	glGetProgramiv(programId, GL_LINK_STATUS, &success);
	if (!success)
	{
		char infoLog[512];
		glGetProgramInfoLog(programId, 512, NULL, infoLog);
		std::cout << "ERROR::PROGRAM::LINK_FAILED\n" << infoLog << std::endl;
	}
}

void checkShaderCompileStatus(GLuint shaderId)
{
	GLint success;
	glGetShaderiv(shaderId, GL_COMPILE_STATUS, &success);
	if (!success)
	{
		char infoLog[512];
		glGetShaderInfoLog(shaderId, 512, NULL, infoLog);
		std::cout << "ERROR::SHADER::VERTEX::COMPILATION_FAILED\n" << infoLog << std::endl;
	}
}

void processInput(GLFWwindow* window)
{
	if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
		glfwSetWindowShouldClose(window, true);
}

void framebuffer_size_callback(GLFWwindow* window, int width, int height)
{
	glViewport(0, 0, width, height); GLCheckError();
}