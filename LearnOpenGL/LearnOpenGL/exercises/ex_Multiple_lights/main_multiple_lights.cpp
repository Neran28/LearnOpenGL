#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <iostream>
#include <array>

#include "../../src/vendor/stb_image/stb_image.h"
#include "../../src/vendor/glm/glm.hpp"
#include "../../src/vendor/glm/gtc/matrix_transform.hpp"
#include "../../src/vendor/glm/gtc/type_ptr.hpp"

#include "../../src/GLDebug.h"
#include "../../src/Shader.h"
#include "../../src/FlyCamera.h"

GLFWwindow* window;
const unsigned int SCR_WIDTH = 800;
const unsigned int SCR_HEIGHT = 600;
std::string title = "";

GLuint VBO;
GLuint VAO;
GLuint pointLightVAO;
GLuint texture_container;
GLuint texture_smiley;
GLuint texture_container2;
GLuint texture_container2_specular;

std::shared_ptr<Shader> litProgram;
std::shared_ptr<Shader> whiteProgram;

glm::mat4 model;
glm::mat4 view;
glm::mat4 projection;
GLint myColorUniformLocation;
GLint modelMatLocation;
GLint viewMatLocation;
GLint projectionMatLocation;

const float cube_data[] = {
	// positions          // normals           // texture coords
	-0.5f, -0.5f, -0.5f,  0.0f,  0.0f, -1.0f,  0.0f, 0.0f,
	 0.5f, -0.5f, -0.5f,  0.0f,  0.0f, -1.0f,  1.0f, 0.0f,
	 0.5f,  0.5f, -0.5f,  0.0f,  0.0f, -1.0f,  1.0f, 1.0f,
	 0.5f,  0.5f, -0.5f,  0.0f,  0.0f, -1.0f,  1.0f, 1.0f,
	-0.5f,  0.5f, -0.5f,  0.0f,  0.0f, -1.0f,  0.0f, 1.0f,
	-0.5f, -0.5f, -0.5f,  0.0f,  0.0f, -1.0f,  0.0f, 0.0f,

	-0.5f, -0.5f,  0.5f,  0.0f,  0.0f, 1.0f,   0.0f, 0.0f,
	 0.5f, -0.5f,  0.5f,  0.0f,  0.0f, 1.0f,   1.0f, 0.0f,
	 0.5f,  0.5f,  0.5f,  0.0f,  0.0f, 1.0f,   1.0f, 1.0f,
	 0.5f,  0.5f,  0.5f,  0.0f,  0.0f, 1.0f,   1.0f, 1.0f,
	-0.5f,  0.5f,  0.5f,  0.0f,  0.0f, 1.0f,   0.0f, 1.0f,
	-0.5f, -0.5f,  0.5f,  0.0f,  0.0f, 1.0f,   0.0f, 0.0f,

	-0.5f,  0.5f,  0.5f, -1.0f,  0.0f,  0.0f,  1.0f, 0.0f,
	-0.5f,  0.5f, -0.5f, -1.0f,  0.0f,  0.0f,  1.0f, 1.0f,
	-0.5f, -0.5f, -0.5f, -1.0f,  0.0f,  0.0f,  0.0f, 1.0f,
	-0.5f, -0.5f, -0.5f, -1.0f,  0.0f,  0.0f,  0.0f, 1.0f,
	-0.5f, -0.5f,  0.5f, -1.0f,  0.0f,  0.0f,  0.0f, 0.0f,
	-0.5f,  0.5f,  0.5f, -1.0f,  0.0f,  0.0f,  1.0f, 0.0f,

	 0.5f,  0.5f,  0.5f,  1.0f,  0.0f,  0.0f,  1.0f, 0.0f,
	 0.5f,  0.5f, -0.5f,  1.0f,  0.0f,  0.0f,  1.0f, 1.0f,
	 0.5f, -0.5f, -0.5f,  1.0f,  0.0f,  0.0f,  0.0f, 1.0f,
	 0.5f, -0.5f, -0.5f,  1.0f,  0.0f,  0.0f,  0.0f, 1.0f,
	 0.5f, -0.5f,  0.5f,  1.0f,  0.0f,  0.0f,  0.0f, 0.0f,
	 0.5f,  0.5f,  0.5f,  1.0f,  0.0f,  0.0f,  1.0f, 0.0f,

	-0.5f, -0.5f, -0.5f,  0.0f, -1.0f,  0.0f,  0.0f, 1.0f,
	 0.5f, -0.5f, -0.5f,  0.0f, -1.0f,  0.0f,  1.0f, 1.0f,
	 0.5f, -0.5f,  0.5f,  0.0f, -1.0f,  0.0f,  1.0f, 0.0f,
	 0.5f, -0.5f,  0.5f,  0.0f, -1.0f,  0.0f,  1.0f, 0.0f,
	-0.5f, -0.5f,  0.5f,  0.0f, -1.0f,  0.0f,  0.0f, 0.0f,
	-0.5f, -0.5f, -0.5f,  0.0f, -1.0f,  0.0f,  0.0f, 1.0f,

	-0.5f,  0.5f, -0.5f,  0.0f,  1.0f,  0.0f,  0.0f, 1.0f,
	 0.5f,  0.5f, -0.5f,  0.0f,  1.0f,  0.0f,  1.0f, 1.0f,
	 0.5f,  0.5f,  0.5f,  0.0f,  1.0f,  0.0f,  1.0f, 0.0f,
	 0.5f,  0.5f,  0.5f,  0.0f,  1.0f,  0.0f,  1.0f, 0.0f,
	-0.5f,  0.5f,  0.5f,  0.0f,  1.0f,  0.0f,  0.0f, 0.0f,
	-0.5f,  0.5f, -0.5f,  0.0f,  1.0f,  0.0f,  0.0f, 1.0f
};

const glm::vec3 cubePositions[] = {
	glm::vec3(0.0f,  0.0f,  0.0f),
	glm::vec3(2.0f,  5.0f, -15.0f),
	glm::vec3(-1.5f, -2.2f, -2.5f),
	glm::vec3(-3.8f, -2.0f, -12.3f),
	glm::vec3(2.4f, -0.4f, -3.5f),
	glm::vec3(-1.7f,  3.0f, -7.5f),
	glm::vec3(1.3f, -2.0f, -2.5f),
	glm::vec3(1.5f,  2.0f, -2.5f),
	glm::vec3(1.5f,  0.2f, -1.5f),
	glm::vec3(-1.3f,  1.0f, -1.5f)
};

std::array<glm::vec3, 4> pointLightPosition = {
	glm::vec3(0.7f,  0.2f,  2.0f),
	glm::vec3(2.3f, -3.3f, -4.0f),
	glm::vec3(-4.0f,  2.0f, -12.0f),
	glm::vec3(0.0f,  0.0f, -3.0f)
};

// time variables
double deltaTime = 0;
double time_last_frame = 0;
long totalFrames = 0;
float fps[30]{};
double time_fps = 0;

// camera data
FlyCamera camera(glm::vec3(0.0f, 0.0f, 3.0f));
bool flyingCameraActive = false;

// mouse data
float mouse[2] = { 400, 300 };
bool firstDown = false;


void scroll_callback(GLFWwindow* window, double xOffset, double yOffset);
void mouse_button_callback(GLFWwindow* window, int button, int action, int mods);
void cursor_callback(GLFWwindow* window, double xPos, double yPos);
void framebuffer_size_callback(GLFWwindow* window, int width, int height);
void processInput(GLFWwindow* window);

void init();
void draw();
void setupShader();
void setupGeometry();

int main(int argc, char** argv)
{
	glfwInit();
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

	title = "LearnOpenGL";

	window = glfwCreateWindow(SCR_WIDTH, SCR_HEIGHT, title.c_str(), NULL, NULL);
	if (window == NULL)
	{
		std::cout << "Failed to create GLFW window" << std::endl;
		glfwTerminate();
		return -1;
	}

	glfwMakeContextCurrent(window);

	if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
	{
		std::cout << "Failed to initialize GLAD" << std::endl;
		return -1;
	}

	glViewport(0, 0, 800, 600);
	glfwSetWindowSizeCallback(window, framebuffer_size_callback);
	glfwSetCursorPosCallback(window, cursor_callback);
	glfwSetMouseButtonCallback(window, mouse_button_callback);
	glfwSetScrollCallback(window, scroll_callback);

	init();
	setupShader();
	setupGeometry();


	while (!glfwWindowShouldClose(window))
	{
		processInput(window);

		draw();

		glfwPollEvents();
	}

	glDeleteVertexArrays(1, &VAO);
	glDeleteVertexArrays(1, &pointLightVAO);
	glDeleteBuffers(1, &VBO);
	glfwTerminate();
	return 0;
}

void init()
{
	const GLubyte* glVersion = glGetString(GL_VERSION); GLCheckError();
	const GLubyte* slVersion = glGetString(GL_SHADING_LANGUAGE_VERSION); GLCheckError();
	std::cout << glVersion << std::endl;
	std::cout << slVersion << std::endl;

	GLint maxAttribs;
	glGetIntegerv(GL_MAX_VERTEX_ATTRIBS, &maxAttribs); GLCheckError();
	std::cout << "Maximum nr of vertex attributes supported: " << maxAttribs << std::endl;

	stbi_set_flip_vertically_on_load(true);
	glEnable(GL_DEPTH_TEST); GLCheckError();
}

void updatePerformanceStats()
{
	int frame = totalFrames % (sizeof(fps) / sizeof(fps[0]));
	fps[frame] = 1 / deltaTime;

	if ((time_last_frame - time_fps) > 0.5) {
		time_fps = time_last_frame;
		float sum = 0;
		for (int i = 0; i < (sizeof(fps) / sizeof(fps[0])); ++i)
			sum += fps[i];

		std::stringstream s;
		float roundedFPS = (int)(sum / (sizeof(fps) / sizeof(fps[0])) * 100 + 0.5f);
		roundedFPS = (float)roundedFPS / 100;
		s << title << " - FPS: " << roundedFPS;
		glfwSetWindowTitle(window, s.str().c_str());
	}
}

void draw()
{
	++totalFrames;
	double t = glfwGetTime();
	deltaTime = t - time_last_frame;
	time_last_frame = t;
	updatePerformanceStats();

	glClearColor(0.1f, 0.1f, 0.1f, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); GLCheckError();

	view = glm::mat4(1.0f);
	view = camera.GetViewMatrix();
	projection = glm::perspective(glm::radians(camera.fov), (float)SCR_WIDTH / (float)SCR_HEIGHT, 0.1f, 100.0f);
	
	glm::vec3 lightColor = glm::vec3(1);

	glm::vec3 diffuseColor = lightColor * glm::vec3(0.8f);
	glm::vec3 ambientColor = lightColor * glm::vec3(0.05f);

	// draw lights
	whiteProgram->bind();
	whiteProgram->setMat4("view", view);
	whiteProgram->setMat4("projection", projection);
	glBindVertexArray(pointLightVAO); GLCheckError();
	for (int i = 0; i < pointLightPosition.size(); ++i) {
		model = glm::mat4(1.0f);
		model = glm::translate(model, pointLightPosition[i]);
		model = glm::scale(model, glm::vec3(0.2f));

		whiteProgram->setMat4("model", model);
		glDrawArrays(GL_TRIANGLES, 0, 36);
	}

	litProgram->bind();
	litProgram->setVec3("viewPos", camera.position);
	litProgram->setInt("material.diffuse", 0);
	litProgram->setInt("material.specular", 1);
	litProgram->setFloat("material.shininess", 64.0f);

	litProgram->setVec3("dirLight.direction", -1.0f, -0.8f, 0.0f);
	litProgram->setVec3("dirLight.ambient", ambientColor);
	litProgram->setVec3("dirLight.diffuse", 0.4f, 0.4f, 0.4f);
	litProgram->setVec3("dirLight.specular", 0.5f, 0.5f, 0.5f);

	litProgram->setVec3("pointLights[0].position", pointLightPosition[0]);
	litProgram->setVec3("pointLights[0].ambient", ambientColor);
	litProgram->setVec3("pointLights[0].diffuse", diffuseColor);
	litProgram->setVec3("pointLights[0].specular", 1.0f, 1.0f, 1.0f);
	litProgram->setFloat("pointLights[0].attenuation_constant", 1.0f);
	litProgram->setFloat("pointLights[0].attenuation_linear", 0.09f);
	litProgram->setFloat("pointLights[0].attenuation_quadratic", 0.032f);

	litProgram->setVec3("pointLights[1].position", pointLightPosition[1]);
	litProgram->setVec3("pointLights[1].ambient", ambientColor);
	litProgram->setVec3("pointLights[1].diffuse", diffuseColor);
	litProgram->setVec3("pointLights[1].specular", 1.0f, 1.0f, 1.0f);
	litProgram->setFloat("pointLights[1].attenuation_constant", 1.0f);
	litProgram->setFloat("pointLights[1].attenuation_linear", 0.09f);
	litProgram->setFloat("pointLights[1].attenuation_quadratic", 0.032f);

	litProgram->setVec3("pointLights[2].position", pointLightPosition[2]);
	litProgram->setVec3("pointLights[2].ambient", ambientColor);
	litProgram->setVec3("pointLights[2].diffuse", diffuseColor);
	litProgram->setVec3("pointLights[2].specular", 1.0f, 1.0f, 1.0f);
	litProgram->setFloat("pointLights[2].attenuation_constant", 1.0f);
	litProgram->setFloat("pointLights[2].attenuation_linear", 0.09f);
	litProgram->setFloat("pointLights[2].attenuation_quadratic", 0.032f);

	litProgram->setVec3("pointLights[3].position", pointLightPosition[3]);
	litProgram->setVec3("pointLights[3].ambient", ambientColor);
	litProgram->setVec3("pointLights[3].diffuse", diffuseColor);
	litProgram->setVec3("pointLights[3].specular", 1.0f, 1.0f, 1.0f);
	litProgram->setFloat("pointLights[3].attenuation_constant", 1.0f);
	litProgram->setFloat("pointLights[3].attenuation_linear", 0.09f);
	litProgram->setFloat("pointLights[3].attenuation_quadratic", 0.032f);

	litProgram->setVec3("spotLight.position", camera.position);
	litProgram->setVec3("spotLight.direction", camera.front);
	litProgram->setVec3("spotLight.ambient", 0.0f, 0.0f, 0.0f);
	litProgram->setVec3("spotLight.diffuse", 1.0f, 1.0f, 1.0f);
	litProgram->setVec3("spotLight.specular", 1.0f, 1.0f, 1.0f);
	litProgram->setFloat("spotLight.attenuation_constant", 1.0f);
	litProgram->setFloat("spotLight.attenuation_linear", 0.09f);
	litProgram->setFloat("spotLight.attenuation_quadratic", 0.032f);
	litProgram->setFloat("spotLight.cutOff", glm::cos(glm::radians(12.5f)));
	litProgram->setFloat("spotLight.outerCutOff", glm::cos(glm::radians(15.0f)));

	// draw cubes
	glBindVertexArray(VAO); GLCheckError();
	glActiveTexture(GL_TEXTURE0); GLCheckError();
	glBindTexture(GL_TEXTURE_2D, texture_container2); GLCheckError();
	glActiveTexture(GL_TEXTURE1); GLCheckError();
	glBindTexture(GL_TEXTURE_2D, texture_container2_specular); GLCheckError();

	litProgram->setMat4(viewMatLocation, view);
	litProgram->setMat4(projectionMatLocation, projection);


	for (unsigned int i = 0; i < 10; ++i) {
		float angle = 20.0f * i;
		model = glm::mat4(1.0f);
		model = glm::translate(model, cubePositions[i]);
		model = glm::rotate(model, glm::radians(angle), glm::vec3(1.0f, 0.3f, 0.5f));
		litProgram->setMat4(modelMatLocation, model);

		glDrawArrays(GL_TRIANGLES, 0, 36); GLCheckError();		
	}

	//glDrawElements(GL_TRIANGLES, sizeof(indices)/sizeof(indices[0]), GL_UNSIGNED_INT, 0); GLCheckError();
	glBindVertexArray(0); GLCheckError();

	glfwSwapBuffers(window);
}


void setupShader()
{
	litProgram = std::make_shared<Shader>(Shader("exercises/ex_Multiple_lights/lit.vert", "exercises/ex_Multiple_lights/lit.frag"));
	whiteProgram = std::make_shared<Shader>(Shader("exercises/ex_Multiple_lights/white.vert", "exercises/ex_Multiple_lights/white.frag"));
	litProgram->bind();
	modelMatLocation = glGetUniformLocation(litProgram->m_rendererID, "model"); GLCheckError();
	viewMatLocation = glGetUniformLocation(litProgram->m_rendererID, "view"); GLCheckError();
	projectionMatLocation = glGetUniformLocation(litProgram->m_rendererID, "projection"); GLCheckError();
	litProgram->unbind();
}

void setupGeometry()
{
	glGenVertexArrays(1, &VAO); GLCheckError();
	glBindVertexArray(VAO); GLCheckError();
	glGenVertexArrays(1, &pointLightVAO); GLCheckError();

	glGenBuffers(1, &VBO); GLCheckError();
	glBindBuffer(GL_ARRAY_BUFFER, VBO); GLCheckError();
	glBufferData(GL_ARRAY_BUFFER, sizeof(cube_data), cube_data, GL_STATIC_DRAW); GLCheckError();
	// positions
	glEnableVertexAttribArray(0); GLCheckError();
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)0); GLCheckError();
	// normals
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)(3 * sizeof(float))); GLCheckError();
	glEnableVertexAttribArray(1); GLCheckError();
	// tex coords
	glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)(6 * sizeof(float))); GLCheckError();
	glEnableVertexAttribArray(2); GLCheckError();

	// light source mesh
	glBindVertexArray(pointLightVAO); GLCheckError();
	glBindBuffer(GL_ARRAY_BUFFER, VBO); GLCheckError();
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)0); GLCheckError();
	glEnableVertexAttribArray(0);

	// texture container
	glGenTextures(1, &texture_container); GLCheckError();
	glBindTexture(GL_TEXTURE_2D, texture_container); GLCheckError();
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT); GLCheckError();
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT); GLCheckError();
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR); GLCheckError();
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR); GLCheckError();

	int width, height, nrChannels;
	std::string texPath = "res/textures/container.jpg";
	unsigned char* data = stbi_load(texPath.c_str(), &width, &height, &nrChannels, 0);
	if (data) {
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, data); GLCheckError();
		glGenerateMipmap(GL_TEXTURE_2D); GLCheckError();
	}
	else
	{
		std::cout << "Failed to load texture: " << texPath << std::endl;
	}
	stbi_image_free(data);

	// texture smiley
	glGenTextures(1, &texture_smiley); GLCheckError();
	glBindTexture(GL_TEXTURE_2D, texture_smiley); GLCheckError();
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT); GLCheckError();
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT); GLCheckError();
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR); GLCheckError();
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR); GLCheckError();

	texPath = "res/textures/awesomeface.png";
	data = stbi_load(texPath.c_str(), &width, &height, &nrChannels, 0);
	if (data) {
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, data); GLCheckError();
		glGenerateMipmap(GL_TEXTURE_2D); GLCheckError();
	}
	else
	{
		std::cout << "Failed to load texture: " << texPath << std::endl;
	}
	stbi_image_free(data);

	// texture container2
	glGenTextures(1, &texture_container2); GLCheckError();
	glBindTexture(GL_TEXTURE_2D, texture_container2); GLCheckError();
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT); GLCheckError();
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT); GLCheckError();
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR); GLCheckError();
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR); GLCheckError();

	texPath = "res/textures/container2.png";
	data = stbi_load(texPath.c_str(), &width, &height, &nrChannels, 0);
	if (data) {
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, data); GLCheckError();
		glGenerateMipmap(GL_TEXTURE_2D); GLCheckError();
	}
	else
	{
		std::cout << "Failed to load texture: " << texPath << std::endl;
	}
	stbi_image_free(data);

	// texture specular
	glGenTextures(1, &texture_container2_specular); GLCheckError();
	glBindTexture(GL_TEXTURE_2D, texture_container2_specular); GLCheckError();
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT); GLCheckError();
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT); GLCheckError();
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR); GLCheckError();
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR); GLCheckError();

	texPath = "res/textures/container2_specular.png";
	data = stbi_load(texPath.c_str(), &width, &height, &nrChannels, 0);
	if (data) {
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, data); GLCheckError();
		glGenerateMipmap(GL_TEXTURE_2D); GLCheckError();
	}
	else
	{
		std::cout << "Failed to load texture: " << texPath << std::endl;
	}
	stbi_image_free(data);

	glBindTexture(GL_TEXTURE_2D, 0); GLCheckError();
}

void processInput(GLFWwindow* window)
{
	if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
		glfwSetWindowShouldClose(window, true);

	if (glfwGetKey(window, GLFW_KEY_L) == GLFW_PRESS)
			glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

	if (glfwGetKey(window, GLFW_KEY_P) == GLFW_PRESS)
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

	if (flyingCameraActive)
	{
		if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS)
			camera.Move(Camera_Movement::FORWARD, deltaTime);
		if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS)
			camera.Move(Camera_Movement::BACKWARD, deltaTime);
		if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS)
			camera.Move(Camera_Movement::LEFT, deltaTime);
		if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS)
			camera.Move(Camera_Movement::RIGHT, deltaTime);
		if (glfwGetKey(window, GLFW_KEY_E) == GLFW_PRESS)
			camera.Move(Camera_Movement::UP, deltaTime);
		if (glfwGetKey(window, GLFW_KEY_Q) == GLFW_PRESS)
			camera.Move(Camera_Movement::DOWN, deltaTime);
	}

	if (glfwGetKey(window, GLFW_KEY_R) == GLFW_PRESS)
	{
		camera.Reset();
	}
}

void scroll_callback(GLFWwindow* window, double xOffset, double yOffset)
{
	camera.Zoom(yOffset);
}

void cursor_callback(GLFWwindow* window, double xPos, double yPos)
{
	if (flyingCameraActive)
	{
		if (firstDown)
		{
			mouse[0] = xPos;
			mouse[1] = yPos;
			firstDown = false;
		}
		float xOffset = xPos - mouse[0];
		float yOffset = mouse[1] - yPos; // reversed since y-coordinates range from top to bottom
		camera.RotateWithMouse(xOffset, yOffset, true);

		mouse[0] = xPos;
		mouse[1] = yPos;
	}
}

void mouse_button_callback(GLFWwindow* window, int button, int action, int mods)
{
	if (button == GLFW_MOUSE_BUTTON_RIGHT && action == GLFW_PRESS)
	{
		glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
		firstDown = true;
		flyingCameraActive = true;
	}
	else if (button == GLFW_MOUSE_BUTTON_RIGHT && action == GLFW_RELEASE)
	{
		glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_NORMAL);
		firstDown = false;
		flyingCameraActive = false;
	}
}

void framebuffer_size_callback(GLFWwindow* window, int width, int height)
{
	glViewport(0, 0, width, height); GLCheckError();
}