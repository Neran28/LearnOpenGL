#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <iostream>

#include "../src/GLDebug.h"
#include "../src/Shader.h"
#include "../src/vendor/stb_image/stb_image.h"
#include "../src/vendor/glm/glm.hpp"
#include "../src/vendor/glm/gtc/matrix_transform.hpp"
#include "../src/vendor/glm/gtc/type_ptr.hpp"


GLFWwindow* window;
std::string title = "";
GLuint VBO;
GLuint VAO;
GLuint EBO;
GLuint texture_container;
GLuint texture_smiley;

std::shared_ptr<Shader> litProgram;

glm::mat4 model;
glm::mat4 view;
glm::mat4 projection;
GLint myColorUniformLocation;
GLint modelMatLocation;
GLint viewMatLocation;
GLint projectionMatLocation;

const float cube_data[] = {
	-0.5f, -0.5f, -0.5f,  0.0f, 0.0f,
	 0.5f, -0.5f, -0.5f,  1.0f, 0.0f,
	 0.5f,  0.5f, -0.5f,  1.0f, 1.0f,
	 0.5f,  0.5f, -0.5f,  1.0f, 1.0f,
	-0.5f,  0.5f, -0.5f,  0.0f, 1.0f,
	-0.5f, -0.5f, -0.5f,  0.0f, 0.0f,

	-0.5f, -0.5f,  0.5f,  0.0f, 0.0f,
	 0.5f, -0.5f,  0.5f,  1.0f, 0.0f,
	 0.5f,  0.5f,  0.5f,  1.0f, 1.0f,
	 0.5f,  0.5f,  0.5f,  1.0f, 1.0f,
	-0.5f,  0.5f,  0.5f,  0.0f, 1.0f,
	-0.5f, -0.5f,  0.5f,  0.0f, 0.0f,

	-0.5f,  0.5f,  0.5f,  1.0f, 0.0f,
	-0.5f,  0.5f, -0.5f,  1.0f, 1.0f,
	-0.5f, -0.5f, -0.5f,  0.0f, 1.0f,
	-0.5f, -0.5f, -0.5f,  0.0f, 1.0f,
	-0.5f, -0.5f,  0.5f,  0.0f, 0.0f,
	-0.5f,  0.5f,  0.5f,  1.0f, 0.0f,

	 0.5f,  0.5f,  0.5f,  1.0f, 0.0f,
	 0.5f,  0.5f, -0.5f,  1.0f, 1.0f,
	 0.5f, -0.5f, -0.5f,  0.0f, 1.0f,
	 0.5f, -0.5f, -0.5f,  0.0f, 1.0f,
	 0.5f, -0.5f,  0.5f,  0.0f, 0.0f,
	 0.5f,  0.5f,  0.5f,  1.0f, 0.0f,

	-0.5f, -0.5f, -0.5f,  0.0f, 1.0f,
	 0.5f, -0.5f, -0.5f,  1.0f, 1.0f,
	 0.5f, -0.5f,  0.5f,  1.0f, 0.0f,
	 0.5f, -0.5f,  0.5f,  1.0f, 0.0f,
	-0.5f, -0.5f,  0.5f,  0.0f, 0.0f,
	-0.5f, -0.5f, -0.5f,  0.0f, 1.0f,

	-0.5f,  0.5f, -0.5f,  0.0f, 1.0f,
	 0.5f,  0.5f, -0.5f,  1.0f, 1.0f,
	 0.5f,  0.5f,  0.5f,  1.0f, 0.0f,
	 0.5f,  0.5f,  0.5f,  1.0f, 0.0f,
	-0.5f,  0.5f,  0.5f,  0.0f, 0.0f,
	-0.5f,  0.5f, -0.5f,  0.0f, 1.0f
};

const unsigned int triangle_indices[] = {
	3, 1, 0, // first triangle
	3, 2, 1
};

const glm::vec3 cubePositions[] = {
	glm::vec3(0.0f,  0.0f,  0.0f),
	glm::vec3(2.0f,  5.0f, -15.0f),
	glm::vec3(-1.5f, -2.2f, -2.5f),
	glm::vec3(-3.8f, -2.0f, -12.3f),
	glm::vec3(2.4f, -0.4f, -3.5f),
	glm::vec3(-1.7f,  3.0f, -7.5f),
	glm::vec3(1.3f, -2.0f, -2.5f),
	glm::vec3(1.5f,  2.0f, -2.5f),
	glm::vec3(1.5f,  0.2f, -1.5f),
	glm::vec3(-1.3f,  1.0f, -1.5f)
};

// time variables
double deltaTime = 0;
double time_last_frame = 0;
long totalFrames = 0;
float fps[30]{};
double time_fps = 0;

// camera data
const float cameraSpeed = 2.5f;
const float radius = 10.0f;
glm::vec3 cameraPos = glm::vec3(0.0f, 0.0f, 3.0f);
glm::vec3 cameraFront = glm::vec3(0.0f, 0.0f, -1.0f);
glm::vec3 cameraUp = glm::vec3(0.0f, 1.0f, 0.0f);
float fov = 45.0f;

// mouse data
float pitch = 0, yaw = -90.0f;
float mouse[2] = { 400, 300 };
const float sensitivity = 0.1f;
bool firstDown = false;
bool flyingCameraActive = false;

void scroll_callback(GLFWwindow* window, double xOffset, double yOffset);
void mouse_button_callback(GLFWwindow* window, int button, int action, int mods);
void cursor_callback(GLFWwindow* window, double xPos, double yPos);
void framebuffer_size_callback(GLFWwindow* window, int width, int height);
void processInput(GLFWwindow* window);

void init();
void draw();
void setupShader();
void setupGeometry();

int main(int argc, char** argv)
{
	glfwInit();
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

	title = "LearnOpenGL";

	window = glfwCreateWindow(800, 600, title.c_str(), NULL, NULL);
	if (window == NULL)
	{
		std::cout << "Failed to create GLFW window" << std::endl;
		glfwTerminate();
		return -1;
	}

	glfwMakeContextCurrent(window);

	if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
	{
		std::cout << "Failed to initialize GLAD" << std::endl;
		return -1;
	}

	glViewport(0, 0, 800, 600);
	glfwSetWindowSizeCallback(window, framebuffer_size_callback);
	glfwSetCursorPosCallback(window, cursor_callback);
	glfwSetMouseButtonCallback(window, mouse_button_callback);
	glfwSetScrollCallback(window, scroll_callback);

	init();
	setupShader();
	setupGeometry();


	while (!glfwWindowShouldClose(window))
	{
		processInput(window);

		draw();

		glfwPollEvents();
	}


	glfwTerminate();
	return 0;
}

void init()
{
	const GLubyte* glVersion = glGetString(GL_VERSION); GLCheckError();
	const GLubyte* slVersion = glGetString(GL_SHADING_LANGUAGE_VERSION); GLCheckError();
	std::cout << glVersion << std::endl;
	std::cout << slVersion << std::endl;

	GLint maxAttribs;
	glGetIntegerv(GL_MAX_VERTEX_ATTRIBS, &maxAttribs); GLCheckError();
	std::cout << "Maximum nr of vertex attributes supported: " << maxAttribs << std::endl;

	stbi_set_flip_vertically_on_load(true);
	glEnable(GL_DEPTH_TEST); GLCheckError();
}

void updatePerformanceStats()
{
	int frame = totalFrames % (sizeof(fps) / sizeof(fps[0]));
	fps[frame] = 1 / deltaTime;

	if ((time_last_frame - time_fps) > 0.5) {
		time_fps = time_last_frame;
		float sum = 0;
		for (int i = 0; i < (sizeof(fps) / sizeof(fps[0])); ++i)
			sum += fps[i];

		std::stringstream s;
		float roundedFPS = (int)(sum / (sizeof(fps) / sizeof(fps[0])) * 100 + 0.5f);
		roundedFPS = (float)roundedFPS / 100;
		s << title << " - FPS: " << roundedFPS;
		glfwSetWindowTitle(window, s.str().c_str());
	}
}

void draw()
{
	++totalFrames;
	double t = glfwGetTime();
	deltaTime = t - time_last_frame;
	time_last_frame = t;
	updatePerformanceStats();

	glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); GLCheckError();

	litProgram->bind();

	glActiveTexture(GL_TEXTURE0); GLCheckError();
	glBindTexture(GL_TEXTURE_2D, texture_container); GLCheckError();
	glActiveTexture(GL_TEXTURE1); GLCheckError();
	glBindTexture(GL_TEXTURE_2D, texture_smiley); GLCheckError();

	// lookat
	view = glm::mat4(1.0f);
	glm::vec3 cameraDir{ cameraPos - (cameraPos + cameraFront) };
	cameraDir = glm::normalize(cameraDir);
	glm::vec3 cameraRight = glm::normalize(glm::cross(glm::normalize(glm::vec3{ 0,1,0 }), cameraDir));
	//glm::vec3 cameraRight = glm::normalize(glm::cross(cameraFront, cameraUp));	// cameraRight kann man so berechnen, aber nur wenn das target (cameraPos + f*cameraFront) ist ...
	glm::vec3 cameraUp = glm::cross(cameraDir, cameraRight);						// ... und dann braucht man diese Zeile auch nicht

	view = glm::translate(view, -cameraPos);
	view = glm::transpose(glm::mat4x4(glm::vec4{ cameraRight, 0 }, glm::vec4{ cameraUp, 0 }, glm::vec4{ cameraDir, 0 }, glm::vec4{ 0,0,0,1 })) * view;
	
	//view = glm::lookAt(cameraPos, glm::vec3(0, 0, 0), glm::vec3(0, 1, 0));
	
	//std::cout << "R: " << cameraRight.x << ", " << cameraRight.y << ", " << cameraRight.z << std::endl;
	//std::cout << "U: " << cameraUp.x << ", " << cameraUp.y << ", " << cameraUp.z << std::endl;
	//std::cout << "F: " << cameraFront.x << ", " << cameraFront.y << ", " << cameraFront.z << std::endl; // Das ist world space
	//std::cout << "D: " << cameraDir.x << ", " << cameraDir.y << ", " << cameraDir.z << std::endl;		// Das ist die umgekehrte Richtung
	// lookat ende

	projection = glm::perspective(glm::radians(fov), 800.0f / 600.0f, 0.1f, 100.0f);
	litProgram->setMat4(viewMatLocation, view);
	litProgram->setMat4(projectionMatLocation, projection);

	glBindVertexArray(VAO); GLCheckError();
	for (unsigned int i = 0; i < 10; ++i)
	{
		model = glm::mat4(1.0f);
		model = glm::translate(model, cubePositions[i]);
		float angle = 20.0f * i;
		model = glm::rotate(model, glm::radians(angle), glm::vec3(1.0f, 0.3f, 0.5f));
		litProgram->setMat4(modelMatLocation, model);
		glDrawArrays(GL_TRIANGLES, 0, 36); GLCheckError();
	}

	//glDrawElements(GL_TRIANGLES, sizeof(indices)/sizeof(indices[0]), GL_UNSIGNED_INT, 0); GLCheckError();
	glBindVertexArray(0); GLCheckError();

	glfwSwapBuffers(window);
}


void setupShader()
{
	litProgram = std::make_shared<Shader>(Shader("res/shaders/shader.vert", "res/shaders/shader.frag"));
	litProgram->bind();
	litProgram->setInt("texture1", 0); GLCheckError();
	glUniform1i(glGetUniformLocation(litProgram->m_rendererID, "texture2"), 1); GLCheckError();
	modelMatLocation = glGetUniformLocation(litProgram->m_rendererID, "model"); GLCheckError();
	viewMatLocation = glGetUniformLocation(litProgram->m_rendererID, "view"); GLCheckError();
	projectionMatLocation = glGetUniformLocation(litProgram->m_rendererID, "projection"); GLCheckError();
	litProgram->unbind();
}

void setupGeometry()
{
	glGenVertexArrays(1, &VAO); GLCheckError();
	glBindVertexArray(VAO); GLCheckError();

	glGenBuffers(1, &VBO); GLCheckError();
	glBindBuffer(GL_ARRAY_BUFFER, VBO); GLCheckError();
	glBufferData(GL_ARRAY_BUFFER, sizeof(cube_data), cube_data, GL_STATIC_DRAW); GLCheckError();

	glEnableVertexAttribArray(0); GLCheckError();
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void*)0); GLCheckError();

	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void*)(0 * sizeof(float))); GLCheckError();
	glEnableVertexAttribArray(1); GLCheckError();

	glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void*)(3 * sizeof(float))); GLCheckError();
	glEnableVertexAttribArray(2); GLCheckError();

	glGenBuffers(1, &EBO);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO); GLCheckError();
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(triangle_indices), triangle_indices, GL_STATIC_DRAW); GLCheckError();
	glBindVertexArray(0); GLCheckError();

	// texture container
	glGenTextures(1, &texture_container); GLCheckError();
	glBindTexture(GL_TEXTURE_2D, texture_container); GLCheckError();
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT); GLCheckError();
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT); GLCheckError();
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR); GLCheckError();
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR); GLCheckError();

	int width, height, nrChannels;
	std::string texPath = "res/textures/container.jpg";
	unsigned char* data = stbi_load(texPath.c_str(), &width, &height, &nrChannels, 0);
	if (data) {
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, data); GLCheckError();
		glGenerateMipmap(GL_TEXTURE_2D); GLCheckError();
	}
	else
	{
		std::cout << "Failed to load texture: " << texPath << std::endl;
	}
	stbi_image_free(data);

	// texture smiley
	glGenTextures(1, &texture_smiley); GLCheckError();
	glBindTexture(GL_TEXTURE_2D, texture_smiley); GLCheckError();
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT); GLCheckError();
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT); GLCheckError();
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR); GLCheckError();
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR); GLCheckError();

	texPath = "res/textures/awesomeface.png";
	data = stbi_load(texPath.c_str(), &width, &height, &nrChannels, 0);
	if (data) {
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, data); GLCheckError();
		glGenerateMipmap(GL_TEXTURE_2D); GLCheckError();
	}
	else
	{
		std::cout << "Failed to load texture: " << texPath << std::endl;
	}
	stbi_image_free(data);

	glBindTexture(GL_TEXTURE_2D, 0); GLCheckError();
}

void processInput(GLFWwindow* window)
{
	if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
		glfwSetWindowShouldClose(window, true);

	if (glfwGetKey(window, GLFW_KEY_L) == GLFW_PRESS)
		glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

	if (glfwGetKey(window, GLFW_KEY_P) == GLFW_PRESS)
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

	if (flyingCameraActive)
	{
		float cameraMovement = cameraSpeed * deltaTime;
		if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS)
			cameraPos += cameraMovement * cameraFront;
		if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS)
			cameraPos -= cameraMovement * cameraFront;
		if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS)
			cameraPos -= glm::normalize(glm::cross(cameraFront, cameraUp)) * cameraMovement;
		if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS)
			cameraPos += glm::normalize(glm::cross(cameraFront, cameraUp)) * cameraMovement;
		if (glfwGetKey(window, GLFW_KEY_E) == GLFW_PRESS)
			cameraPos += cameraMovement * cameraUp;
		if (glfwGetKey(window, GLFW_KEY_Q) == GLFW_PRESS)
			cameraPos -= cameraMovement * cameraUp;
	}

	if (glfwGetKey(window, GLFW_KEY_R) == GLFW_PRESS)
	{
		// reset camera
		yaw = -90.0f;
		pitch = 0;
		cameraPos = glm::vec3(0.0f, 0.0f, 3.0f);
		cameraFront = glm::vec3(0.0f, 0.0f, -1.0f);
		cameraUp = glm::vec3(0.0f, 1.0f, 0.0f);
	}
}

void scroll_callback(GLFWwindow* window, double xOffset, double yOffset)
{
	fov -= (float)yOffset;
	if (fov < 1)
		fov = 1;
	if (fov > 45)
		fov = 45;
}

void cursor_callback(GLFWwindow* window, double xPos, double yPos)
{
	if (flyingCameraActive)
	{
		if (firstDown)
		{
			mouse[0] = xPos;
			mouse[1] = yPos;
			firstDown = false;
		}

		float xOffset = xPos - mouse[0];
		float yOffset = mouse[1] - yPos; // reversed since y-coordinates range from top to bottom

		xOffset *= sensitivity;
		yOffset *= sensitivity;
		yaw += xOffset;
		pitch += yOffset;


		if (pitch > 89.0f)
			pitch = 89.0f;
		if (pitch < -89.0f)
			pitch = -89.0f;


		glm::vec3 direction;
		direction.x = cos(glm::radians(yaw)) * cos(glm::radians(pitch));
		direction.y = sin(glm::radians(pitch));
		direction.z = sin(glm::radians(yaw)) * cos(glm::radians(pitch));
		cameraFront = glm::normalize(direction);

		glm::vec3 right = glm::normalize(glm::cross(cameraFront, glm::vec3(0.0, 1.0, 0.0)));
		cameraUp = glm::normalize(glm::cross(right, cameraFront));

		//std::cout << "Yaw: " << yaw << " Pitch: " << pitch << std::endl;
		//std::cout << "F: " << direction.x << ", " << direction.y << ", " << direction.z << std::endl;
		//std::cout << "R: " << right.x << ", " << right.y << ", " << right.z << std::endl;
		//std::cout << "U: " << cameraUp.x << ", " << cameraUp.y << ", " << cameraUp.z << std::endl;

		mouse[0] = xPos;
		mouse[1] = yPos;
	}
}

void mouse_button_callback(GLFWwindow* window, int button, int action, int mods)
{
	if (button == GLFW_MOUSE_BUTTON_RIGHT && action == GLFW_PRESS)
	{
		glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
		firstDown = true;
		flyingCameraActive = true;
	}
	else if (button == GLFW_MOUSE_BUTTON_RIGHT && action == GLFW_RELEASE)
	{
		glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_NORMAL);
		firstDown = false;
		flyingCameraActive = false;
	}
}

void framebuffer_size_callback(GLFWwindow* window, int width, int height)
{
	glViewport(0, 0, width, height); GLCheckError();
}