#version 330 core

struct Material {
    sampler2D diffuse;
    sampler2D specular;
    sampler2D emission;
    float shininess;
};

struct Light {
    vec3 position;

    vec3 ambient;
    vec3 diffuse;
    vec3 specular;
};

in vec2 texCoord;
in vec3 FragPos;
in vec3 Normal;

out vec4 fragColor;

uniform Material material;
uniform Light light;
uniform vec3 viewPos;



void main()
{
    vec3 N = normalize(Normal);
    vec3 L = normalize(light.position - FragPos);

    vec3 matDiffuse = vec3(texture(material.diffuse, texCoord));

    float d = max(0.0, dot(N, L));

    // specular
    vec3 V = normalize(viewPos - FragPos);
    vec3 R = reflect(-L, N);
    float s = pow(max(0.0, dot(V, R)), material.shininess);

    vec3 ambient = matDiffuse * light.ambient;
    vec3 diffuse = d * matDiffuse * light.diffuse;
    vec3 specular = s * texture(material.specular, texCoord).rgb * light.specular;
    //vec3 specular = s * (vec3(1) - vec3(texture(material.specular, texCoord))) * light.specular; // Task 2: invert specular map

    vec3 emission;
    //vec3 show = step(vec3(1.0), vec3(1.0) - texture(material.specular, texCoord).rgb); oder das hier
    if(texture(material.specular, texCoord).rgb == vec3(0))
    {
        emission = texture(material.emission, texCoord).rgb;
    }

    vec3 result = ambient + diffuse + specular + emission;
    fragColor = vec4(result, 1.0);
}