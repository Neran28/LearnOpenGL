#version 330 core

struct Material {
    vec3 ambient;
    vec3 diffuse;
    vec3 specular;
    float shininess;
};

struct Light {
    vec3 position;

    vec3 ambient;
    vec3 diffuse;
    vec3 specular;
};

in vec2 texCoord;
in vec3 FragPos;
in vec3 Normal;

uniform vec3 objectColor;
uniform vec3 lightColor;
uniform vec3 lightPos;
uniform vec3 viewPos;

out vec4 fragColor;

uniform Material material;
uniform Light light;



void main()
{
    vec3 N = normalize(Normal);
    vec3 L = normalize(lightPos - FragPos);

    // ambient
    vec3 ambient = material.ambient * light.ambient;

    // diffuse
    float d = max(0.0, dot(N, L));
    vec3 diffuse = d * material.diffuse * light.diffuse;

    // specular
    vec3 V = normalize(viewPos - FragPos);
    vec3 R = reflect(-L, N);
    float s = pow(max(0.0, dot(V, R)), material.shininess);
    vec3 specular = s * material.specular * light.specular;

    vec3 result = ambient + diffuse + specular;
    fragColor = vec4(result, 1.0);
}