#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <assimp/scene.h>
#include <iostream>
#include <sstream>
#include <map>

#include "../../src/vendor/stb_image/stb_image.h"
#include "../../src/vendor/glm/glm.hpp"
#include "../../src/vendor/glm/gtc/matrix_transform.hpp"
#include "../../src/vendor/glm/gtc/type_ptr.hpp"

#include "../../src/GLDebug.h"
#include "../../src/Shader.h"
#include "../../src/FlyCamera.h"
#include "../../src/Model.h"


GLFWwindow* window;
const unsigned int SCR_WIDTH = 800;
const unsigned int SCR_HEIGHT = 600;
std::string title;

std::shared_ptr<Shader> shader_lit;
std::shared_ptr<Shader> screen_shader;

glm::mat4 model;
glm::mat4 view;
glm::mat4 projection;

GLuint cubeVAO, cubeVBO;
GLuint plane_vao, planeVBO;
GLuint transparentVAO, transparentVBO;
GLuint quadVAO, quadVBO;
GLuint fbo;
GLuint rbo;

GLuint cubeTexture, floorTexture, transparentTexture;
GLuint texture_color_buffer;
const unsigned int REARVIEW_SCR_WIDTH = 160;
const unsigned int REARVIEW_SCR_HEIGHT = 120;
const unsigned int REARVIEW_START_POS_X = SCR_WIDTH / 2 - REARVIEW_SCR_WIDTH / 2;
const unsigned int REARVIEW_START_POS_Y = SCR_HEIGHT - REARVIEW_SCR_HEIGHT;

std::vector<glm::vec3> windows{};


// set up vertex data (and buffer(s)) and configure vertex attributes
/*
Remember: to specify vertices in a counter-clockwise winding order you need to visualize the triangle
as if you're in front of the triangle and from that point of view, is where you set their order.

To define the order of a triangle on the right side of the cube for example, you'd imagine yourself looking
straight at the right side of the cube, and then visualize the triangle and make sure their order is specified
in a counter-clockwise order. This takes some practice, but try visualizing this yourself and see that this
is correct.
*/
// ------------------------------------------------------------------
float cubeVertices[] = {
	// Back face
	-0.5f, -0.5f, -0.5f, 0.0f, 0.0f, // Bottom-left
	0.5f, 0.5f, -0.5f, 1.0f, 1.0f, // top-right
	0.5f, -0.5f, -0.5f, 1.0f, 0.0f, // bottom-right         
	0.5f, 0.5f, -0.5f, 1.0f, 1.0f, // top-right
	-0.5f, -0.5f, -0.5f, 0.0f, 0.0f, // bottom-left
	-0.5f, 0.5f, -0.5f, 0.0f, 1.0f, // top-left
	// Front face
	-0.5f, -0.5f, 0.5f, 0.0f, 0.0f, // bottom-left
	0.5f, -0.5f, 0.5f, 1.0f, 0.0f, // bottom-right
	0.5f, 0.5f, 0.5f, 1.0f, 1.0f, // top-right
	0.5f, 0.5f, 0.5f, 1.0f, 1.0f, // top-right
	-0.5f, 0.5f, 0.5f, 0.0f, 1.0f, // top-left
	-0.5f, -0.5f, 0.5f, 0.0f, 0.0f, // bottom-left
	// Left face
	-0.5f, 0.5f, 0.5f, 1.0f, 0.0f, // top-right
	-0.5f, 0.5f, -0.5f, 1.0f, 1.0f, // top-left
	-0.5f, -0.5f, -0.5f, 0.0f, 1.0f, // bottom-left
	-0.5f, -0.5f, -0.5f, 0.0f, 1.0f, // bottom-left
	-0.5f, -0.5f, 0.5f, 0.0f, 0.0f, // bottom-right
	-0.5f, 0.5f, 0.5f, 1.0f, 0.0f, // top-right
	// Right face
	0.5f, 0.5f, 0.5f, 1.0f, 0.0f, // top-left
	0.5f, -0.5f, -0.5f, 0.0f, 1.0f, // bottom-right
	0.5f, 0.5f, -0.5f, 1.0f, 1.0f, // top-right         
	0.5f, -0.5f, -0.5f, 0.0f, 1.0f, // bottom-right
	0.5f, 0.5f, 0.5f, 1.0f, 0.0f, // top-left
	0.5f, -0.5f, 0.5f, 0.0f, 0.0f, // bottom-left     
	// Bottom face
	-0.5f, -0.5f, -0.5f, 0.0f, 1.0f, // top-right
	0.5f, -0.5f, -0.5f, 1.0f, 1.0f, // top-left
	0.5f, -0.5f, 0.5f, 1.0f, 0.0f, // bottom-left
	0.5f, -0.5f, 0.5f, 1.0f, 0.0f, // bottom-left
	-0.5f, -0.5f, 0.5f, 0.0f, 0.0f, // bottom-right
	-0.5f, -0.5f, -0.5f, 0.0f, 1.0f, // top-right
	// Top face
	-0.5f, 0.5f, -0.5f, 0.0f, 1.0f, // top-left
	0.5f, 0.5f, 0.5f, 1.0f, 0.0f, // bottom-right
	0.5f, 0.5f, -0.5f, 1.0f, 1.0f, // top-right     
	0.5f, 0.5f, 0.5f, 1.0f, 0.0f, // bottom-right
	-0.5f, 0.5f, -0.5f, 0.0f, 1.0f, // top-left
	-0.5f, 0.5f, 0.5f, 0.0f, 0.0f // bottom-left        
};
float planeVertices[] = {
	// positions         // texture Coords (note we set these higher than 1 (together with GL_REPEAT as texture wrapping mode). this will cause the floor texture to repeat)
	-5.0f, -0.5f, 5.0f,  0.0f, 0.0f,
	5.0f, -0.5f, 5.0f,   2.0f, 0.0f,
	-5.0f, -0.5f, -5.0f, 0.0f, 2.0f,

	-5.0f, -0.5f, -5.0f, 0.0f, 2.0f,
	5.0f, -0.5f, 5.0f,   2.0f, 0.0f,
	5.0f, -0.5f, -5.0f,  2.0f, 2.0f
};
float transparentVertices[] = {
	// positions       // texture Coords (swapped y coordinates because texture is flipped upside down)
	0.0f, 0.5f, 0.0f,  0.0f, 0.0f,
	0.0f, -0.5f, 0.0f, 0.0f, 1.0f,
	1.0f, -0.5f, 0.0f, 1.0f, 1.0f,

	0.0f, 0.5f, 0.0f,  0.0f, 0.0f,
	1.0f, -0.5f, 0.0f, 1.0f, 1.0f,
	1.0f, 0.5f, 0.0f,  1.0f, 0.0f
};

float quadVertices[] = {
	// positions   // texCoords
	-1.0f,  1.0f,  0.0f, 1.0f,
	-1.0f, -1.0f,  0.0f, 0.0f,
	 1.0f, -1.0f,  1.0f, 0.0f,

	-1.0f,  1.0f,  0.0f, 1.0f,
	 1.0f, -1.0f,  1.0f, 0.0f,
	 1.0f,  1.0f,  1.0f, 1.0f
};

// time variables
double deltaTime = 0;
double time_last_frame = 0;
long totalFrames = 0;
float fps[30]{};
double time_fps = 0;

// camera data
FlyCamera camera(glm::vec3(0.0f, 0.0f, 3.0f));
bool flyingCameraActive = false;

// mouse data
float mouse[2] = {(float)SCR_WIDTH / 2.0, (float)SCR_HEIGHT / 2.0};
bool firstDown = false;


void scroll_callback(GLFWwindow* window, double xOffset, double yOffset);
void mouse_button_callback(GLFWwindow* window, int button, int action, int mods);
void cursor_callback(GLFWwindow* window, double xPos, double yPos);
void framebuffer_size_callback(GLFWwindow* window, int width, int height);
void processInput(GLFWwindow* window);
GLuint loadTexture(char const* path);

void init();
void draw();
void setupShader();
void setupGeometry();

int main(int argc, char** argv)
{
	glfwInit();
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

	title = "LearnOpenGL";

	window = glfwCreateWindow(SCR_WIDTH, SCR_HEIGHT, title.c_str(), NULL, NULL);
	if (window == NULL)
	{
		std::cout << "Failed to create GLFW window" << std::endl;
		glfwTerminate();
		return -1;
	}

	glfwMakeContextCurrent(window);

	if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
	{
		std::cout << "Failed to initialize GLAD" << std::endl;
		return -1;
	}


	glfwSetWindowSizeCallback(window, framebuffer_size_callback);
	glfwSetCursorPosCallback(window, cursor_callback);
	glfwSetMouseButtonCallback(window, mouse_button_callback);
	glfwSetScrollCallback(window, scroll_callback);

	init();
	setupShader();
	setupGeometry();


	while (!glfwWindowShouldClose(window))
	{
		processInput(window);

		draw();

		glfwPollEvents();
	}

	// optional: de-allocate all resources once they've outlived their purpose:
	// ------------------------------------------------------------------------
	glDeleteVertexArrays(1, &cubeVAO);
	glDeleteVertexArrays(1, &plane_vao);
	glDeleteVertexArrays(1, &transparentVAO);
	glDeleteBuffers(1, &cubeVBO);
	glDeleteBuffers(1, &planeVBO);
	glDeleteBuffers(1, &transparentVBO);

	glfwTerminate();
	return 0;
}

void init()
{
	const GLubyte* glVersion = glGetString(GL_VERSION);
	GLCheckError();
	const GLubyte* slVersion = glGetString(GL_SHADING_LANGUAGE_VERSION);
	GLCheckError();
	std::cout << glVersion << std::endl;
	std::cout << slVersion << std::endl;

	GLint maxAttribs;
	glGetIntegerv(GL_MAX_VERTEX_ATTRIBS, &maxAttribs);
	GLCheckError();
	std::cout << "Maximum nr of vertex attributes supported: " << maxAttribs << std::endl;

	stbi_set_flip_vertically_on_load(true);

	// configure global opengl state
	// -----------------------------
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LESS);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glEnable(GL_CULL_FACE);

	windows.emplace_back(-1.5f, 0.0f, -0.48f);
	windows.emplace_back(1.5f, 0.0f, 0.51f);
	windows.emplace_back(0.0f, 0.0f, 0.7f);
	windows.emplace_back(-0.3f, 0.0f, -2.3f);
	windows.emplace_back(0.5f, 0.0f, -0.6f);
}

void updatePerformanceStats()
{
	int frame = totalFrames % (sizeof(fps) / sizeof(fps[0]));
	fps[frame] = 1 / deltaTime;

	if ((time_last_frame - time_fps) > 0.5)
	{
		time_fps = time_last_frame;
		float sum = 0;
		for (int i = 0; i < (sizeof(fps) / sizeof(fps[0])); ++i)
			sum += fps[i];

		std::stringstream s;
		float roundedFPS = (int)(sum / (sizeof(fps) / sizeof(fps[0])) * 100 + 0.5f);
		roundedFPS = (float)roundedFPS / 100;
		s << title << " - FPS: " << roundedFPS;
		glfwSetWindowTitle(window, s.str().c_str());
	}
}

void render_scene(std::map<float, glm::vec3> sorted)
{
	glClearColor(0.1f, 0.1f, 0.1f, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); GLCheckError();
	glEnable(GL_DEPTH_TEST);
	
	// render normal geometry
	glEnable(GL_CULL_FACE);

	model = glm::mat4(1.0f);

	shader_lit->bind();
	shader_lit->setMat4("view", view);
	shader_lit->setMat4("projection", projection);
	// cubes
	glBindVertexArray(cubeVAO);
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, cubeTexture);
	model = glm::translate(model, glm::vec3(-1.0f, 0.0f, -1.0f));
	shader_lit->setMat4("model", model);
	glDrawArrays(GL_TRIANGLES, 0, 36);
	model = glm::mat4(1.0f);
	model = glm::translate(model, glm::vec3(2.0f, 0.0f, 0.0f));
	shader_lit->setMat4("model", model);
	glDrawArrays(GL_TRIANGLES, 0, 36);
	// floor
	glBindVertexArray(plane_vao);
	glBindTexture(GL_TEXTURE_2D, floorTexture);
	shader_lit->setMat4("model", glm::mat4(1.0f));
	glDrawArrays(GL_TRIANGLES, 0, 6);
	// grass
	// disable face culling because grass is rendered as a plane
	glDisable(GL_CULL_FACE);
	glBindVertexArray(transparentVAO);
	glBindTexture(GL_TEXTURE_2D, transparentTexture);
	for (std::map<float, glm::vec3>::reverse_iterator it = sorted.rbegin(); it != sorted.rend(); ++it)
	{
		model = glm::mat4(1.0f);
		model = glm::translate(model, it->second);
		shader_lit->setMat4("model", model);
		glDrawArrays(GL_TRIANGLES, 0, 6);
	}

	glBindVertexArray(0);
}

void draw()
{
	++totalFrames;
	double t = glfwGetTime();
	deltaTime = t - time_last_frame;
	time_last_frame = t;
	updatePerformanceStats();

	// sort transparent objects, std::map is sorted
	// ------
	std::map<float, glm::vec3> sorted;
	for (glm::vec3& window_pos : windows)
	{
		float distance = glm::length(camera.position - window_pos);
		sorted[distance] = window_pos;
	}

	glViewport(0, 0, SCR_WIDTH, SCR_HEIGHT);
	// render rear view into texture
	glBindFramebuffer(GL_FRAMEBUFFER, fbo);
	camera.yaw += 180.0f;
	camera.pitch = -camera.pitch;
	camera.RotateWithMouse(0, 0, false); // update camera vectors with disabled constrains
	view = camera.GetViewMatrix();
	projection = glm::perspective(glm::radians(camera.fov), (float)SCR_WIDTH / (float)SCR_HEIGHT, 0.1f, 100.0f);
	render_scene(sorted);

	//reset camera
	camera.yaw -= 180.0f;
	camera.pitch = -camera.pitch;
	camera.RotateWithMouse(0, 0, true); // update camera vectors with constrains enabled
	
	// render normal scene
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	view = camera.GetViewMatrix();
	projection = glm::perspective(glm::radians(camera.fov), (float)SCR_WIDTH / (float)SCR_HEIGHT, 0.1f, 100.0f);
	render_scene(sorted);
	//glDisable(GL_DEPTH_TEST);

	// render rear view from texture on top
	screen_shader->bind();
	glViewport(REARVIEW_START_POS_X, REARVIEW_START_POS_Y, REARVIEW_SCR_WIDTH, REARVIEW_SCR_HEIGHT);
	glBindTexture(GL_TEXTURE_2D, texture_color_buffer);
	glBindVertexArray(quadVAO);
	glDrawArrays(GL_TRIANGLES, 0, 6);
	glBindVertexArray(0);
	
	glfwSwapBuffers(window);
}


void setupShader()
{
	shader_lit = std::make_shared<Shader>("exercises/ex_Framebuffers/blending.vert", "exercises/ex_Framebuffers/blending.frag");
	screen_shader = std::make_shared<Shader>("exercises/ex_Framebuffers/5.1.framebuffers.vert", "exercises/ex_Framebuffers/5.1.framebuffers.frag");
	// shader configuration
	// --------------------
	shader_lit->bind();
	shader_lit->setInt("texture1", 0);

	screen_shader->bind();
	screen_shader->setInt("screenTexture", 0);
	
}

void setupGeometry()
{
	// cube VAO
	glGenVertexArrays(1, &cubeVAO);
	glGenBuffers(1, &cubeVBO);
	glBindVertexArray(cubeVAO);
	glBindBuffer(GL_ARRAY_BUFFER, cubeVBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(cubeVertices), &cubeVertices, GL_STATIC_DRAW);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void*)0);
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void*)(3 * sizeof(float)));
	// plane VAO
	glGenVertexArrays(1, &plane_vao);
	glGenBuffers(1, &planeVBO);
	glBindVertexArray(plane_vao);
	glBindBuffer(GL_ARRAY_BUFFER, planeVBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(planeVertices), &planeVertices, GL_STATIC_DRAW);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void*)0);
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void*)(3 * sizeof(float)));
	// transparent quad VAO
	glGenVertexArrays(1, &transparentVAO);
	glGenBuffers(1, &transparentVBO);
	glBindVertexArray(transparentVAO);
	glBindBuffer(GL_ARRAY_BUFFER, transparentVBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(transparentVertices), &transparentVertices, GL_STATIC_DRAW);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void*)0);
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void*)(3 * sizeof(float)));
	// screen quad VAO
	glGenVertexArrays(1, &quadVAO);
	glGenBuffers(1, &quadVBO);
	glBindVertexArray(quadVAO);
	glBindBuffer(GL_ARRAY_BUFFER, quadVBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(quadVertices), &quadVertices, GL_STATIC_DRAW);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 4 * sizeof(float), (void*)0);
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 4 * sizeof(float), (void*)(2 * sizeof(float)));
	
	glBindVertexArray(0);

	cubeTexture = loadTexture("res/textures/container.jpg");
	floorTexture = loadTexture("res/textures/metal.png");
	transparentTexture = loadTexture("res/textures/window.png");
	glBindTexture(GL_TEXTURE_2D, transparentTexture);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glBindTexture(GL_TEXTURE_2D, 0);

	// framebuffers
	glGenFramebuffers(1, &fbo);
	glBindFramebuffer(GL_FRAMEBUFFER, fbo);

	// create texture for the color attachement, need to sample it in shader
	glGenTextures(1, &texture_color_buffer);
	glBindTexture(GL_TEXTURE_2D, texture_color_buffer);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, SCR_WIDTH, SCR_HEIGHT, 0, GL_RGB, GL_UNSIGNED_BYTE, nullptr);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, texture_color_buffer, 0);

	// create rernderbuffer for stencil and depth attachement
	glGenRenderbuffers(1, &rbo);
	glBindRenderbuffer(GL_RENDERBUFFER, rbo);
	glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH24_STENCIL8, 800, 600);
	glBindRenderbuffer(GL_RENDERBUFFER, 0);

	glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_STENCIL_ATTACHMENT, GL_RENDERBUFFER, rbo);
	
	
	if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
	{
		std::cout << "ERROR::FRAMEBUFFER:: Framebuffer is not complete!" << std::endl;
	}
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

void processInput(GLFWwindow* window)
{
	if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
		glfwSetWindowShouldClose(window, true);

	if (glfwGetKey(window, GLFW_KEY_L) == GLFW_PRESS)
		glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

	if (glfwGetKey(window, GLFW_KEY_P) == GLFW_PRESS)
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

	if (flyingCameraActive)
	{
		if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS)
			camera.Move(Camera_Movement::FORWARD, deltaTime);
		if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS)
			camera.Move(Camera_Movement::BACKWARD, deltaTime);
		if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS)
			camera.Move(Camera_Movement::LEFT, deltaTime);
		if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS)
			camera.Move(Camera_Movement::RIGHT, deltaTime);
		if (glfwGetKey(window, GLFW_KEY_E) == GLFW_PRESS)
			camera.Move(Camera_Movement::UP, deltaTime);
		if (glfwGetKey(window, GLFW_KEY_Q) == GLFW_PRESS)
			camera.Move(Camera_Movement::DOWN, deltaTime);
	}

	if (glfwGetKey(window, GLFW_KEY_R) == GLFW_PRESS)
	{
		camera.Reset();
	}
}

void scroll_callback(GLFWwindow* window, double xOffset, double yOffset)
{
	camera.Zoom(yOffset);
}

void cursor_callback(GLFWwindow* window, double xPos, double yPos)
{
	if (flyingCameraActive)
	{
		if (firstDown)
		{
			mouse[0] = xPos;
			mouse[1] = yPos;
			firstDown = false;
		}
		float xOffset = xPos - mouse[0];
		float yOffset = mouse[1] - yPos; // reversed since y-coordinates range from top to bottom
		camera.RotateWithMouse(xOffset, yOffset, true);

		mouse[0] = xPos;
		mouse[1] = yPos;
	}
}

void mouse_button_callback(GLFWwindow* window, int button, int action, int mods)
{
	if (button == GLFW_MOUSE_BUTTON_RIGHT && action == GLFW_PRESS)
	{
		glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
		firstDown = true;
		flyingCameraActive = true;
	}
	else if (button == GLFW_MOUSE_BUTTON_RIGHT && action == GLFW_RELEASE)
	{
		glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_NORMAL);
		firstDown = false;
		flyingCameraActive = false;
	}
}

void framebuffer_size_callback(GLFWwindow* window, int width, int height)
{
	glViewport(0, 0, width, height);
	GLCheckError();
}

GLuint loadTexture(char const* path)
{
	GLuint textureID;
	glGenTextures(1, &textureID);
	GLCheckError();

	int width, height, nrChannels;
	unsigned char* data = stbi_load(path, &width, &height, &nrChannels, 0);

	if (data)
	{
		GLenum format;
		if (nrChannels == 1)
			format = GL_RED;
		else if (nrChannels == 3)
			format = GL_RGB;
		else if (nrChannels == 4)
			format = GL_RGBA;

		glBindTexture(GL_TEXTURE_2D, textureID);
		GLCheckError();
		glTexImage2D(GL_TEXTURE_2D, 0, format, width, height, 0, format, GL_UNSIGNED_BYTE, data);
		GLCheckError();
		glGenerateMipmap(GL_TEXTURE_2D);
		GLCheckError();

		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		GLCheckError();
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
		GLCheckError();
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
		GLCheckError();
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		GLCheckError();

		stbi_image_free(data);
	}
	else
	{
		std::cout << "Failed to load texture at path: " << path << std::endl;
		stbi_image_free(data);
	}

	return textureID;
}
